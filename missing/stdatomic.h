static inline long atomic_fetch_sub(volatile long* loc, long amt) {
    // optimistic concurrency control
    register long rax;
    do {
        rax = *loc;
    } while (!__sync_bool_compare_and_swap(loc, rax, rax - amt));
    return rax;
}
static inline int atomic_fetch_add(volatile int* loc, int amt) {
    register int rax;
    do {
        rax = *loc;
    } while (!__sync_bool_compare_and_swap(loc, rax, rax + amt));
    return rax;
}
static inline int atomic_compare_exchange_strong(void *volatile *loc, void **expected, void *desired) {
    return __sync_bool_compare_and_swap(loc, *expected, desired);
}
