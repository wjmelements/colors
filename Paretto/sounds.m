#include "sounds.h"

#import <AudioToolbox/AudioToolbox.h>

static SystemSoundID soundIDs[NUM_SOUNDS];
static NSString *paths[NUM_SOUNDS] = {
    @"ting-clean",
    @"swipe3",
    @"splash",
    @"ringing"
};

static void sound_load(sound_t sound) {
    NSString *path = [[NSBundle mainBundle] pathForResource:paths[sound] ofType:@"m4a"];
    NSURL *url = [NSURL fileURLWithPath:path];
    OSStatus status = AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundIDs[sound]);
    if (status != 0) {
        fprintf(stderr, "Could not load sound %u (%s)\n", sound, paths[sound].UTF8String);
    }
}

void sounds_load() {
    for (sound_t sound = 0; sound < NUM_SOUNDS; sound++) {
        sound_load(sound);
    }
}

void sounds_unload() {
    for (sound_t sound = 0; sound < NUM_SOUNDS; sound++) {
        AudioServicesDisposeSystemSoundID(soundIDs[sound]);
    }
}

void sounds_play(sound_t sound) {
    AudioServicesPlaySystemSound(soundIDs[sound]);
}

void sounds_stop(sound_t sound) {
    AudioServicesDisposeSystemSoundID(soundIDs[sound]);
    sound_load(sound);
}
