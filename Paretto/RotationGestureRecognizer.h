#import <UIKit/UIKit.h>
@interface RotationGestureRecognizer : UIGestureRecognizer

- (CGFloat)rotation;

@end
