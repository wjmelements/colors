#import "PuzzlePresenter.h"
#import "PuzzleView.h"

@implementation PuzzlePresenter

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    PuzzleView *puzzleView = [transitionContext viewForKey:UITransitionContextToViewKey];
    [[transitionContext containerView] addSubview:puzzleView];
    UIView *icon = puzzleView.icon;
    [UIView
        animateWithDuration:0.1
        animations:^{
            icon.alpha = 0.0;
        }];
    [UIView animateWithDuration:0.2
        animations:^{
            puzzleView.transform = CGAffineTransformIdentity;
            puzzleView.layer.cornerRadius = 0;
        }
        completion:^(BOOL finished) {
            puzzleView.presented = YES;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.2;
}

@end
