@import UIKit;

#include "color_t.h"
#include "group_t.h"
#include "point_t.h"

typedef struct colorstate {
    UIGestureRecognizerState state;
    point_t midPoint;
    color_t startColor;
    color_t color;
    group_t group;
    BOOL wasTap;
} colorstate_t;

@interface ColorGestureRecognizer : UIGestureRecognizer

- (colorstate_t *)next;

@property (nonatomic, strong) group_t(^groupForPoint)(point_t);
@property (nonatomic, assign) CGPoint scale;

- (void)setColors:(const color_t *)colors;
- (const color_t *)colors;
- (void)setAngle:(CGFloat)angle;

@end
