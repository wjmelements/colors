#import <UIKit/UIKit.h>

#import "SettingsViewController.h"

@interface SurvivalDismisser : NSObject <UIViewControllerAnimatedTransitioning>

- (instancetype)initWithPresenter:(SettingsViewController *)presenter;

- (void)dismiss;

@end
