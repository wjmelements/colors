#import "AppDelegate.h"
#import "ColorPieControl.h"
#import "ColorShop.h"
#import "DefeatOverlay.h"
#import "IntegralCounter.h"
#import "LoadingOverlay.h"
#import "PuzzleDismisser.h"
#import "PuzzleHighScoresView.h"
#import "PuzzlePresenter.h"
#import "PuzzleView.h"
#import "PuzzleViewController.h"
#import "RationalCounter.h"
#import "RotationGestureRecognizer.h"
#import "SettingsViewController.h"
#import "SurvivalDismisser.h"
#import "SurvivalHighScoresView.h"
#import "SurvivalPresenter.h"
#import "SurvivalViewController.h"
#import "UserManager.h"
#import "UIColor+C.h"
#import "IntegralCounter.h"

#include "style.h"
#include "trophies.h"

CGFloat minScale = .485f;
#define SCORES_HEIGHT 80
#define MIN_INSET 5

@interface SettingsViewController () <
    ColorShopDelegate
    >

@end

@implementation SettingsViewController {
    PuzzleViewController *_puzzleViewController;
    SurvivalViewController *_survivalViewController;
    PuzzleDismisser *_puzzleDismisser;
    SurvivalDismisser *_survivalDismisser;
    UITapGestureRecognizer *_puzzleTapRecognizer;
    UITapGestureRecognizer *_survivalTapRecognizer;
    RotationGestureRecognizer *_pieRotationRecognizer;

    NSDictionary *_textAttributes;
    IntegralCounter *_survivalScore;
    UILabel *_survivalTrophies;
    IntegralCounter *_puzzleScore;
    RationalCounter *_puzzleTime;

    ColorPieControl *_colorPieControl;
    ColorPie *_colorPie;

    UIView *_survivalScoreLock, *_puzzleScoreLock;
    UILabel *_trophyLabel;

    BOOL _statusBarOverride;
    BOOL _statusBarHidden;
}

+ (instancetype)shared
{
    static SettingsViewController *shared;
    if (!shared) {
        shared = [[SettingsViewController alloc] init];
    }
    return shared;
}

- (void)viewDidLoad
{
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    minScale = MIN(minScale,
        ((bounds.size.width - 3 * MIN_INSET)/2)/bounds.size.width
    );
    [self _ensurePuzzleVC];
    sView.backgroundColor = [UIColor color:interpolate(NULL_COLOR, BLACK, 1)];

    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *puzzleView = _puzzleViewController.view;
        puzzleView.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(minScale, minScale),
            CGAffineTransformMakeTranslation(bounds.size.width*-(1+minScale)/6, .25*bounds.size.height)
        );
        CGRect shadowFrame = shadowRect(puzzleView.frame);
        UIView *shadow = [[UIView alloc] initWithFrame:shadowFrame];
        shadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
        [sView addSubview:shadow];
        [sView addSubview:puzzleView];
        shadow.layer.masksToBounds = puzzleView.layer.masksToBounds = YES;
        shadow.layer.cornerRadius = CORNER_RADIUS;
        puzzleView.layer.cornerRadius = CORNER_RADIUS / minScale;
        _puzzleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTapPuzzle:)];
        [_puzzleViewController.view addGestureRecognizer:_puzzleTapRecognizer];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _initHighScores];
            [self _updatePuzzleScore];
            [self _updateSurvivalScore];
            [self _initColorPie];
        });
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        [self _ensureSurvivalVC];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIView *survivalView = _survivalViewController.view;
            survivalView.transform = CGAffineTransformConcat(
                CGAffineTransformMakeScale(minScale,minScale),
                CGAffineTransformMakeTranslation(bounds.size.width*(1+minScale)/6, .25*sView.frame.size.height)
            );
            survivalView.layer.cornerRadius = CORNER_RADIUS / minScale;
            UIView *shadow = [[UIView alloc] initWithFrame:shadowRect(survivalView.frame)];
            shadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
            shadow.layer.cornerRadius = CORNER_RADIUS;
            survivalView.layer.masksToBounds = shadow.layer.masksToBounds = YES;

            [sView addSubview:shadow];
            [sView addSubview:survivalView];
            _survivalTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTapSurvival:)];
            [survivalView addGestureRecognizer:_survivalTapRecognizer];
        });
    });
    UITapGestureRecognizer *tutorialRecognizer = [[UITapGestureRecognizer alloc]
        initWithTarget:[AppDelegate shared]
        action:@selector(showTutorial)];
    tutorialRecognizer.numberOfTapsRequired = 2;
    tutorialRecognizer.numberOfTouchesRequired = 2;
    [sView addGestureRecognizer:tutorialRecognizer];
}

- (void)_ensurePuzzleVC
{
    if (!_puzzleViewController) {
        _puzzleViewController = [PuzzleViewController shared];
        _puzzleViewController.transitioningDelegate = self;
    }
}

- (void)_ensureSurvivalVC
{
    if (!_survivalViewController) {
        _survivalViewController = [SurvivalViewController shared];
        _survivalViewController.transitioningDelegate = self;
    }
}

- (void)_didTapPuzzle:(UITapGestureRecognizer *)sender
{
    _puzzleTapRecognizer.enabled = NO;
    _puzzleDismisser = [[PuzzleDismisser alloc] initWithPresenter:self];
    _puzzleViewController.puzzleDismisser = _puzzleDismisser;
    [self presentViewController:_puzzleViewController animated:YES completion:nil];
}

- (void)_didTapSurvival:(UITapGestureRecognizer *)sender
{
    _survivalTapRecognizer.enabled = NO;
    _survivalDismisser = [[SurvivalDismisser alloc] initWithPresenter:self];
    _survivalViewController.dismisser = _survivalDismisser;
    [self presentViewController:_survivalViewController animated:YES completion:nil];
}

- (void)puzzleDismisserDidDismiss:(PuzzleDismisser *)puzzleDismisser {
    if (_puzzleDismisser == puzzleDismisser) {
        _puzzleDismisser = nil;
        _puzzleTapRecognizer.enabled = YES;
    }
    [self _updatePuzzleScore];
    [self _updateTrophyCase];
}

- (void)survivalDismisserDidDismiss:(SurvivalDismisser *)survivalDismisser {
    if (_survivalDismisser == survivalDismisser) {
        _survivalViewController.dismisser = _survivalDismisser = nil;
        _survivalTapRecognizer.enabled = YES;
    }
    [self _updateSurvivalScore];
    [self _updateTrophyCase];
}

- (void)overrideStatusBarHidden:(BOOL)hidden
{
    _statusBarOverride = YES;
    _statusBarHidden = hidden;
    [self setNeedsStatusBarAppearanceUpdate];
}
- (void)unsetStatusBarOverride
{
    _statusBarOverride = NO;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (BOOL)prefersStatusBarHidden
{
    return _statusBarOverride ? _statusBarHidden : NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)_initColorPie
{
    UIView *selfView = self.view;
    CGRect bounds = selfView.bounds;
    CGFloat halfHeight = bounds.size.height / 2;
    CGFloat inset = MAX(bounds.size.width * (1 - 2 * minScale) / 3, MIN_INSET);
    CGFloat controlHeight = halfHeight - SCORES_HEIGHT - inset - 20;
    CGRect panelFrame = CGRectMake(inset, 20, controlHeight, controlHeight);
    _colorPieControl = [[ColorPieControl alloc]
        initWithFrame:panelFrame];
    UIView *panelShadow = [[UIView alloc]
        initWithFrame:shadowRect(panelFrame)];
    panelShadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
    _colorPieControl.layer.masksToBounds = panelShadow.layer.masksToBounds = YES;
    _colorPieControl.layer.cornerRadius = panelShadow.layer.cornerRadius = CORNER_RADIUS;
    _colorPie = _colorPieControl.colorPie;
    [selfView addSubview:panelShadow];
    [selfView addSubview:_colorPieControl];
    _pieRotationRecognizer = [[RotationGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(didRotate:)
    ];
    [_colorPie addGestureRecognizer:_pieRotationRecognizer];
    [_colorPie update];

    [self _setupTrophyCase:panelFrame];
}

- (void)_setupTrophyCase:(CGRect)panelFrame
{
    UIView *selfView = self.view;
    CGRect rightFrame = CGRectMake(
        panelFrame.size.width + panelFrame.origin.x * 2,
        panelFrame.origin.y,
        selfView.bounds.size.width - panelFrame.size.width - panelFrame.origin.x * 3,
        panelFrame.size.height);
    UIView *rightView = [[UIView alloc]
        initWithFrame:rightFrame
    ];
    rightView.backgroundColor = [UIColor whiteColor];
    UIView *rightShadow = [[UIView alloc]
        initWithFrame:shadowRect(rightFrame)];
    rightShadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
    rightShadow.layer.cornerRadius = rightView.layer.cornerRadius = CORNER_RADIUS;
    [selfView addSubview:rightShadow];
    [selfView addSubview:rightView];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(_presentColorShop:)];
    [rightView addGestureRecognizer:tapRecognizer];
    UILabel *lockView = [self _makeLockView];
    lockView.center = CGPointMake(rightFrame.size.width / 2, rightFrame.size.height / 2);
    lockView.frame = [rightView convertRect:rightFrame fromView:selfView];
    lockView.numberOfLines = 0;
    lockView.lineBreakMode = NSLineBreakByCharWrapping;
    [rightView addSubview:lockView];
    _trophyLabel = lockView;
    [self _updateTrophyCase];
}

- (void)_presentColorShop:(UITapGestureRecognizer *)sender
{
    if (![self _earnedTrophies].length) {
        tease(sender.view);
        return;
    }
    ColorShop *shop = [[ColorShop alloc] init];
    shop.earnedTrophies = [self _earnedTrophies];
    shop.textAttributes = _textAttributes;
    shop.delegate = self;
    [self
        presentViewController:shop
        animated:NO
        completion:nil
    ];
}

- (NSString *)_earnedTrophies
{
    // the trophies are:
    // 6 from Survival
    // 4 from Streak
    // 4 from Difficulty
    uint32_t highestScore = user_highestScore(&user);
    score_level_t scoreLevel = scoreLevelForScore(highestScore);
    NSMutableString *trophies = [NSMutableString stringWithCapacity:NUM_TROPHIES];
    for (; scoreLevel >= DIAMOND; scoreLevel--) {
        [trophies appendString:scoreStrings[scoreLevel]];
    }
    uint64_t longestStreak = piXor(user.piLongestStreak);
    if (longestStreak > 24) {
        [trophies appendString:@"🌸"];
    }
    if (longestStreak > 14) {
        [trophies appendString:@"🌺"];
    }
    if (longestStreak > 9) {
        [trophies appendString:@"🍀"];
    }
    if (longestStreak > 2) {
        [trophies appendString:@"💝"];
    }
    for (; scoreLevel > SHIT; scoreLevel--) {
        [trophies appendString:scoreStrings[scoreLevel]];
    }
    unsigned didPerfect = user.header.didPerfectDifficulty;
    for (difficulty_t difficulty = NUM_DIFFICULTIES; difficulty --> 0;) {
        if ((didPerfect >> difficulty) & 1) {
            [trophies appendString:difficultyTexts[difficulty]];
        }
    }
    return [trophies copy];
}

- (void)_updateTrophyCase
{
    NSString *trophies = [self _earnedTrophies];
    _trophyLabel.attributedText = [[NSAttributedString alloc]
        initWithString:trophies.length ? trophies : @"🔒"
        attributes:_textAttributes];
}

- (UILabel *)_makeLockView
{
    UILabel *rightLock = [[UILabel alloc]
        initWithFrame:CGRectZero
    ];
    rightLock.attributedText = [[NSAttributedString alloc]
        initWithString:@"🔒"
        attributes:_textAttributes
    ];
    rightLock.textAlignment = NSTextAlignmentCenter;
    [rightLock sizeToFit];
    return rightLock;
}

- (void)didRotate:(RotationGestureRecognizer *)sender
{
    switch (sender.state) {
    case UIGestureRecognizerStatePossible:
    case UIGestureRecognizerStateFailed:
    case UIGestureRecognizerStateCancelled:
        return;
    case UIGestureRecognizerStateBegan:
    case UIGestureRecognizerStateChanged:
        [self setPieRotation:sender.rotation];
        break;
    case UIGestureRecognizerStateEnded:
        [_colorPieControl commitPieRotation:sender.rotation];
    }
}

- (void)setPieRotation:(CGFloat)rotation
{
    rotation += user.pieAngle;
    _colorPie.transform = CGAffineTransformMakeRotation(rotation);
}

- (void)_initHighScores
{
    _textAttributes = @{
        NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:36],
        NSKernAttributeName : @2
    };
    UIView *selfView = self.view;
    CGRect bounds = selfView.bounds;
    CGFloat halfWidth = CGRectGetWidth(bounds) / 2;
    CGFloat halfHeight = CGRectGetHeight(bounds) / 2;
    CGFloat inset = MAX(bounds.size.width * (1 - 2 * minScale) / 3, MIN_INSET);

    CGRect wrapperRect = CGRectMake(inset, halfHeight - SCORES_HEIGHT, halfWidth - inset * 1.5, SCORES_HEIGHT);
    UIView *puzzleWrapper = [[UIView alloc]
        initWithFrame:wrapperRect];
    CGFloat survivalOffset = halfWidth - inset * .5;
    UIView *survivalWrapper = [[UIView alloc]
        initWithFrame:CGRectOffset(wrapperRect, survivalOffset, 0)];
    survivalWrapper.backgroundColor = puzzleWrapper.backgroundColor = [UIColor whiteColor];
    survivalWrapper.layer.masksToBounds = puzzleWrapper.layer.masksToBounds = YES;
    survivalWrapper.layer.cornerRadius = puzzleWrapper.layer.cornerRadius = CORNER_RADIUS;
    CGRect shadowFrame = shadowRect(wrapperRect);
    UIView *puzzleShadow = [[UIView alloc]
        initWithFrame:shadowFrame];
    UIView *survivalShadow = [[UIView alloc]
        initWithFrame:CGRectOffset(shadowFrame, survivalOffset, 0)];

    survivalShadow.backgroundColor = puzzleShadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
    survivalShadow.layer.masksToBounds = puzzleShadow.layer.masksToBounds = YES;
    survivalShadow.layer.cornerRadius = puzzleShadow.layer.cornerRadius = CORNER_RADIUS;
    [selfView addSubview:puzzleShadow];
    [selfView addSubview:survivalShadow];
    [selfView addSubview:puzzleWrapper];
    [selfView addSubview:survivalWrapper];

    _puzzleScore = [[IntegralCounter alloc]
        initWithFrame:CGRectMake(0, 0, wrapperRect.size.width, 40)];
    _puzzleScore.numberOfLines = 1;
    _puzzleScore.textAlignment = NSTextAlignmentRight;
    _puzzleScore.attributedFormatBlock = ^NSAttributedString *(uint64_t value) {
        return [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%llu", value]
            attributes:_textAttributes];
    };
    UILabel *puzzleScorePrefix = [[UILabel alloc]
        initWithFrame:CGRectMake(0,0,40,40)];
    puzzleScorePrefix.attributedText = [[NSAttributedString alloc]
        initWithString:@"💖"
        attributes:_textAttributes];
    puzzleScorePrefix.textAlignment = NSTextAlignmentLeft;
    [_puzzleScore addSubview:puzzleScorePrefix];
    [puzzleWrapper addSubview:_puzzleScore];

    _puzzleTime = [[RationalCounter alloc]
        initWithFrame:CGRectMake(0, 40, wrapperRect.size.width, 40)];
    _puzzleTime.numberOfLines = 1;
    _puzzleTime.textAlignment = NSTextAlignmentRight;
    UILabel *puzzleTimePrefix = [[UILabel alloc]
        initWithFrame:CGRectMake(0,0,40,40)];
    _puzzleTime.attributedFormatBlock = ^NSAttributedString *(CGFloat value) {
        puzzleTimePrefix.attributedText = [[NSAttributedString alloc]
            initWithString:timeEmojiForTime(value)
            attributes:_textAttributes];
        return [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%.1f", value]
            attributes:_textAttributes];
    };
    puzzleTimePrefix.textAlignment = NSTextAlignmentLeft;
    [_puzzleTime addSubview:puzzleTimePrefix];
    [puzzleWrapper addSubview:_puzzleTime];

    UITapGestureRecognizer *puzzleTap = [[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(puzzleWrapperTapped:)];
    [puzzleWrapper addGestureRecognizer:puzzleTap];
    UITapGestureRecognizer *survivalTap = [[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(survivalWrapperTapped:)];
    [survivalWrapper addGestureRecognizer:survivalTap];

    _survivalScore = [[IntegralCounter alloc]
        initWithFrame:CGRectMake(0, 40, wrapperRect.size.width, 40)];
    _survivalScore.numberOfLines = 1;
    _survivalScore.textAlignment = NSTextAlignmentRight;
    _survivalScore.attributedFormatBlock = ^NSAttributedString *(uint64_t value) {
        if (value < 1) {
            return nil;
        }
        if (value < 100000) {
            return [[NSAttributedString alloc]
                    initWithString:[NSString stringWithFormat:@"%llu%@", value, scoreStringForScore(lround(value))]
                    attributes:_textAttributes];
        } else {
            return [[NSAttributedString alloc]
                initWithString:[NSString stringWithFormat:@"%llu", value]
                attributes:_textAttributes];
        }
    };
    _survivalTrophies = [[UILabel alloc]
        initWithFrame:CGRectMake(0, 0, wrapperRect.size.width, 40)];
    _survivalTrophies.numberOfLines = 1;
    _survivalTrophies.textAlignment = NSTextAlignmentLeft;
    [survivalWrapper addSubview:_survivalScore];
    [survivalWrapper addSubview:_survivalTrophies];
    if (!user.survivalHighScores[0]) {
        _survivalScoreLock = [self _makeLockView];
        _survivalScoreLock.center = CGPointMake(wrapperRect.size.width / 2, wrapperRect.size.height / 2);
        [survivalWrapper addSubview:_survivalScoreLock];
    }

    if (!user.latestAttempts[0]) {
        _puzzleScoreLock = [self _makeLockView];
        _puzzleScoreLock.center = CGPointMake(wrapperRect.size.width / 2, wrapperRect.size.height / 2);
        [puzzleWrapper addSubview:_puzzleScoreLock];
    }
}

- (void)survivalWrapperTapped:(UITapGestureRecognizer *)sender
{
    UIView *survivalWrapper = sender.view;
    if (user.survivalHighScores[0]) {
        [self _showSurvivalHighScores:survivalWrapper];
    } else {
        tease(survivalWrapper);
    }
}

/** Ideal
 * The trophies unsheath and rotate to vertical left, as the high score
 * slides into position, among other scores, which ride in behind the trophies.
 * The trophies are always below the score texts.
 * The minimize button is pulled in from the right.
*/
- (void)_showSurvivalHighScores:(UIView *)survivalWrapper
{
    CGFloat y = survivalWrapper.frame.origin.y;
    CGFloat width = CGRectGetWidth(self.view.bounds);
    CGFloat height = CGRectGetHeight(self.view.bounds) - y;
    SurvivalHighScoresView *scoresView = [[SurvivalHighScoresView alloc]
        initWithFrame:CGRectMake(0, y, width, height)
        textAttributes:_textAttributes];
    [self.view addSubview:scoresView];
    CGPoint scoresViewCenter = scoresView.center;
    CGPoint wrapperCenter = survivalWrapper.center;
    CGSize scaleSize = CGSizeMake(
        CGRectGetWidth(survivalWrapper.bounds) / width,
        CGRectGetHeight(survivalWrapper.bounds) / height
    );
    CGAffineTransform scale = CGAffineTransformMakeScale(
        scaleSize.width,
        scaleSize.height
    );
    scoresView.transform = CGAffineTransformConcat(
        scale,
        CGAffineTransformMakeTranslation(
            wrapperCenter.x - scoresViewCenter.x,
            wrapperCenter.y - scoresViewCenter.y
        )
    );
    CGPoint firstCenter = scoresView.emojiViews[0].center;
    CGAffineTransform downScale = CGAffineTransformMakeScale(
        1 / scaleSize.width,
        1 / scaleSize.height
    );
    const CGFloat ROW_SIZE = scoresView.scoreViews[0].bounds.size.height;
    for (int i = 0; i < 8; i++) {
        CGPoint currentCenter = scoresView.emojiViews[i].center;
        CGPoint startCenter = CGPointMake(currentCenter.y / scaleSize.width, firstCenter.y / scaleSize.height);
        scoresView.emojiViews[i].transform = CGAffineTransformConcat(
            downScale,
            CGAffineTransformMakeTranslation(
                startCenter.x - currentCenter.x,
                startCenter.y - currentCenter.y));
        currentCenter = scoresView.scoreViews[i].center;

        startCenter = CGPointMake((scoresView.frame.size.width - ROW_SIZE) / scaleSize.width, (currentCenter.y + ROW_SIZE) / scaleSize.height);
        scoresView.scoreViews[i].transform = CGAffineTransformConcat(
            downScale,
            CGAffineTransformMakeTranslation(
                startCenter.x - currentCenter.x,
                startCenter.y - currentCenter.y
            ));
    }
    [UIView
        animateWithDuration:0.2
        delay:0
        options:UIViewAnimationOptionAllowUserInteraction
        animations:^{
            scoresView.transform = CGAffineTransformIdentity;
            for (int i = 0; i < 8; i++) {
                scoresView.emojiViews[i].transform = CGAffineTransformIdentity;
                scoresView.scoreViews[i].transform = CGAffineTransformIdentity;
            }
        }
        completion:nil];
    [scoresView addGestureRecognizer:[[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(survivalHighScoresTapped:)]];
}

- (void)survivalHighScoresTapped:(UITapGestureRecognizer *)sender
{
    UIView *scoresView = sender.view;
    [scoresView.layer removeAllAnimations];
    [UIView
        animateWithDuration:0.1
        animations:^{
            scoresView.alpha = 0;
        }
        completion:^(BOOL finished) {
            [scoresView removeFromSuperview];
        }];
}

- (void)puzzleWrapperTapped:(UITapGestureRecognizer *)sender
{
    UIView *puzzleWrapper = sender.view;
    if (user.latestAttempts[0]) {
        [self _showPuzzleHighScores:puzzleWrapper];
    } else {
        tease(puzzleWrapper);
    }
}

- (void)_showPuzzleHighScores:(UIView *)puzzleWrapper
{
    CGRect wrapperRect = puzzleWrapper.frame;
    CGFloat y = wrapperRect.origin.y;
    PuzzleHighScoresView *scoresView = [[PuzzleHighScoresView alloc]
        initWithFrame:CGRectMake(0, y, self.view.bounds.size.width, self.view.bounds.size.height - y)
        attributes:_textAttributes
    ];
    [self.view addSubview:scoresView];
    [scoresView addGestureRecognizer:[[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(survivalHighScoresTapped:)
    ]];
}


static int comparator(const void *one, const void *two) {
    uint64_t d1 = *(uint64_t *)one;
    uint64_t d2 = *(uint64_t *)two;
    return d2 - d1;
}

- (void)_updateSurvivalScore
{
    if (_survivalScoreLock && user.survivalHighScores[0]) {
        [_survivalScoreLock removeFromSuperview];
        _survivalScoreLock = nil;
    }
    [_survivalScore countFromCurrentValueTo:user_highestScore(&user) withDuration:2.2];

    uint64_t scores[8];
    memcpy(&scores[0], user.survivalHighScores, sizeof(user.survivalHighScores));
    memcpy(&scores[4], user.nextSurvivalHighScores, sizeof(user.nextSurvivalHighScores));
    qsort(scores, 8, sizeof(scores[0]), comparator);
    NSMutableArray *symbols = [NSMutableArray arrayWithCapacity:4];
    for (int i = 0; i < 4; i++) {
        if (scores[i]) {
            [symbols addObject:scoreStringForScore(scores[i])];
        }
    }
    NSString *string = [symbols componentsJoinedByString:@""];
    _survivalTrophies.attributedText = [[NSAttributedString alloc]
        initWithString:string
        attributes:_textAttributes];
}

- (void)_updatePuzzleScore
{
    if (_puzzleScoreLock && user.latestAttempts[0]) {
        [_puzzleScoreLock removeFromSuperview];
    }
    uint32_t puzzlesPerfected = user_puzzlesPerfected(&user);
    [_puzzleScore countFromCurrentValueTo:puzzlesPerfected withDuration:2.2];
    _puzzleScore.alpha = puzzlesPerfected ? 1.0 : 0.0;
    double bestTime = piToggle(user.piBestTime.value);
    if (isinf(bestTime)) {
        _puzzleTime.alpha = 0.0;
    } else {
        if (_puzzleTime.alpha == 0.0) {
            [_puzzleTime countFromCurrentValueTo:bestTime withDuration:0];
        } else {
            [_puzzleTime countFromCurrentValueTo:bestTime withDuration:2.2];
        }
        _puzzleTime.alpha = 1.0;
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
    presentingController:(UIViewController *)presenting
    sourceController:(UIViewController *)source
{
    if (source == self && presented == [PuzzleViewController shared]) {
        return [[PuzzlePresenter alloc] init];
    }
    if (source == self && presented == [SurvivalViewController shared]) {
        return [[SurvivalPresenter alloc] init];
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    if (dismissed == [PuzzleViewController shared]) {
        return _puzzleDismisser;
    }
    if (dismissed == [SurvivalViewController shared]) {
        return _survivalDismisser;
    }
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if ([animator respondsToSelector:@selector(startInteractiveTransition:)]) {
        return (id<UIViewControllerInteractiveTransitioning>)animator;
    }
    return nil;
}

#pragma mark - ColorShopDelegate

- (void)didChangeColors
{
    [_colorPie update];
}

@end
