@import UIKit;
#import "TutorialDelegate.h"
@interface TutorialViewController : UIViewController
- (instancetype)initWithDelegate:(id<TutorialDelegate>)tutorialDelegate;
@end
