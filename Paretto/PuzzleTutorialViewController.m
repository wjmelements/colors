#import "PuzzleTutorialViewController.h"

#import "HintView.h"
#import "UIColor+C.h"
#include "colormap.h"
#import "sounds.h"

static NSDictionary *textAttributes(CGFloat size)
{
    return @{
        NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:size],
    };
}

enum {
    TL,
    TR,
    BL,
    BR,
    CENTER,
    NUM_LAYERS
};

#include "bitmap.h"
static uint16_t edgeMap = 0x3f3;

static uint64_t indexOf(int d1, int d2) {
    if (d1 > d2) {
        int d = d1;
        d1 = d2;
        d2 = d;
    }
    return (d2 * d2 - d2) / 2 + d1;
}
static bool isAdjacent(int d1, int d2) {
    return bitmap_isSet(&edgeMap, indexOf(d1, d2));
}

@implementation PuzzleTutorialViewController{
    // FIXME reorder members
    id<TutorialDelegate> _delegate;
    CALayer *_layers[NUM_LAYERS];
    color_t _layerColors[NUM_LAYERS];
    color_t _directionColors[NUM_LAYERS];
    CGPoint _startLocation;
    HintView
        *_downHints[NUM_LAYERS],
        *_overHint,
        *_tapHint;
    int _currentIndex;
    uint8_t _invalids[NUM_LAYERS];
}

static int getDirection(const CGPoint one, const CGPoint two) {
    CGFloat dx = one.x - two.x;
    CGFloat dy = one.y - two.y;
    if (fabs(dx) + fabs(dy) < 25) {
        return CENTER;
    }
    if (dx < 0) {
        if (dy < 0) {
            return TL;
        } else {
            return BL;
        }
    } else {
        if (dy < 0) {
            return TR;
        } else {
            return BR;
        }
    }
}

- (instancetype)initWithDelegate:(id<TutorialDelegate>)tutorialDelegate
{
    self = [super init];
    _delegate = tutorialDelegate;
    [self _setupPuzzle];
    dispatch_async(dispatch_get_main_queue(), ^{
        _overHint = [HintView overHintView:self.view];
        for (int i = 0; i < NUM_LAYERS; i++) {
            _downHints[i] = [HintView
                downHintView:self.view
                frame:_layers[CENTER].frame // for consistent size
                showLine:NO];
            _downHints[i].center = _layers[i].position;
            [_downHints[i] start];
        }
        [self _showTutorialLabel];
    });
    return self;
}

- (void)_setupPuzzle
{
    CGColorRef nullColor = [UIColor color:NULL_COLOR].CGColor;
    CGColorRef borderColor = [UIColor color:borderColorFor(NULL_COLOR)].CGColor;
    for (int i = 0; i < NUM_LAYERS; i++) {
        _layers[i] = [[CALayer alloc] init];
        _layers[i].backgroundColor = nullColor;
        _layers[i].borderWidth = 1.5;
        _layers[i].borderColor = borderColor;
        _layerColors[i] = NULL_COLOR;
    }
    _directionColors[TL] = SALMON;
    _directionColors[TR] = TEAL;
    _directionColors[BL] = PANSY_PINK;
    _directionColors[BR] = GOLDENROD;
    _directionColors[CENTER] = NULL_COLOR;
    UIView *sView = self.view;
    CGSize size = sView.bounds.size;
    _layers[TL].frame = CGRectMake(
        0, 0, size.width / 2, size.height / 2
    );
    _layers[TR].frame = CGRectMake(
        size.width / 2, 0, size.width / 2, size.height / 2
    );
    _layers[BL].frame = CGRectMake(
        0, size.height / 2, size.width / 2, size.height / 2
    );
    _layers[BR].frame = CGRectMake(
        size.width / 2, size.height / 2, size.width / 2, size.height / 2
    );
    _layers[CENTER].frame = CGRectMake(
        size.width / 3, size.height / 3, size.width / 3, size.height / 3
    );
    CALayer *sLayer = sView.layer;
    for (int i = 0; i < NUM_LAYERS; i++) {
        [sLayer addSublayer:_layers[i]];
    }
    UILongPressGestureRecognizer *longPanSender = [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(_longPress:)];
    longPanSender.minimumPressDuration = 0.03;
    longPanSender.allowableMovement = CGFLOAT_MAX;
    [sView addGestureRecognizer:longPanSender];
    sView.userInteractionEnabled = YES;
    _invalids[TL] = _invalids[BR] = 1 << TR | 1 << BL | 1 << CENTER;
    _invalids[TR] = _invalids[BL] = 1 << TL | 1 << BR | 1 << CENTER;
    _invalids[CENTER] = 0xf;
}

- (void)_showTutorialLabel
{
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    UILabel *tutorialLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(bounds.size.width - 80, 0, 80, 40)
    ];
    tutorialLabel.attributedText = [[NSAttributedString alloc]
        initWithString:@"👶🏼✅"
        attributes:textAttributes(36)
    ];
    [sView addSubview:tutorialLabel];
}

- (void)_longPress:(UILongPressGestureRecognizer *)sender
{
    UIView *sView = self.view;
    CGPoint location = [sender locationInView:sView];
    if (sender.state == UIGestureRecognizerStateBegan) {
        _startLocation = location;
        for (_currentIndex = CENTER; !CGRectContainsPoint(_layers[_currentIndex].frame, location); _currentIndex--);
        [_downHints[_currentIndex] stop];
    }
    int direction = getDirection(location, _startLocation);
    switch (sender.state) {
    case UIGestureRecognizerStateBegan:
        [_overHint start];
        _overHint.validDirections = [self _validDirections];
        _overHint.baseCenter = location;
        [self _colorWithDirection:direction];
        break;
    case UIGestureRecognizerStateChanged:
        [self _colorWithDirection:direction];
        break;
    case UIGestureRecognizerStateEnded:
        [_overHint stop];
        [self _colorWithDirection:direction];
        if (_invalids[_currentIndex] || direction == CENTER) {
            [_downHints[_currentIndex] start];
        }
        [self _checkSolved:sender];
    case UIGestureRecognizerStateCancelled:
        break;
    }
    _overHint.center = location;
    _overHint.direction = direction;
}

- (uint8_t)_validDirections
{
    uint8_t validDirections = 0xF;
    for (uint8_t direction = 0 ; direction < 4; direction++) {
        color_t color = _directionColors[direction];
        for (uint8_t layer = 0; layer < NUM_LAYERS; layer++) {
            if (layer == _currentIndex) {
                continue;
            }
            if (!isAdjacent(layer, _currentIndex)) {
                continue;
            }
            if (_layerColors[layer] == color) {
                validDirections &= ~(1 << direction);
                break;
            }
        }
    }
    return validDirections;
}

- (void)_colorWithDirection:(int)direction
{
    if (_layerColors[_currentIndex] == _directionColors[direction]) {
        return;
    }
    _layerColors[_currentIndex] = _directionColors[direction];
    _layers[_currentIndex].backgroundColor = [UIColor color:_layerColors[_currentIndex]].CGColor;
    CGColorRef borderColor = [UIColor color:borderColorFor(_directionColors[direction])].CGColor;
    for (int i = 0; i < NUM_LAYERS; i++) {
        if (i == _currentIndex) {
            continue;
        }
        if (!isAdjacent(i, _currentIndex)) {
            continue;
        }
        BOOL valid = _layerColors[i] != _layerColors[_currentIndex];
        if (valid) {
            bitmap_unset(&_invalids[i], _currentIndex);
            bitmap_unset(&_invalids[_currentIndex], i);
            if (!_invalids[i]) {
                // remove border
                _layers[i].borderWidth = 0;
                if (_layerColors[i] != NULL_COLOR) {
                    [_downHints[i] stop];
                }
            }
        } else {
            if (!_invalids[i]) {
                // add border
                _layers[i].borderWidth = 1.5;
                _layers[i].borderColor = borderColor;
                [_downHints[i] start];
            }
            bitmap_set(&_invalids[i], _currentIndex);
            bitmap_set(&_invalids[_currentIndex], i);
        }
    }
    if (_invalids[_currentIndex]) {
        // add border
        _layers[_currentIndex].borderWidth = 1.5;
        _layers[_currentIndex].borderColor = borderColor;
    } else {
        // remove border
        _layers[_currentIndex].borderWidth = 0;
        [_downHints[_currentIndex] stop];
    }
    BOOL invalid = _invalids[_currentIndex] && direction != CENTER;
    sounds_play(invalid ? SWIPE : SPLASH);
}

- (void)_checkSolved:(UIGestureRecognizer *)sender
{
    for (int i = 0; i < NUM_LAYERS; i++) {
        if (_invalids[i]) {
            return;
        }
        if (_layerColors[i] == NULL_COLOR) {
            return;
        }
    }
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    [sView removeGestureRecognizer:sender];
    // then it's solved
    UIView *whiteOverlay = [[UIView alloc]
        initWithFrame:bounds];
    whiteOverlay.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    whiteOverlay.userInteractionEnabled = YES;
    [whiteOverlay addGestureRecognizer:[[UITapGestureRecognizer alloc]
        initWithTarget:_delegate
        action:@selector(didFinishTutorial)
    ]];
    UILabel *nice = [[UILabel alloc]
        initWithFrame:CGRectMake(bounds.size.width / 2 - 25, bounds.size.height / 4 - 25, 50, 50)];
    nice.attributedText = [[NSAttributedString alloc]
        initWithString:@"🎓"
        attributes:textAttributes(48)
    ];
    [whiteOverlay addSubview:nice];

    [sView addSubview:whiteOverlay];
    _tapHint = [HintView tapHintView:sView frame:_layers[CENTER].frame showLine:YES];
    [_tapHint start];
    sounds_play(GLASS_PING);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
