@import UIKit;

@interface ColorShopCell : UICollectionViewCell
@property (nonatomic, strong) NSString *earnedTrophies;
@property (nonatomic, strong) NSDictionary *textAttributes;
@property (nonatomic, assign) BOOL locked;
- (void)setRow:(NSInteger)row;
@end
