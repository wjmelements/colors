#import <UIKit/UIKit.h>
#import "SurvivalDismisser.h"
@interface SurvivalViewController : UIViewController

+ (instancetype)shared;

@property (nonatomic, strong) UIView *whiteOverlay;
- (UIView *)icon;
- (UIView *)defeatOverlay;

@property (nonatomic, strong) SurvivalDismisser *dismisser;
@property (nonatomic, assign) BOOL enabled;

- (void)updateColorPie;

@end
