typedef enum sound {
    GLASS_PING,
    SWIPE,
    SPLASH,
    RING,
    NUM_SOUNDS
} sound_t;
void sounds_load();
void sounds_unload();
void sounds_play(sound_t sound);
void sounds_stop(sound_t sound);
