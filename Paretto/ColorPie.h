#import <UIKit/UIKit.h>
@interface ColorPie : UIView
- (void)update;
- (void)setRow:(NSInteger)row locked:(BOOL)locked;
@end
