#import "ColorGestureRecognizer.h"

#import <UIKit/UIGestureRecognizerSubclass.h>

typedef struct colorstate_internal {
    __unsafe_unretained UITouch *touch;
    NSTimeInterval timestamp;
    colorstate_t state;
    BOOL consumed;
} internal_t;

#include "vector.h"
VECTOR(internal, internals);

@implementation ColorGestureRecognizer {
    internals_t _internals;
    const color_t *_colors;
    CGFloat _tangents[2];
}

- (void)dealloc
{
    internals_destroy(&_internals);
}

- (instancetype)initWithTarget:(id)target action:(SEL)selector
{
    self = [super
        initWithTarget:target
        action:selector
    ];
    internals_init(&_internals, 4);
    return self;
}

- (colorstate_t *)next
{
    for (uint32_t i = 0; i < _internals.num_internals; i++) {
        internal_t *internal = &_internals.internals[i];
        if (internal->consumed) {
            continue;
        }
        internal->consumed = YES;
        return &internal->state;
    }
    return NULL;
}

- (point_t)_pointForLocation:(CGPoint)location
{
    point_t point;
    point.x = (uint16_t)(location.x * _scale.x);
    point.y = (uint16_t)(location.y * _scale.y);
    return point;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches
    withEvent:(UIEvent *)event
{
    UIView *sView = self.view;

    for (UITouch *touch in touches) {
        CGPoint loc = [touch locationInView:sView]; 

        internal_t internal;
        internal.touch = touch;
        internal.timestamp = touch.timestamp;
        internal.state.midPoint = [self _pointForLocation:loc];
        internal.state.group = _groupForPoint(internal.state.midPoint);
        int cancel = 0;
        for (size_t i = 0; i < _internals.num_internals; i++) {
            if (_internals.internals[i].state.group == internal.state.group
                && _internals.internals[i].state.state != UIGestureRecognizerStateEnded
                && _internals.internals[i].state.state != UIGestureRecognizerStateCancelled) {
                cancel = 1;
                break;
            }
        }
        internal.state.wasTap = NO;
        internal.state.color = NULL_COLOR;
        internal.state.state = cancel
            ? UIGestureRecognizerStateCancelled
            : UIGestureRecognizerStateBegan;
        internal.consumed = NO;
        internals_append(&_internals, internal);
        self.state = UIGestureRecognizerStateBegan;
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches
    withEvent:(UIEvent *)event
{
    BOOL changed = NO;
    UIView *sView = self.view;
    for (UITouch *touch in touches) {
        for (uint32_t i = 0; i < _internals.num_internals; i++) {
            internal_t *internal = &_internals.internals[i];
            if (internal->touch != touch) {
                continue;
            }
            if (internal->state.state == UIGestureRecognizerStateCancelled) {
                continue;
            }
            CGPoint loc = [touch locationInView:sView];
            loc.x *= _scale.x;
            loc.y *= _scale.y;
            CGPoint translation = CGPointMake(loc.x - internal->state.midPoint.x, loc.y - internal->state.midPoint.y);
            CGFloat distance = sqrt(translation.x * translation.x + translation.y * translation.y);
            color_t color;
            if (distance < 16) {
                color = NULL_COLOR;
            } else {
                if (translation.x * _tangents[0] < translation.y) {
                    if (translation.x * _tangents[1] < translation.y) {
                        color = _colors[0];
                    } else {
                        color = _colors[3];
                    }
                } else {
                    if (translation.x * _tangents[1] < translation.y) {
                        color = _colors[1];
                    } else {
                        color = _colors[2];
                    }
                }
            }
            if (color != internal->state.color) {
                internal->state.state = UIGestureRecognizerStateChanged;
                internal->state.color = color;
                internal->consumed = NO;
                changed = YES;
            }
            break;
        }
    }
    if (changed) {
        self.state = UIGestureRecognizerStateChanged;
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches
    withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        for (uint32_t i = 0; i < _internals.num_internals; i++) {
            internal_t *internal = &_internals.internals[i];
            if (internal->touch != touch) {
                continue;
            }
            if (internal->state.state == UIGestureRecognizerStateCancelled) {
                continue;
            }
            internal->state.state = UIGestureRecognizerStateEnded;
            internal->consumed = NO;
            
            NSTimeInterval uptime = [NSProcessInfo processInfo].systemUptime;
            if (uptime - internal->timestamp < 0.23) {
                internal->state.wasTap = YES;
            }
            break;
        }
    }
    [self _checkEmpty];
}

- (void)_checkEmpty
{
    // set the state to Changed or Ended based on remaining touches
    for (uint32_t i = 0; i < _internals.num_internals; i++) {
        switch (_internals.internals[i].state.state) {
            case UIGestureRecognizerStateEnded:
            case UIGestureRecognizerStateCancelled:
                continue;
            default:
                self.state = UIGestureRecognizerStateChanged;
                return;
        }
        break;
    }
    self.state = UIGestureRecognizerStateEnded;
}

- (void)reset
{
    [super reset];
    _internals.num_internals = 0;
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches
    withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        for (uint32_t i = 0; i < _internals.num_internals; i++) {
            internal_t *internal = &_internals.internals[i];
            if (internal->touch != touch) {
                continue;
            }
            internal->state.state = UIGestureRecognizerStateCancelled;
            internal->consumed = NO;
        }
    }
    [self _checkEmpty];
}

- (void)setColors:(const color_t *)colors
{
    _colors = colors;
}

- (const color_t *)colors
{
    return _colors;
}

- (void)setAngle:(CGFloat)angle
{
    _tangents[0] = tan(angle - M_PI_2);
    _tangents[1] = tan(angle);
}

@end
