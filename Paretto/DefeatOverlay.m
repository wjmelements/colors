#import "DefeatOverlay.h"

#import "IntegralCounter.h"
#import "UIColor+C.h"

NSString *const scoreStrings[NUM_SCORE_LEVELS] = {
    @"💀",
    @"💩",
    @"🍎",
    @"🎩",
    @"💎",
    @"🐲",
    @"👑",
    @"🔬",
};

NSString *scoreStringForScore(uint64_t score)
{
    score_level_t scoreLevel = scoreLevelForScore(score);
    return scoreStrings[scoreLevel];
}

@implementation DefeatOverlay
- (instancetype)
    initWithFrame:(CGRect)frame
    score:(uint64_t)score
    dismissButton:(UIButton *)dismissButton
{
    self = [super initWithFrame:frame];
    NSDictionary *attributes = @{
        NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Thin" size:48],
        NSKernAttributeName : @2
    };
    IntegralCounter *scoreNumeric = [[IntegralCounter alloc]
        initWithFrame:self.bounds
    ];
    scoreNumeric.attributedFormatBlock = ^NSAttributedString *(uint64_t value) {
        return [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%llu", score]
            attributes:attributes
        ];
    };
    scoreNumeric.textAlignment = NSTextAlignmentCenter;
    [scoreNumeric countFromZeroTo:score withDuration:0.4];
    scoreNumeric.attributedText = scoreNumeric.attributedFormatBlock(0);
    [self addSubview:scoreNumeric];
    [self addSubview:dismissButton];
    CGFloat dismissWidth = CGRectGetWidth(dismissButton.bounds);
    CGFloat dismissHeight = CGRectGetHeight(dismissButton.bounds);
    [scoreNumeric sizeToFit];
    scoreNumeric.center = CGPointMake(
        CGRectGetWidth(self.bounds) / 2,
        CGRectGetHeight(self.bounds) * .175
    );
    dismissButton.frame = CGRectMake(
        CGRectGetWidth(self.bounds) - dismissWidth,
        CGRectGetHeight(self.bounds) - dismissHeight,
        dismissWidth,
        dismissHeight
    );
    self.userInteractionEnabled = NO;
    dispatch_after(
        dispatch_time(DISPATCH_TIME_NOW, 4E8),
        dispatch_get_main_queue(), ^{
            NSString *scoreString = scoreStringForScore(score);
            UILabel *scoreLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            scoreLabel.attributedText = [[NSAttributedString alloc]
                initWithString:scoreString
                attributes:attributes
            ];
            scoreLabel.textAlignment = NSTextAlignmentCenter;
            scoreLabel.frame = CGRectOffset(scoreNumeric.frame, 0, scoreNumeric.bounds.size.height);
            [self addSubview:scoreLabel];
            self.userInteractionEnabled = YES;
        }
    );
    return self;
}
@end
