#define CORNER_RADIUS 4.0f
#define stopWatchAdjustment 1.15
#define iconSize 48
extern CGFloat minScale;
static inline CGRect shadowRect(CGRect rect) {
    rect.origin.x -= 0.5;
    rect.size.width += 1.0;
    rect.size.height += 2.5;
    return rect;
}
static inline void tease(UIView *toTease)
{
    // teaser animation
    [UIView
        animateWithDuration:0.1
        animations:^{
            toTease.transform = CGAffineTransformMakeScale(1.05, 1.05);
            for (UIView *subview in toTease.subviews) {
                subview.alpha *= 0.7;
            }
        }
        completion:^(BOOL finished) {
            [UIView
                animateWithDuration:0.1
                animations:^{
                    toTease.transform = CGAffineTransformIdentity;
                for (UIView *subview in toTease.subviews) {
                    subview.alpha /= 0.7;
                }
                }];
        }];
}
