#import "PuzzleDismisser.h"
@interface PuzzleViewController : UIViewController

- (void)setPuzzleDismisser:(PuzzleDismisser *)puzzleDismisser;

+ (instancetype)shared;

- (void)willEnterForeground;
- (void)willResignActive;
- (void)loadAnotherPuzzle;

- (void)hideTapHint;

@end
