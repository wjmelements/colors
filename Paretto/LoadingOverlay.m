#import "LoadingOverlay.h"
#import "UserManager.h"
#import "PuzzleViewController.h"
#import "UIColor+C.h"

#include "puzzlemanager.h"

static NSString *const timeEmojis = @"🕧🕐🕜🕑🕝🕒🕞🕓🕟🕔🕠🕕🕡🕖🕢🕗🕣🕘🕤🕙🕥🕚🕦🕛";

NSString *timeEmojiForTime(double time) {
    // TODO if these are all contiguous in unicode, can calculate instead
    long int emojiIndex = lround(time) * 2 % timeEmojis.length;
    return [timeEmojis substringWithRange:NSMakeRange(emojiIndex, 2)];
}

NSString *scoreEmojiForScore(uint32_t attempt, uint64_t streak) {
    double score = 2000.0 / attempt;
    NSString *scoreText = nil;
    if (score >= 90.0) {
        if (score >= 100.0) {
            if (streak > 2) {
                if (streak > 9) {
                    if (streak > 14) {
                        if (streak > 24) {
                            scoreText = @"🌸";
                        } else {
                            scoreText = @"🌺";
                        }
                    } else {
                        scoreText = @"🍀";
                    }
                } else {
                    scoreText = @"💝";
                }
            } else {
                scoreText = @"💖";
            }
        } else {
            scoreText = @"💛";
        }
    } else {
        if (score >= 70.0) {
            scoreText = @"❤️";
        } else {
            scoreText = @"💔";
        }
    }
    return scoreText;
}

NSString *const difficultyTexts[4] = { @"🐣", @"🐓", @"🐊", @"🐉" };

@interface LoadingOverlay ()
- (void)_updateAvailability;
@end

static __weak LoadingOverlay *subscriber;

void onAvailabilityChanged() {
    if (subscriber) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [subscriber _updateAvailability];
            [[PuzzleViewController shared] loadAnotherPuzzle];
        });
    }
}

@implementation LoadingOverlay {
    UILabel *_scoreLabel;
    UILabel *_timeLabel;
    UISegmentedControl *_difficultyView;
}

- (instancetype)initWithFrame:(CGRect)frame showSwitch:(BOOL)showSwitch
{
    self = [super initWithFrame:frame];

    _scoreLabel = [[UILabel alloc] initWithFrame:self.bounds];
    _scoreLabel.transform = CGAffineTransformMakeTranslation(0, self.center.y * -.35);
    _scoreLabel.numberOfLines = 1;
    _scoreLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:_scoreLabel];
    UIImageView *target = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PT"]];
    target.frame = CGRectMake(3,6,43,43); // coded so hard
    [_scoreLabel addSubview:target];

    _timeLabel = [[UILabel alloc] initWithFrame:self.bounds];
    _timeLabel.transform = CGAffineTransformMakeTranslation(0, self.center.y * -.65);
    _timeLabel.numberOfLines = 1;
    _timeLabel.textColor = [UIColor color:interpolate(NULL_COLOR, BLACK, 3)];
    [self addSubview:_timeLabel];

    if (showSwitch) {
        [self _makeDifficultyControl];
        [self addSubview:_difficultyView];
    }

    return self;
}

- (void)update
{
    if (_puzzleAttempt) {
        _timeLabel.attributedText = nil;
        int bestTime = _puzzleTime == piToggle(user.piBestTime.value)
            || _puzzleTime == piToggle(user.piBestTimes[_puzzleDifficulty].value);
        if (_puzzleTime < 99.95 || bestTime) {
            NSString *timeText = [NSString stringWithFormat:@"%@ %4.1f",
                timeEmojiForTime(_puzzleTime),
                _puzzleTime];
            if (bestTime) {
                timeText = [timeText stringByAppendingString:@" 💫"];
            }
            _timeLabel.attributedText = [[NSAttributedString alloc] initWithString:timeText attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Thin" size:48]
            }];
        }
        NSString *scoreText = scoreEmojiForScore(_puzzleAttempt, _puzzleStreak);
        NSString *difficultyText = difficultyTexts[_puzzleDifficulty];
        _scoreLabel.attributedText = [[NSAttributedString alloc]
            initWithString:[difficultyText stringByAppendingString:scoreText]
            attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:48]
            }];
    }
    _scoreLabel.alpha = 1;
    if (_timeLabel.attributedText) {
        [_timeLabel sizeToFit];
        _timeLabel.center = self.center;
        _scoreLabel.frame = CGRectOffset(_timeLabel.frame, 0, _timeLabel.frame.size.height);
    } else if (_scoreLabel.attributedText) {
        [_scoreLabel sizeToFit];
        CGRect frame = _scoreLabel.frame;
        frame.size.width *= 1.5;
        _scoreLabel.frame = frame;
        _scoreLabel.center = self.center;
    } else {
        _scoreLabel.alpha = 0;
    }
}

- (void)_makeDifficultyControl
{
    if (_difficultyView) {
        return;
    }
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:NUM_DIFFICULTIES];
    for (difficulty_t i = 0; i < NUM_DIFFICULTIES; i++) {
        [items addObject:difficultyTexts[i]];
    }
    UISegmentedControl *control = [[UISegmentedControl alloc] initWithItems:items];
    control.tintColor = [UIColor blackColor];
    control.backgroundColor = [UIColor color:NULL_COLOR];
    control.selectedSegmentIndex = user.preferredDifficulty;
    control.layer.cornerRadius = 5;
    CGFloat segmentWidth = roundl(CGRectGetWidth(self.bounds) / (NUM_DIFFICULTIES + 2));
    for (difficulty_t i = 0; i < NUM_DIFFICULTIES; i++) {
        [control setWidth:segmentWidth forSegmentAtIndex:i];
    }
    CGRect frame = control.frame;
    frame.size.height = 40;
    control.frame = frame;
    [control setTitleTextAttributes:@{
            NSFontAttributeName : [UIFont systemFontOfSize:24]
        }
        forState:UIControlStateNormal];
    [control addTarget:self
                action:@selector(_changeDifficulty:)
      forControlEvents:UIControlEventValueChanged];
    control.center = CGPointMake(self.center.x, CGRectGetHeight(self.bounds) * .7);
    _difficultyView = control;
    subscriber = self;
    [self _updateAvailability];
}

- (void)_updateAvailability
{
    availability_t availability;
    getLatestAvailability(&availability);

    UIColor *hasColor = [UIColor color:EMERALD];
    UIColor *notColor = [UIColor color:CRIMSON];

    if (_delegate) {
        availability.bits |= 1 << [_delegate loadedDifficulty];
    }
    
    NSArray *controls = _difficultyView.subviews;
    controls = [controls sortedArrayUsingComparator:^NSComparisonResult(UIView *a, UIView *b) {
        CGRect aF = a.frame;
        CGRect bF = b.frame;
        if (aF.origin.x < bF.origin.x) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        if (aF.origin.x > bF.origin.x) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    #define hasColor(Hatch, i)\
    UIView *view ## Hatch = controls[i];\
    if (availability.has ## Hatch) {\
        [view ## Hatch setTintColor:hasColor];\
    } else {\
        [view ## Hatch setTintColor:notColor];\
    }
    hasColor(Hatch, 0);
    hasColor(Chicken, 1);
    hasColor(Alligator, 2);
    hasColor(Dragon, 3);
    #undef hasColor
}

- (void)_changeDifficulty:(UISegmentedControl *)control
{
    user.preferredDifficulty = (difficulty_t)control.selectedSegmentIndex;
    [_delegate loadAnotherPuzzle];
}

@end
