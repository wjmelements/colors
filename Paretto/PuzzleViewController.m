#import "PuzzleView.h"
#import "PuzzleViewController.h"

@implementation PuzzleViewController {
    PuzzleView *_puzzleView;
    BOOL _warned;
    BOOL _backgrounded;
}

+ (instancetype)shared
{
    static PuzzleViewController *shared;
    if (!shared) {
        shared = [[PuzzleViewController alloc] init];
    }
    return shared;
}

- (void)loadView
{
    _puzzleView = [[PuzzleView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view = _puzzleView;
}

- (void)viewDidAppear:(BOOL)animated
{
    _puzzleView.pinchGestureRecognizer.enabled = NO;
    _puzzleView.panGestureRecognizer.enabled = NO;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)setPuzzleDismisser:(PuzzleDismisser *)puzzleDismisser
{
    _puzzleView.puzzleDismisser = puzzleDismisser;
}

- (void)didReceiveMemoryWarning
{
    if (_warned) {
        return;
    }
    if (_puzzleView.presented && !_backgrounded) {
        return;
    }
    _warned = YES;
    [_puzzleView memoryRelease];
    [super didReceiveMemoryWarning];
}

- (void)willResignActive
{
    _backgrounded = YES;
}

- (void)willEnterForeground
{
    _backgrounded = NO;
    if (!_warned) {
        return;
    }
    _warned = NO;
    [_puzzleView memoryRestore];
}

- (void)loadAnotherPuzzle
{
    [_puzzleView loadAnotherPuzzle];
}

- (void)hideTapHint
{
    [_puzzleView hideTapHint];
}

@end
