@import UIKit;

@protocol ColorShopDelegate
- (void)didChangeColors;
@end

@interface ColorShop : UIViewController

@property (nonatomic, strong) NSString *earnedTrophies;
@property (nonatomic, strong) NSDictionary *textAttributes;
@property (nonatomic, weak) id<ColorShopDelegate> delegate;

@end
