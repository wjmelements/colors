#import "UIColor+C.h"

@implementation UIColor (C)

+ (UIColor *)color:(color_t)color
{
    pixel_t pixel;
    setColor(&pixel, color);
    return [UIColor colorWithRed:pixel.r/255.0 green:pixel.g/255.0 blue:pixel.b/255.0 alpha:pixel.a/255.0];
}

+ (UIColor *)color:(color_t)color alpha:(CGFloat)alpha{
    pixel_t pixel;
    setColor(&pixel, color);
    return [UIColor colorWithRed:pixel.r/255.0 green:pixel.g/255.0 blue:pixel.b/255.0 alpha:alpha];
}

@end
