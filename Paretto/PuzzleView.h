#import <UIKit/UIKit.h>

#import "PuzzleDismisser.h"

#include "difficulty_t.h"

@protocol LoadingOverlayDelegate
- (void)loadAnotherPuzzle;
- (difficulty_t)loadedDifficulty;
@end

@interface PuzzleView : UIScrollView <LoadingOverlayDelegate>

@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, assign) BOOL presented;
@property (nonatomic, strong) UIView *whiteOverlay;
@property (nonatomic, strong, readonly) UIView *icon;
@property (nonatomic, strong) PuzzleDismisser *puzzleDismisser;
@property (nonatomic, assign, readonly) difficulty_t loadedDifficulty;

- (void)memoryRelease;
- (void)memoryRestore;

- (void)updateColorPie;
- (void)loadAnotherPuzzle;

- (void)hideTapHint;

@end
