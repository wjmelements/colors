#import "SurvivalPresenter.h"

#import "SurvivalViewController.h"

@implementation SurvivalPresenter

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    SurvivalViewController *vc = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *view = vc.view;
    UIView *whiteOverlay = vc.whiteOverlay;
    UIView *icon = vc.icon;
    [[transitionContext containerView] addSubview:view];
    [UIView
        animateWithDuration:0.1
        animations:^{
            icon.alpha = 0.0;
        }];
    [UIView
        animateWithDuration:0.2
        animations:^{
            whiteOverlay.alpha = 0.0;
            view.transform = CGAffineTransformIdentity;
            view.layer.cornerRadius = 0;
        }
        completion:^(BOOL finished) {
            [whiteOverlay removeFromSuperview];
            vc.enabled = YES;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.2;
}

@end
