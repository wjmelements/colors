#import "PuzzleHighScoresView.h"
#import "PuzzleDifficultyScoresView.h"
#import "LoadingOverlay.h"
#import "PieChart.h"
#import "UIColor+C.h"
#import "UserManager.h"

#include "group_t.h"
#include "style.h"

@implementation PuzzleHighScoresView {
    UILabel *_latestPuzzles[9];
}
- (instancetype)initWithFrame:(CGRect)frame attributes:(NSDictionary *)attributes
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = CORNER_RADIUS;
    const CGFloat rowHeight = frame.size.height / 9;
    const CGFloat buttonHeight = rowHeight - CORNER_RADIUS / 2;
    const CGFloat buttonOffset = (rowHeight - buttonHeight) / 2;
    const CGFloat rowWidth = frame.size.width * .62;
    const CGFloat rowOffset = 0.5;
    for (uint16_t i = 0; i < 9; i++) {
        if (!user.latestAttempts[i]) {
            break;
        }
        CGRect labelFrame = CGRectMake(rowOffset, i * rowHeight + buttonOffset, rowWidth, buttonHeight);
        _latestPuzzles[i] = [[UILabel alloc]
            initWithFrame:labelFrame
        ];
        NSString *timeEmoji = timeEmojiForTime(user.latestTimes[i]);
        NSString *difficulty = difficultyTexts[user.latestDifficulties[i]];
        NSString *heart = scoreEmojiForScore(user.latestAttempts[i], user.latestStreaks[i]);
        if (user.latestTimes[i] >= 100) {
            _latestPuzzles[i].attributedText = [[NSAttributedString alloc]
                initWithString:[NSString stringWithFormat:@"%@%@", difficulty, heart]
                attributes:attributes];
            _latestPuzzles[i].textAlignment = NSTextAlignmentCenter;
        } else {
            _latestPuzzles[i].attributedText = [[NSAttributedString alloc]
                initWithString:[NSString stringWithFormat:@"%@%@%@", difficulty, heart, timeEmoji]
                attributes:attributes];
            _latestPuzzles[i].textAlignment = NSTextAlignmentLeft;
            UILabel *puzzleTime = [[UILabel alloc]
                initWithFrame:CGRectMake(0, 0, labelFrame.size.width, labelFrame.size.height)];
            puzzleTime.attributedText = [[NSAttributedString alloc]
                initWithString:[NSString stringWithFormat:@"%4.1f", user.latestTimes[i]]
                attributes:attributes];
            puzzleTime.textAlignment = NSTextAlignmentRight;
            [_latestPuzzles[i] addSubview:puzzleTime];
        }
        _latestPuzzles[i].backgroundColor = [UIColor whiteColor];
        UIView *shadow = [[UIView alloc]
            initWithFrame:shadowRect(labelFrame)
        ];
        shadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
        shadow.layer.masksToBounds = _latestPuzzles[i].layer.masksToBounds = YES;
        shadow.layer.cornerRadius = _latestPuzzles[i].layer.cornerRadius = CORNER_RADIUS;
        [self addSubview:shadow];
        [self addSubview:_latestPuzzles[i]];
        _latestPuzzles[i].userInteractionEnabled = YES;
        [_latestPuzzles[i] addGestureRecognizer:[[UITapGestureRecognizer alloc]
            initWithTarget:self
            action:@selector(rowTapped:)
        ]];
    }
    CGFloat remainingWidth = frame.size.width - rowWidth - rowOffset - CORNER_RADIUS;
    CGFloat remainingX = frame.size.width - remainingWidth;
    CGRect pieFrame = CGRectInset(CGRectMake(remainingX, 0, remainingWidth, remainingWidth), CORNER_RADIUS, CORNER_RADIUS);
    PieChart *pie = [[PieChart alloc]
        initWithFrame:pieFrame
    ];
    [self addSubview:pie];
    CGFloat remainingY = CGRectGetMaxY(pieFrame);
    uint64_t streak = piXor(user.piLongestStreak);
    if (streak > 2) {
        UILabel *streakRecord = [[UILabel alloc]
            initWithFrame:CGRectMake(remainingX, remainingY + CORNER_RADIUS, remainingWidth, rowHeight)
        ];
        streakRecord.attributedText = [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%llu%@", streak, scoreEmojiForScore(NUM_GROUPS, streak)]
            attributes:attributes];
        streakRecord.textAlignment = NSTextAlignmentRight;
        [self addSubview:streakRecord];
        remainingY = CGRectGetMaxY(streakRecord.frame);
    }
    for (difficulty_t difficulty = HATCH_DIFFICULTY; difficulty < NUM_DIFFICULTIES; difficulty++) {
        double difficultyBestTime = piToggle(user.piBestTimes[difficulty].value);
        if (difficultyBestTime < 99.95) {
            CGRect rowFrame = CGRectMake(remainingX, remainingY, remainingWidth, rowHeight);
            PuzzleDifficultyScoresView *difficultyRow = [[PuzzleDifficultyScoresView alloc]
                initWithFrame:rowFrame
                difficulty:difficulty
                difficultyBestTime:difficultyBestTime
                attributes:attributes
            ];
            difficultyRow.backgroundColor = [UIColor whiteColor];
            CGRect shadowFrame = shadowRect(rowFrame);
            UIView *shadow = [[UIView alloc]
                initWithFrame:shadowFrame];
            shadow.backgroundColor = [UIColor color:interpolate(BLACK, BEIGE, 2)];
            shadow.layer.masksToBounds = difficultyRow.layer.masksToBounds = YES;
            shadow.layer.cornerRadius = difficultyRow.layer.cornerRadius = CORNER_RADIUS;
            [self addSubview:shadow];
            [self addSubview:difficultyRow];
            difficultyRow.userInteractionEnabled = YES;
            [difficultyRow addGestureRecognizer:[[UITapGestureRecognizer alloc]
                initWithTarget:self
                action:@selector(difficultyRowTapped:)
            ]];
            remainingY = CGRectGetMaxY(shadowFrame);
        }
    }
    return self;
}

- (void)difficultyRowTapped:(UITapGestureRecognizer *)sender
{
    PuzzleDifficultyScoresView *puzzleDifficultyScoresView = sender.view;
    difficulty_t difficulty = puzzleDifficultyScoresView.difficulty;
    UIImage *solvedImage = loadBestTimedPuzzleForDifficulty(difficulty);
    LoadingOverlay *loadingOverlay = [self showPuzzleOverlay:solvedImage];
    loadingOverlay.puzzleTime = piToggle(user.piBestTimes[difficulty].value);
    loadingOverlay.puzzleAttempt = user.difficultyBestTimesAttempts[difficulty];
    loadingOverlay.puzzleDifficulty = difficulty;
    loadingOverlay.puzzleStreak = user.difficultyBestTimesStreaks[difficulty];
    [loadingOverlay update];
}
- (void)rowTapped:(UITapGestureRecognizer *)sender
{
    UIView *button = sender.view;
    int index;
    for (index = 0; index < 9; index++) {
        if (button == _latestPuzzles[index]) {
            break;
        }
    }
    UIImage *solvedImage = loadSolvedPuzzle(index);
    LoadingOverlay *loadingOverlay = [self showPuzzleOverlay:solvedImage];
    loadingOverlay.puzzleTime = user.latestTimes[index];
    loadingOverlay.puzzleAttempt = user.latestAttempts[index];
    loadingOverlay.puzzleDifficulty = user.latestDifficulties[index];
    loadingOverlay.puzzleStreak = user.latestStreaks[index];
    [loadingOverlay update];
}
- (LoadingOverlay *)showPuzzleOverlay:(UIImage *)solvedImage
{
    UIImageView *solvedImageView = [[UIImageView alloc]
        initWithImage:solvedImage];
    solvedImageView.userInteractionEnabled = YES;
    UIView *superview = self.superview;
    solvedImageView.frame = superview.bounds;
    UIView *whiteOverlay = [[UIView alloc]
        initWithFrame:superview.bounds];
    whiteOverlay.backgroundColor = [UIColor color:WHITE alpha:0.5];
    LoadingOverlay *loadingOverlay = [[LoadingOverlay alloc]
        initWithFrame:superview.bounds showSwitch:NO];
    loadingOverlay.loading = NO;
    [superview addSubview:solvedImageView];
    [solvedImageView addSubview:whiteOverlay];
    [solvedImageView addSubview:loadingOverlay];
    [solvedImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(_puzzleTapped:)
    ]];
    [[SettingsViewController shared] overrideStatusBarHidden:YES];
    return loadingOverlay;
}

- (void)_puzzleTapped:(UITapGestureRecognizer *)sender
{
    [sender.view removeFromSuperview];
    [[SettingsViewController shared] unsetStatusBarOverride];
}
@end
