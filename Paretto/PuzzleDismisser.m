#import "PuzzleDismisser.h"
#import "PuzzleView.h"

#include "style.h"

@implementation PuzzleDismisser {
    id<UIViewControllerContextTransitioning> _transitionContext;
    __weak SettingsViewController *_presenter;
}

- (instancetype)initWithPresenter:(SettingsViewController *)presenter
{
    self = [super init];
    _presenter = presenter;
    return self;
}

- (void)setDismissing:(BOOL)dismissing
{
    if (_dismissing == dismissing) {
        return;
    }
    _dismissing = dismissing;
    if (dismissing) {
        [_presenter dismissViewControllerAnimated:YES completion:nil];
    } else {
        [_transitionContext cancelInteractiveTransition];
        [_transitionContext completeTransition:NO];
        _transitionContext = nil;
    }
}

- (void)dismiss
{
    if (!_transitionContext) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismiss];
        });
        return;
    }
    PuzzleView *fromView = [_transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *icon = fromView.icon;
    icon.alpha = 0.0;
    [UIView animateWithDuration:.3
        animations:^{
            fromView.transform = CGAffineTransformConcat(
                CGAffineTransformMakeScale(minScale, minScale),
                CGAffineTransformMakeTranslation(fromView.frame.size.width*-(1+minScale)/6, .25*fromView.frame.size.height)
            );
            fromView.layer.cornerRadius = CORNER_RADIUS / minScale;
        }
        completion:^(BOOL finished){
            [_transitionContext completeTransition:YES];
            UIView *backgroundView = [_transitionContext viewForKey:UITransitionContextToViewKey];
            [backgroundView addSubview:fromView];
            [_presenter puzzleDismisserDidDismiss:self];
            [UIView
                animateWithDuration:0.1
                animations:^{
                    icon.alpha = 1.0;
                }];
        }];
}

- (void)startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    _transitionContext = transitionContext;
    UIView *backgroundView = [_transitionContext viewForKey:UITransitionContextToViewKey];
    [[_transitionContext containerView] insertSubview:backgroundView atIndex:0];
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    // not called
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    // ignored
    return 1;
}

@end
