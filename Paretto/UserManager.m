#include "puzzlemanager.h"
#include "UserManager.h"

#include <UIKit/UIKit.h>
user_t user;

static NSURL *documentURL()
{
    static NSURL *documentURL = NULL;
    if (!documentURL) {
        documentURL =  [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    }
    return documentURL;
}

static NSString *const USER_FILE_NAME = @"ParettoU.PU";
static NSString *const SOLVED_FILE_NAME = @"ParettoS.png";
static NSString *const DIFFICULTY_FILE_NAME = @"ParettoD.png";

static inline NSString *userPath()
{
    NSURL *url = documentURL();
    return [[url URLByAppendingPathComponent:USER_FILE_NAME] path];
}

static inline NSString *puzzlePath(uint8_t index)
{
    NSURL *url = documentURL();
    return [[url URLByAppendingPathComponent:[NSString stringWithFormat:@"%u", index]] path];
}

void saveUser()
{
    NSData *data = [NSData
        dataWithBytesNoCopy:&user
                     length:sizeof(user)
               freeWhenDone:NO
    ];
    NSString *path = userPath();
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createFileAtPath:path
                     contents:data
                   attributes:nil /*TODO*/];
}

void loadUser()
{
    NSData *data = [NSData
        dataWithContentsOfFile:userPath()];
    memset(&user, 0, sizeof(user));
    [data getBytes:&user length:sizeof(user)];
    if (!data) {
        user.piBestTime.value = piToggle(INFINITY);;
        for (difficulty_t d = 0; d < NUM_DIFFICULTIES; d++) {
            user.piBestTimes[d].value = piToggle(INFINITY);
        }
    }
    user_upgrade(&user);
}

void savePuzzles() {
    puzzle_t puzzle = getNextPuzzle();
    uint8_t i = 0;
    while (puzzle) {
        NSData *data = [NSData
            dataWithBytesNoCopy:puzzle
                        length:fileSizeOf(puzzle)
                        freeWhenDone:YES];
        NSString *path = puzzlePath(i);
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager createFileAtPath:path
                         contents:data
                    attributes:nil /*TODO*/];
        puzzle = getNextPuzzle();
    }
}

void loadPuzzles() {
    for (uint8_t i = 0; ; i++) {
        // TODO remove files
        NSString *path = puzzlePath(i);
        NSFileHandle *fh = [NSFileHandle fileHandleForReadingAtPath:path];
        if (!fh) {
            return;
        }
        int fd = fh.fileDescriptor;
        
        puzzle_t puzzle = readPuzzle(fd);
        suggestPuzzle(puzzle);
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}

static NSString *solvedPath(uint16_t relativeIndex) {
    NSURL *documents = documentURL();
    return [[documents URLByAppendingPathComponent:[SOLVED_FILE_NAME stringByAppendingFormat:@"%u", relativeIndex]] path];
}

static NSString *difficultyPath(difficulty_t difficulty) {
    NSURL *documents = documentURL();
    return [[documents URLByAppendingPathComponent:[DIFFICULTY_FILE_NAME stringByAppendingFormat:@"%u", difficulty]] path];
}

void saveSolvedPuzzle(UIImage *image, difficulty_t difficulty, int wasBestTime) {
    NSData *data = UIImagePNGRepresentation(image);
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *path = solvedPath(8);
    for (uint16_t i = 8; i --> 0;) {
        NSString *nextPath = solvedPath(i);
        [fm moveItemAtPath:nextPath toPath:path error:nil];
        path = nextPath;
    }
    [data writeToFile:path atomically:YES];
    if (wasBestTime) {
        NSString *dPath = difficultyPath(difficulty);
        [fm removeItemAtPath:dPath error:nil];
        [fm linkItemAtPath:path toPath:dPath error:nil];
    }
}

UIImage *loadSolvedPuzzle(uint16_t relativeIndex) {
    return [UIImage imageWithContentsOfFile:solvedPath(relativeIndex)];
}
UIImage *loadBestTimedPuzzleForDifficulty(difficulty_t difficulty)
{
    return [UIImage imageWithContentsOfFile:difficultyPath(difficulty)];
}
