#import "AppDelegate.h"

#import "SettingsViewController.h"
#import "SurvivalViewController.h"
#import "PuzzleViewController.h"
#import "TutorialViewController.h"
#import "PuzzleTutorialViewController.h"
#import "UserManager.h"

#include "puzzlemanager.h"
#include "sounds.h"

@implementation AppDelegate

static AppDelegate *sInstance;
+ (instancetype)shared
{
    return sInstance;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    sInstance = self;
    startPuzzleManager();
    loadUser();
    dispatch_async(dispatch_get_main_queue(), ^{
        // FIXME this is I/O on the main thread
        loadPuzzles();
        sounds_load();
    });
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    UIWindow *window = [[UIWindow alloc] initWithFrame:screenBounds];
    UIViewController *vc = nil;
    if (user.header.hasCompletedTutorial) {
        vc = [SettingsViewController shared];
    } else {
        vc = [[TutorialViewController alloc] initWithDelegate:self];
    }
    [window setRootViewController:vc];
    [window makeKeyAndVisible];
    _window = window;
    return YES;
}

- (void)showTutorial
{
    UIViewController *vc = [[TutorialViewController alloc] initWithDelegate:self];
    _window.rootViewController = vc;
}

- (void)didFinishTutorial
{
    if ([_window.rootViewController isKindOfClass:[TutorialViewController class]]) {
        UIViewController *vc = [[PuzzleTutorialViewController alloc] initWithDelegate:self];
        _window.rootViewController = vc;
    } else {
        user.header.hasCompletedTutorial = 1;
        _window.rootViewController = [SettingsViewController shared];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    saveUser();
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    pausePuzzleManager(0);
    [[PuzzleViewController shared] willResignActive];
    dispatch_async(dispatch_get_main_queue(), ^{
        savePuzzles();
    });
    sounds_unload();
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[PuzzleViewController shared] willEnterForeground];
    dispatch_async(dispatch_get_main_queue(), ^{
        loadPuzzles();
        resumePuzzleManager();
        sounds_load();
    });
}

@end
