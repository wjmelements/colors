#import "SurvivalDismisser.h"

#import "SurvivalViewController.h"

#include "style.h"

@implementation SurvivalDismisser {
    SettingsViewController *_presenter;
}

- (instancetype)initWithPresenter:(SettingsViewController *)presenter
{
    self = [super init];
    _presenter = presenter;
    return self;
}

- (void)dismiss
{
    [_presenter dismissViewControllerAnimated:YES completion:nil];
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    SurvivalViewController *survivalViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *whiteOverlay = survivalViewController.whiteOverlay;
    whiteOverlay.alpha = 0.0;
    UIView *defeatOverlay = survivalViewController.defeatOverlay;
    if (defeatOverlay) {
        [survivalViewController.view insertSubview:whiteOverlay belowSubview:defeatOverlay];
    } else {
        [survivalViewController.view addSubview:whiteOverlay];
    }
    UIView *icon = survivalViewController.icon;
    icon.alpha = 0.0;
    UIView *backgroundView = [transitionContext viewForKey:UITransitionContextToViewKey];
    [[transitionContext containerView] addSubview:backgroundView];
    [backgroundView addSubview:fromView];
    [UIView animateWithDuration:.2
        animations:^{
            whiteOverlay.alpha = 1.0;
            fromView.transform = CGAffineTransformConcat(
                CGAffineTransformMakeScale(minScale, minScale),
                CGAffineTransformMakeTranslation(fromView.bounds.size.width*(1+minScale)/6, .25*fromView.bounds.size.height)
            );
            fromView.layer.cornerRadius = CORNER_RADIUS / minScale;
        }
        completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
            [backgroundView addSubview:fromView];
            [UIView
                animateWithDuration:0.1
                animations:^{
                    icon.alpha = 1.0;
                }];
            [_presenter survivalDismisserDidDismiss:self];
        }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return .3;
}

@end
