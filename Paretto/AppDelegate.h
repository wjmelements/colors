#import <UIKit/UIKit.h>

#import "TutorialDelegate.h"

@interface AppDelegate : UIResponder <TutorialDelegate, UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (instancetype)shared;
- (void)showTutorial;

@end

