#import "PuzzleDifficultyScoresView.h"
#import "LoadingOverlay.h"

@implementation PuzzleDifficultyScoresView

- (instancetype)initWithFrame:(CGRect)frame
    difficulty:(difficulty_t)difficulty
    difficultyBestTime:(double)difficultyBestTime
    attributes:(NSDictionary *)attributes;
{
    self = [super initWithFrame:frame];
    _difficulty = difficulty;
    CGRect bounds = self.bounds;
    UILabel *star = [[UILabel alloc]
        initWithFrame:bounds
    ];
    star.attributedText = [[NSAttributedString alloc]
        initWithString:@"💫"
        attributes:attributes];
    UILabel *bestTime = [[UILabel alloc]
        initWithFrame:bounds
    ];
    bestTime.attributedText = [[NSAttributedString alloc]
        initWithString:[NSString stringWithFormat:@"%4.1f%@", difficultyBestTime, difficultyTexts[_difficulty]]
        attributes:attributes];
    star.textAlignment = bestTime.textAlignment = NSTextAlignmentRight;
    [self addSubview:bestTime];
    [self addSubview:star];
    return self;
}

@end
