#include "trophies.h"
NSString *trophies[NUM_TROPHIES] = {
    @"🐣", @"🐓", @"🐊", @"🐉" ,
    @"🍎", @"🎩",
    @"💝", @"🍀", @"🌺", @"🌸",
    @"💎", @"🐲", @"👑", @"🔬",
};
