#import "ArrowLayer.h"

@implementation ArrowLayer

- (void)drawInContext:(CGContextRef)context
{
    CGRect bounds = self.bounds;
    CGFloat w = bounds.size.width / 2;
    CGFloat slantSize = w * .8;
    CGFloat p1 = (slantSize / 2) * .6;
    CGFloat p2 = slantSize - p1;
    CGFloat p3 = w + p2 - p1;

    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, slantSize, 0);
    CGContextAddLineToPoint(context, p2, p1);
    CGContextAddLineToPoint(context, p3, w);
    CGContextAddLineToPoint(context, w, p3);
    CGContextAddLineToPoint(context, p1, p2);
    CGContextAddLineToPoint(context, 0, slantSize);
    CGContextAddLineToPoint(context, 0, 0);
    CGContextClosePath(context);

    CGContextSetFillColorWithColor(context, _color.CGColor);
    CGPathDrawingMode mode = kCGPathFill;
    if (_strokeColor) {
        mode = kCGPathFillStroke;
        CGContextSetLineWidth(context, 1);
        CGContextSetStrokeColorWithColor(context, _strokeColor.CGColor);
    }
    CGContextDrawPath(context, mode);
}

- (BOOL)needsDisplayOnBoundsChange {
    return YES;
}

@end
