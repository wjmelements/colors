#import "HintView.h"

#import "ArrowLayer.h"
#import "GradientView.h"
#import "UIColor+C.h"

#include "colormap.h"

@interface HintView(Subclass)
- (instancetype)initWithFrame:(CGRect)frame
    superView:(UIView *)superView;
@end

@interface DownHintView : HintView
- (instancetype)initWithFrame:(CGRect)frame
    superView:(UIView *)superView
    showLine:(BOOL)showLine;
@end
@interface TapHintView : DownHintView
@end
@interface OverHintView : HintView
@end

@implementation HintView {
    UIView *_superView;
}

- (void)setValidDirections:(uint8_t)mask
{
    // nop
}

- (instancetype)initWithFrame:(CGRect)frame
    superView:(UIView *)superView
{
    self = [super initWithFrame:frame];
    self.userInteractionEnabled = NO;
    _superView = superView;
    return self;
}
+ (instancetype)downHintView:(UIView *)superView
    frame:(CGRect)frame showLine:(BOOL)showLine;
{
    return [[DownHintView alloc]
        initWithFrame:frame
        superView:superView
        showLine:showLine
    ];
}
+ (instancetype)tapHintView:(UIView *)superView
    frame:(CGRect)frame showLine:(BOOL)showLine
{
    return [[TapHintView alloc]
        initWithFrame:frame
        superView:superView
        showLine:showLine
    ];
}

- (void)setBaseCenter:(CGPoint)baseCenter
{
    // nop
}

- (void)setDirection:(int)direction
{
    // nop
}

static const CGFloat kOverHintRatio = 0.18;

+ (instancetype)overHintView:(UIView *)superView
{

    CGSize size = superView.bounds.size;
    return [[OverHintView alloc]
        initWithFrame:CGRectMake(0, 0, size.height * kOverHintRatio, size.height * kOverHintRatio)
        superView:superView
    ];
}

- (void)start
{
    [_superView addSubview:self];
}
- (void)stop
{
    [self removeFromSuperview];
}
@end

@implementation DownHintView {
    UILabel *_downHand;
}

#define DOWN_DISTANCE 15
- (instancetype)initWithFrame:(CGRect)frame
    superView:(UIView *)superView
    showLine:(BOOL)showLine
{
    self = [super
        initWithFrame:frame
        superView:superView];

    CGRect bounds = self.bounds;
    _downHand = [[UILabel alloc]
        initWithFrame:bounds];
    _downHand.text = @"☝🏽";
    _downHand.textAlignment = NSTextAlignmentCenter;
    _downHand.font = [UIFont systemFontOfSize:frame.size.height / 3];
    _downHand.transform = CGAffineTransformMakeRotation(M_PI);
    [_downHand sizeToFit];
    CGPoint center = _downHand.center;
    center.y -= _downHand.frame.size.height / 3;
    _downHand.center = center;
    [self addSubview:_downHand];

    if (showLine) {
        CGRect handFrame = _downHand.frame;
        #define LINE_HEIGHT 2
        CGRect line = CGRectMake(
            bounds.size.width / 3,
            handFrame.origin.y + handFrame.size.height * 0.9,
            bounds.size.width / 3,
            LINE_HEIGHT
        );
        GradientView *screenLine = [[GradientView alloc]
            initWithFrame:line];
        CAGradientLayer *layer = screenLine.gradientLayer;
        layer.colors = @[
            (__bridge id)[UIColor blackColor].CGColor,
            (__bridge id)[UIColor color:PLATINUM].CGColor,
            (__bridge id)[UIColor blackColor].CGColor
            ];
        layer.startPoint = CGPointMake(0, 0.5);
        layer.endPoint = CGPointMake(1, 0.5);
        [self addSubview:screenLine];
    }

    return self;
}

- (void)start
{
    [super start];
    [self _downToUp];
}
- (void)_upToDown
{
    [UIView
        animateWithDuration:.3
        delay:0
        options:UIViewAnimationOptionCurveEaseIn
        animations:^{
            CGPoint center = _downHand.center;
            _downHand.center = CGPointMake(center.x, center.y + DOWN_DISTANCE);
        }
        completion:^(BOOL finished){
            if (finished) {
                [self _downToUp];
            }
        }];
}
- (CGFloat)_delay
{
    return 0.7;
}
- (void)_downToUp
{
    [UIView
        animateWithDuration:.3
        delay:[self _delay]
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            CGPoint center = _downHand.center;
            _downHand.center = CGPointMake(center.x, center.y - DOWN_DISTANCE);
        }
        completion:^(BOOL finished){
            if (finished) {
                [self _upToDown];
            }
        }];
}

- (void)stop
{
    [super stop];
    [_downHand.layer removeAllAnimations];
    _downHand.frame = self.bounds;
    [_downHand sizeToFit];
    CGPoint center = _downHand.center;
    center.y -= _downHand.frame.size.height / 3;
    _downHand.center = center;
}

@end

@implementation OverHintView {
    CGPoint _baseCenter;
    CALayer *_arrows[4];
    BOOL _started;
}

static inline CGPoint makeAnchorPoint(CGRect frame, CGRect bounds) {
    CGPoint center = CGPointMake(
        frame.origin.x + frame.size.width / 2,
        frame.origin.y + frame.size.height / 2
    );
    // TODO simplify
    return CGPointMake(
        0.5 + (bounds.size.width / 2 - center.x) / frame.size.width,
        0.5 + (bounds.size.height / 2 - center.y) / frame.size.height
    );
}

- (instancetype)initWithFrame:(CGRect)frame
    superView:(UIView *)superView
{
    self = [super
        initWithFrame:frame
        superView:superView];
    
    CGRect bounds = self.bounds;
    CGSize arrowSize = CGSizeMake(bounds.size.width / 3, bounds.size.width / 3);

    ArrowLayer *topLeftArrow = [ArrowLayer layer];
    CGRect topLeftFrame = CGRectMake(0, 0, arrowSize.width, arrowSize.height);
    topLeftArrow.anchorPoint = makeAnchorPoint(topLeftFrame, bounds);
    topLeftArrow.frame = topLeftFrame;
    topLeftArrow.color = [UIColor color:SALMON];
    topLeftArrow.strokeColor = [UIColor color:borderColorFor(SALMON)];

    ArrowLayer *topRightArrow = [ArrowLayer layer];
    topRightArrow.anchorPoint = topLeftArrow.anchorPoint;
    topRightArrow.frame = topLeftFrame;
    topRightArrow.color = [UIColor color:TEAL];
    topRightArrow.strokeColor = [UIColor color:borderColorFor(TEAL)];
    topRightArrow.transform = CATransform3DMakeRotation(M_PI_2, 0, 0, 1);

    ArrowLayer *bottomLeftArrow = [ArrowLayer layer];
    bottomLeftArrow.anchorPoint = topLeftArrow.anchorPoint;
    bottomLeftArrow.frame = topLeftFrame;
    bottomLeftArrow.color = [UIColor color:PANSY_PINK];
    bottomLeftArrow.strokeColor = [UIColor color:borderColorFor(PANSY_PINK)];
    bottomLeftArrow.transform = CATransform3DMakeRotation(-M_PI_2, 0, 0, 1);

    ArrowLayer *bottomRightArrow = [ArrowLayer layer];
    bottomRightArrow.anchorPoint = topLeftArrow.anchorPoint;
    bottomRightArrow.frame = topLeftFrame;
    bottomRightArrow.color = [UIColor color:GOLDENROD];
    bottomRightArrow.strokeColor = [UIColor color:borderColorFor(GOLDENROD)];
    bottomRightArrow.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);

    CALayer *sLayer = self.layer;
    _arrows[0] = topLeftArrow;
    [sLayer addSublayer:topLeftArrow];
    _arrows[1] = topRightArrow;
    [sLayer addSublayer:topRightArrow];
    _arrows[2] = bottomLeftArrow;
    [sLayer addSublayer:bottomLeftArrow];
    _arrows[3] = bottomRightArrow;
    [sLayer addSublayer:bottomRightArrow];

    return self;
}

- (void)setBaseCenter:(CGPoint)baseCenter
{
    _baseCenter = baseCenter;
}

static inline CGFloat clamp(CGFloat x, CGFloat min, CGFloat max) {
    if (x > max) {
        return max;
    }
    if (x < min) {
        return min;
    }
    return x;
}

static inline CGFloat angleFor(CGFloat angle, CGFloat magnitude, CGFloat target) {
    angle -= M_PI_4;
    if (angle < -M_PI) {
        angle += 2 * M_PI;
    }
    if (angle < -M_PI_2) {
        angle -= 2 * (angle + M_PI_2);
    }
    if (angle > M_PI_2) {
        angle -= 2 * (angle - M_PI_2);
    }
    angle *= (magnitude / (magnitude + 125));
    angle = clamp(angle, -M_PI_4, M_PI_4);
    angle += target;
    return angle;
}

- (void)setCenter:(CGPoint)center
{
    [super setCenter:center];
    CGPoint d = CGPointMake(center.x - _baseCenter.x, center.y - _baseCenter.y);
    CGFloat magnitude = sqrt(d.x * d.x + d.y * d.y);

    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    // FIXME blue and purple are broken in their opposing direction
    _arrows[0].transform = CATransform3DMakeRotation(angleFor(atan2(d.y, d.x), magnitude, 0), 0, 0, 1);
    _arrows[1].transform = CATransform3DMakeRotation(angleFor(atan2(-d.y, d.x), magnitude, M_PI_2), 0, 0, 1);
    _arrows[2].transform = CATransform3DMakeRotation(angleFor(atan2(d.y, -d.x), magnitude, -M_PI_2), 0, 0, 1);
    _arrows[3].transform = CATransform3DMakeRotation(angleFor(atan2(-d.y, -d.x), magnitude, -M_PI), 0, 0, 1);
    [CATransaction commit];
}

- (void)setValidDirections:(uint8_t)validDirections
{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    for (uint8_t i = 0; i < 4; i++) {
        _arrows[i].opacity = validDirections & (1 << i)? 1.0 : 0.3;
    }
    [CATransaction commit];
}

- (void)setDirection:(int)direction
{
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    for (int i = 0; i < 4; i++) {
        _arrows[i].hidden = i == direction;
    }
    [CATransaction commit];
}

- (void)start
{
    [super start];
    if (!_started) {
        [self goOut];
    }
    _started = YES;
}

- (void)goOut
{
    [UIView
        animateWithDuration:0.3
        delay:0
        options:UIViewAnimationOptionCurveEaseIn
        animations:^{
            self.transform = CGAffineTransformMakeScale(1.2, 1.2);
        }
        completion:^(BOOL finished) {
            if (finished) {
                [self goIn];
            }
        }];

}

- (void)goIn
{
    [UIView
        animateWithDuration:0.3
        delay:0
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            self.transform = CGAffineTransformIdentity;
        }
        completion:^(BOOL finished) {
            if (finished) {
                [self goOut];
            }
        }];
}

- (void)stop
{
    [self.layer removeAllAnimations];
    [super stop];
    _started = NO;
}

@end

@implementation TapHintView
- (CGFloat)_delay
{
    return 0;
}
@end
