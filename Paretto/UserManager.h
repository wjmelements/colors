#include "difficulty_t.h"
#include "user.h"

@class UIImage;

extern user_t user;

void loadUser();
void saveUser();
void savePuzzles();
void loadPuzzles();

void saveSolvedPuzzle(UIImage *image, difficulty_t difficulty, int wasBestTime);
UIImage *loadSolvedPuzzle(uint16_t relativeIndex);
UIImage *loadBestTimedPuzzleForDifficulty(difficulty_t difficulty);
