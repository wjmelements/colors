#import "SurvivalHighScoresView.h"

#include "color_t.h"

#include "style.h"
#import "DefeatOverlay.h"
#import "UIColor+C.h"
#import "UserManager.h"

static int comparator(const void *one, const void *two) {
    uint64_t d1 = *(uint64_t *)one;
    uint64_t d2 = *(uint64_t *)two;
    return d2 - d1;
}

static inline CGFloat max(CGFloat a, CGFloat b) {
    return a > b ? a : b;
}

@implementation SurvivalHighScoresView {
    UILabel *_emojiViews[8];
    UILabel *_scoreViews[8];
    UILabel *_timeViews[8];
}

- (instancetype)initWithFrame:(CGRect)frame
    textAttributes:(NSDictionary *)textAttributes
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = CORNER_RADIUS;
    self.layer.masksToBounds = YES;
    struct {
        uint64_t score;
        double time;
    } survivalHighs[8];
    for (int i = 0; i < 4; i++) {
        survivalHighs[i].score = user.survivalHighScores[i];
        survivalHighs[i].time = user.survivalTimes[i];
    }
    for (int i = 4; i < 8; i++) {
        survivalHighs[i].score = user.nextSurvivalHighScores[i - 4];
        survivalHighs[i].time = user.survivalTimes[i];
    }
    qsort(&survivalHighs[0], 8, sizeof(survivalHighs[0]), comparator);
    UILabel *dummy = [[UILabel alloc]
        initWithFrame:CGRectZero
    ];
    dummy.attributedText = [[NSAttributedString alloc]
        initWithString:scoreStringForScore(survivalHighs[0].score)
        attributes:textAttributes];
    [dummy sizeToFit];
    const CGFloat ROW_SIZE = max(dummy.frame.size.width, dummy.frame.size.height);
    for (uint8_t i = 0; i < 8; i++) {
        if (survivalHighs[i].score == 0) {
            break;
        }
        _emojiViews[i] = [[UILabel alloc]
            initWithFrame:CGRectMake(0, i * ROW_SIZE, ROW_SIZE, ROW_SIZE)];
        _timeViews[i] = [[UILabel alloc]
            initWithFrame:CGRectMake(ROW_SIZE, i * ROW_SIZE, ROW_SIZE * 2.5, ROW_SIZE)
        ];
        _scoreViews[i] = [[UILabel alloc]
            initWithFrame:CGRectMake(ROW_SIZE * 3.5, i * ROW_SIZE, (frame.size.width - ROW_SIZE * 3.5), ROW_SIZE)];
        NSString *emojiI = scoreStringForScore(survivalHighs[i].score);
        NSString *scoreI = [NSString stringWithFormat:@"%llu", survivalHighs[i].score];
        double time = survivalHighs[i].time;
        int minutes = (int) (time / 60);
        // time expressed in less width than 
        NSString *timeI = !time ? nil
          : time < 10 ? [NSString stringWithFormat:@"%.2f", time]
          : time < 100 ? [NSString stringWithFormat:@"%.1f", time]
          : time < 600 ? [NSString stringWithFormat:@"%i:%02i", minutes, (int)(time - 60 * minutes)]
          : [NSString stringWithFormat:@"%i", minutes];
        _emojiViews[i].attributedText = [[NSAttributedString alloc]
            initWithString:emojiI
            attributes:textAttributes];
        _scoreViews[i].attributedText = [[NSAttributedString alloc]
            initWithString:scoreI
            attributes:textAttributes];
        if (timeI) {
            _timeViews[i].attributedText = [[NSAttributedString alloc]
                initWithString:timeI
                attributes:textAttributes];
        }
        _scoreViews[i].textAlignment = NSTextAlignmentRight;
        CGRect frame = _scoreViews[i].frame;
        _scoreViews[i].layer.anchorPoint = CGPointMake(1.0, 0.5);
        _scoreViews[i].frame = frame;
    }
    for (uint8_t i = 0; i < 8; i++) {
        [self addSubview:_emojiViews[i]];
    }
    for (uint8_t i = 0; i < 8; i++) {
        [self addSubview:_timeViews[i]];
    }
    for (uint8_t i = 0; i < 8; i++) {
        [self addSubview:_scoreViews[i]];
    }
    return self;
}

- (__strong UIView **)emojiViews
{
    return &_emojiViews[0];
}

- (__strong UIView **)scoreViews
{
    return &_scoreViews[0];
}

@end
