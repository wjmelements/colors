#import "UIGestureRecognizer+Helpers.h"
@implementation UIGestureRecognizer (Helpers)
- (void)cancel
{
    if (self.enabled) {
        self.enabled = NO;
        self.enabled = YES;
    }
}
@end
