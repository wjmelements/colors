#import "ColorPie.h"
@interface ColorPieControl : UIView
@property (nonatomic, strong) ColorPie *colorPie;
- (void)commitPieRotation:(CGFloat)rotation;
@end
