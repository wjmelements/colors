@import UIKit;

#import "TutorialDelegate.h"

@interface PuzzleTutorialViewController : UIViewController

- (instancetype)initWithDelegate:(id<TutorialDelegate>)tutorialDelegate;

@end
