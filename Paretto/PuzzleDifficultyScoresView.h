@import UIKit;

#include "difficulty_t.h"

@interface PuzzleDifficultyScoresView : UIView

- (instancetype)initWithFrame:(CGRect)frame
    difficulty:(difficulty_t)difficulty
    difficultyBestTime:(double)difficultyBestTime
    attributes:(NSDictionary *)attributes;

@property (nonatomic, assign) difficulty_t difficulty;

@end
