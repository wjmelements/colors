#include "color.h"
#include "puzzle.h"
#include "layer.h"
#include "panic.h"
#include "puzzlemanager.h"

#import "HintView.h"
#import "LoadingOverlay.h"
#import "PuzzleView.h"
#import "ColorGestureRecognizer.h"
#import "UserManager.h"
#import "UIColor+C.h"
#import "UIGestureRecognizer+Helpers.h"

#include "style.h"
#include "sounds.h"

static dispatch_queue_t puzzleQueue = 0;
#define kPuzzleScale .5

@interface PuzzleView () <UIScrollViewDelegate>
@end

@implementation PuzzleView {
    UIImageView *_imageView;
    ColorGestureRecognizer *_panRecognizer;
    UITapGestureRecognizer *_nextRecognizer;
    CGPoint _scale;

    color_t _panColorStart[NUM_GROUPS];

    LoadingOverlay *_loadingOverlay;
    UIButton *_dismissButton;
    UILabel *_icon;

    NSDate *_puzzleStart;
    NSTimeInterval _puzzleTime;
    BOOL _inactive;
    uint32_t _moveCount;
    board_t *_board;
    layer_t *_layer;
    
    UIImage *_nextImage;
    puzzle_t _nextPuzzle;
    board_t *_nextBoard;
    layer_t *_nextLayer;

    HintView *_tapHint;

    BOOL _isQuickZoomed;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!puzzleQueue) {
        puzzleQueue = dispatch_queue_create("puzzle", 0);
    }
    _loadedDifficulty = NUM_DIFFICULTIES;

    self.minimumZoomScale = kPuzzleScale;
    self.maximumZoomScale = 8;
    self.bouncesZoom = NO;
    self.clipsToBounds = NO;

    self.contentSize = frame.size;
    CGFloat width = kPuzzleScale * frame.size.width;
    CGFloat height = kPuzzleScale * frame.size.height;
    self.contentInset = UIEdgeInsetsMake(height, width, height, width);

    self.backgroundColor = [UIColor clearColor];

    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.backgroundColor = [UIColor color:NULL_COLOR];
    _imageView.userInteractionEnabled = YES;
    dispatch_async(puzzleQueue, ^{
        UIImage *image = loadSolvedPuzzle(0);
        dispatch_async(dispatch_get_main_queue(), ^{
            _imageView.image = image;
            if (!image) {
                [self _setupTapHint];
            }
            [self presentLoadingOverlay];
            _loadingOverlay.puzzleTime = user.latestTimes[0];
            _loadingOverlay.puzzleAttempt = user.latestAttempts[0];
            _loadingOverlay.puzzleDifficulty = user.latestDifficulties[0];
            _loadingOverlay.puzzleStreak = user.latestStreaks[0];
            [_loadingOverlay update];
            [_loadingOverlay addSubview:self.icon];
            _icon.center = _loadingOverlay.center;
            dispatch_async(puzzleQueue, ^{
                [self setupPuzzle];
            });
        });
    });
    [self addSubview:_imageView];

    _scale = CGPointMake(LAYER_WIDTH / self.bounds.size.width, LAYER_HEIGHT / self.bounds.size.height);

    _panRecognizer = [[ColorGestureRecognizer alloc] initWithTarget:self action:@selector(_didPan:)];
    _panRecognizer.scale = _scale;
    _panRecognizer.groupForPoint = ^group_t(point_t point) {
        return _board->groupForPixel[point.x][point.y];
    };
    [_imageView addGestureRecognizer:_panRecognizer];
    _imageView.multipleTouchEnabled = YES;

    self.enabled = NO;
    
    [self _ensureLoadingOverlay];
    _loadingOverlay.loading = YES;
    [self addSubview:_loadingOverlay];
    self.delegate = self;

    return self;
}

- (void)_updateColorPie
{
    _panRecognizer.angle = user.pieAngle;
    _panRecognizer.colors = _layer->colors;
}

- (void)_ensureLoadingOverlay
{
    if (_loadingOverlay) {
        return;
    }
    _loadingOverlay = [[LoadingOverlay alloc] initWithFrame:self.bounds showSwitch:YES];
    _loadingOverlay.delegate = self;
    [_imageView addSubview:_loadingOverlay];

    _nextRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadingOverlayTapped)];
    [_loadingOverlay addGestureRecognizer:_nextRecognizer];

    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.frame = CGRectMake(0,self.bounds.size.height - 40,40,40);
    [_dismissButton
        setImage:[UIImage imageNamed:@"PMin"]
        forState:UIControlStateNormal];
    [_dismissButton
        addTarget:self
        action:@selector(_dismiss)
        forControlEvents:UIControlEventTouchUpInside];
    [_loadingOverlay addSubview:_dismissButton];
}

- (void)_dismiss
{
    self.presented = NO;
    _puzzleDismisser.dismissing = YES;
    [_puzzleDismisser dismiss];
}

- (void)hideLoadingOverlay
{
    _loadingOverlay.alpha = 0;
    self.enabled = YES;
}

- (void)presentLoadingOverlay
{
    self.enabled = NO;
    [self _ensureLoadingOverlay];
    [UIView animateWithDuration:.2
        animations:^{
            _loadingOverlay.alpha = 1.0;
        }
        completion:nil];
}

- (void)setEnabled:(BOOL)enabled
{
    _enabled = enabled;
    if (enabled) {
        [self hideTapHint];
    }
    [self updateActive];
}

- (void)setPresented:(BOOL)presented
{
    _presented = presented;
    [self updateActive];
}

- (void)updateActive
{
    _nextRecognizer.enabled = _dismissButton.enabled = _presented && !_enabled;
    BOOL inactive = !(_presented && _enabled);
    if (_inactive == inactive) {
        return;
    }
    _inactive = inactive;

    _panRecognizer.enabled = !inactive;
    if (inactive) {
        if (_loadingOverlay) {
            [self insertSubview:self.whiteOverlay belowSubview:_loadingOverlay];
        } else {
            [self addSubview:self.whiteOverlay];
        }
        _whiteOverlay.alpha = 0.0;
        [UIView
            animateWithDuration:0.2
            animations:^{
                _whiteOverlay.alpha = 1.0;
            }];
        _puzzleTime += -[_puzzleStart timeIntervalSinceNow];
        _puzzleStart = nil;
    } else {
        [UIView
            animateWithDuration:0.1
            animations:^{
                _whiteOverlay.alpha = 0.0;
            }
            completion:^(BOOL finished) {
                [_whiteOverlay removeFromSuperview];
            }];
        _puzzleStart = [NSDate date];
    }
}

- (UIView *)whiteOverlay
{
    if (_whiteOverlay) {
        return _whiteOverlay;
    }
    _whiteOverlay = [[UIView alloc]
        initWithFrame:CGRectMake(-10000, -10000, 20000, 20000)];
    _whiteOverlay.backgroundColor = [UIColor color:WHITE alpha:0.5];
    return _whiteOverlay;
}

- (UIView *)icon
{
    if (_icon) {
        return _icon;
    }
    _icon = [[UILabel alloc]
        initWithFrame:CGRectZero];
    _icon.attributedText = [[NSAttributedString alloc]
        initWithString:@"⏱"
        attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:(iconSize / minScale) * stopWatchAdjustment],
                NSKernAttributeName : @2
        }
    ];
    [_icon sizeToFit];
    _icon.layer.anchorPoint = CGPointMake(0.5, 0.5 + ((stopWatchAdjustment - 1.0) / 4));
    return _icon;
}

- (void)setupPuzzle
{
    // this is on other thread
    board_t *board = Malloc(sizeof(board_t));
    puzzle_t puzzle = awaitNextBestFitPuzzle();
    _loadingOverlay.loading = NO;
    boardFrom(board, puzzle);
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self shouldPrefer:board to:_nextBoard]) {
            dispatch_async(puzzleQueue, ^{
                layer_t *layer = Malloc(sizeof(layer_t));
                layerFrom(board, layer);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self finishBoard:board layer:layer puzzle:puzzle];
                });
            });
        } else {
            insertPuzzle(puzzle, board->edgeCount, board->smallestTileSize);
            board_destroy(board);
        }
    });
}

- (void)finishedLoadingImage:(UIImage *)image board:(board_t *)board layer:(layer_t *)layer
{
    _imageView.image = image;
    [self hideLoadingOverlay];
    _moveCount = 0;
    _puzzleTime = 0;
    if (_board) {
        board_destroy(_board);
    }
    free(_layer);
    _board = board;
    _layer = layer;
    [self _updateColorPie];
    [self loadAnotherPuzzle];
}

- (void)loadAnotherPuzzle
{
    dispatch_async(puzzleQueue, ^{
        [self setupPuzzle];
    });
}

+ (UIImage *)imageForLayer:(layer_t *)layer
{
    // TODO find way to avoid CGSConvertBGRX8888toRGBA8888
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, layer->pixels, LAYER_WIDTH * LAYER_HEIGHT * 4, NULL);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef imageRef = CGImageCreate(LAYER_WIDTH, LAYER_HEIGHT, 8, 32, LAYER_WIDTH * 4, colorSpaceRef, bitmapInfo, provider, NULL, YES, renderingIntent);
    CGColorSpaceRelease(colorSpaceRef);
    CGDataProviderRelease(provider);
    UIImage *image = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return image;
}

- (BOOL)shouldPrefer:(board_t *)board to:(board_t *)other
{
    if (!other) {
        return YES;
    }
    difficulty_t otherDifficulty = difficultyForEdgeCount(board->edgeCount);
    difficulty_t difficulty = difficultyForEdgeCount(board->edgeCount);
    switch (user.preferredDifficulty) {
        case HATCH_DIFFICULTY:
        case CHICKEN_DIFFICULTY:
        case ALLIGATOR_DIFFICULTY:
            if (otherDifficulty == user.preferredDifficulty) {
                return NO;
            } else if (difficulty == user.preferredDifficulty) {
                return YES;
            }
            return board->edgeCount < other->edgeCount;
        case DRAGON_DIFFICULTY:
            return board->edgeCount > other->edgeCount;
    }
    return NO;
}

- (void)updateColorPie
{
    if (_nextLayer) {
        for (int i = 0; i < 4; i++) {
            _nextLayer->colors[i] = piColor(user.piColors[i].color);
        }
    }
}

- (void)finishBoard:(board_t *)board layer:(layer_t *)layer puzzle:(puzzle_t)puzzle
{
    if ([self shouldPrefer:board to:_nextBoard]) {
        UIImage *image = [PuzzleView imageForLayer:layer];
        _nextImage = image;
        board_t *oldBoard = _nextBoard;
        layer_t *oldLayer = _nextLayer;
        _nextBoard = board;
        _nextLayer = layer;
        [self updateColorPie];
        if (oldBoard) {
            insertPuzzle(_nextPuzzle, oldBoard->edgeCount, oldBoard->smallestTileSize);
            board_destroy(oldBoard);
            layer_destroy(oldLayer);
        }
        _nextPuzzle = puzzle;
    } else {
        insertPuzzle(puzzle, board->edgeCount, board->smallestTileSize);
        board_destroy(board);
        layer_destroy(layer);
    }
    difficulty_t readiedDifficulty = difficultyForEdgeCount(_nextBoard->edgeCount);
    if (_loadedDifficulty != readiedDifficulty) {
        _loadedDifficulty = readiedDifficulty;
        onAvailabilityChanged();
    } else {
        _loadedDifficulty = readiedDifficulty;
    }
}

- (void)memoryRestore
{
    _imageView.image = [PuzzleView imageForLayer:_layer];
    if (_nextBoard) {
        _nextLayer = Malloc(sizeof(*_nextLayer));
        layerFrom(_nextBoard, _nextLayer);
        _nextImage = [PuzzleView imageForLayer:_nextLayer];
    }
}

- (void)memoryRelease
{
    _imageView.image = nil;
    if (_nextLayer) {
        layer_destroy(_nextLayer);
        free(_nextLayer);
    }
    _nextLayer = 0;
    _nextImage = 0;
    if (!_whiteOverlay.superview) {
        _whiteOverlay = nil;
    }
}

- (void)_didPan:(ColorGestureRecognizer *)sender
{
    colorstate_t *state;
    while ((state = [sender next])) {
        [self _handleStateChange:state];
    }
}

- (void)_handleStateChange:(colorstate_t *)state
{
    color_t color = state->color;
    group_t group = state->group;
    color_t groupColor = getGroupColor(_layer, group);
    switch (state->state) {
        case UIGestureRecognizerStateBegan:
            state->startColor = groupColor;
            _layer->midPoints[group] = state->midPoint;
            _panColorStart[group] = groupColor;
        case UIGestureRecognizerStateChanged:
            if (groupColor != color || color == NULL_COLOR) {
                int invalid = colorGroup(_board, _layer, group, color, 1);
                _imageView.image = [PuzzleView imageForLayer:_layer];
                if (color != groupColor) {
                    sounds_play(invalid ? SWIPE : SPLASH);
                }
            }
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
            return;
        case UIGestureRecognizerStateEnded:
            if (state->wasTap && color == NULL_COLOR && state->startColor == NULL_COLOR) {
                color = forced(_board, _layer, group);
            }
            {
                int invalid = colorGroup(_board, _layer, group, color, 0);
                if (groupColor != color) {
                    sounds_play(invalid ? SWIPE : SPLASH);
                }
            }
            _imageView.image = [PuzzleView imageForLayer:_layer];
            if (color == NULL_COLOR) {
                if (state->wasTap && _panColorStart[group] == NULL_COLOR) {
                    [self _didDoubleTap:group];
                }
            } else if (_panColorStart[group] != color) {
                _moveCount++;
                if (_isQuickZoomed) {
                    [self _quickZoomOut];
                }
            }
            if (layerComplete(_layer)) {
                [self _puzzleWasSolved];
            }
    }
}

- (void)_puzzleWasSolved
{
    if (_moveCount == NUM_GROUPS) {
        sounds_play(GLASS_PING);
    }
    [self setZoomScale:1.0 animated:YES];
    [self setContentOffset:CGPointZero animated:YES];
    [self presentLoadingOverlay];
    _puzzleTime = _puzzleTime - [_puzzleStart timeIntervalSinceNow];
    _loadingOverlay.puzzleTime = _puzzleTime;
    _loadingOverlay.puzzleAttempt = _moveCount;
    [self _endOfPuzzleUserUpdate];
    _loadingOverlay.puzzleStreak = user.latestStreaks[0];
    _loadingOverlay.puzzleDifficulty = user.latestDifficulties[0];
    [_loadingOverlay update];
}

- (void)loadingOverlayTapped
{
    [self _showNext];
}

- (void)_showNext
{
    if (_nextImage) {
        [self finishedLoadingImage:_nextImage board:_nextBoard layer:_nextLayer];
        _nextImage = nil;
        _nextBoard = 0;
        _nextLayer = 0;
    } else {
        _loadingOverlay.loading = YES;
    }
}

- (void)_endOfPuzzleUserUpdate
{
    if (_loadingOverlay.puzzleTime < piToggle(user.piBestTime.value)) {
        user.piBestTime.value = piToggle(_loadingOverlay.puzzleTime);
    }
    difficulty_t difficulty = difficultyForEdgeCount(_board->edgeCount);
    uint8_t counts[4] = {
        0, 0, 0, 0
    };

    for (group_t group = 0; group < NUM_GROUPS; group++) {
        for (int i = 0; i < 4; i++) {
            if (_layer->groupColors[group] == _layer->colors[i]) {
                counts[i]++;
                break;
            }
        }
    }
    for (int i = 0; i < 4; i++) {
        user.colorCounts[i]++;
    }

    uint32_t i = 7;
    do {
        user.latestAttempts[i + 1] = user.latestAttempts[i];
        user.latestDifficulties[i + 1] = user.latestDifficulties[i];
        user.latestTimes[i + 1] = user.latestTimes[i];
        user.latestStreaks[i + 1] = user.latestStreaks[i];
    } while (i --> 0);
    user.latestAttempts[0] = _moveCount;
    if (_moveCount == NUM_GROUPS) {
        user.header.didPerfectDifficulty |= 1 << difficulty;
    }
    user.latestDifficulties[0] = difficulty;
    user.latestTimes[0] = _puzzleTime;
    if (_moveCount == NUM_GROUPS) {
        user_incrementPuzzlesPerfected(&user);
        user.latestStreaks[0] = user.latestStreaks[1] + 1;
        user_setLongestStreak(&user);
    } else {
        user.latestStreaks[0] = 0;
    }
    bool wasBestTime;
    if (wasBestTime = _loadingOverlay.puzzleTime < piToggle(user.piBestTimes[difficulty].value)) {
        user.piBestTimes[difficulty].value = piToggle(_loadingOverlay.puzzleTime);
        user.difficultyBestTimesAttempts[difficulty] = _moveCount;
        user.difficultyBestTimesStreaks[difficulty] = user.latestStreaks[0];
    }

    saveSolvedPuzzle(_imageView.image, difficulty, wasBestTime);
}

- (void)dealloc
{
    free(_board);
    free(_layer);
    free(_nextBoard);
    free(_nextLayer);
}

- (void)_fullscreen
{
    self.zoomScale = 1;
    self.contentOffset = CGPointZero;
    _imageView.transform = CGAffineTransformIdentity;
    _imageView.frame = self.bounds;
}

#pragma mark - Quick Zoom

- (void)_didDoubleTap:(group_t)group
{
    if (_isQuickZoomed) {
        [self _quickZoomOut];
    } else {
        [self _quickZoom:group];
    }
}

- (void)_quickZoom:(group_t)group
{
    extreme_t *extreme = &_layer->extremes[group];
    CGRect zoomRect = CGRectMake(
        (extreme->lowX - 6) / _scale.x,
        (extreme->lowY - 6) / _scale.y,
        (extreme->highX - extreme->lowX + 12) / _scale.x,
        (extreme->highY - extreme->lowY + 12) / _scale.y
    );
    zoomRect = CGRectIntersection(zoomRect, _imageView.bounds);
    CGPoint translation = CGPointMake(
        CGRectGetMidX(zoomRect),
        CGRectGetMidY(zoomRect)
    );
    zoomRect = CGRectOffset(zoomRect, -translation.x, -translation.y);
    // assumption(aspectRatio < 1)
    // FIXME this expands it over the edge
    // I need to move it to the actual center
    CGFloat aspectRatio = CGRectGetWidth([UIScreen mainScreen].bounds) / CGRectGetHeight([UIScreen mainScreen].bounds);
    CGFloat shapeRatio = CGRectGetWidth(zoomRect) / CGRectGetHeight(zoomRect);
    if (shapeRatio > aspectRatio) {
        zoomRect = CGRectApplyAffineTransform(
            zoomRect,
            CGAffineTransformMakeScale(
                1,
                shapeRatio / aspectRatio
            )
        );
    } else {
        zoomRect = CGRectApplyAffineTransform(
            zoomRect,
            CGAffineTransformMakeScale(
                aspectRatio / shapeRatio,
                1
            )
        );
    }
    zoomRect = CGRectOffset(
        zoomRect,
        MAX(MIN(translation.x, CGRectGetWidth(_imageView.bounds) - CGRectGetWidth(zoomRect) / 2), CGRectGetWidth(zoomRect) / 2),
        MAX(MIN(translation.y, CGRectGetHeight(_imageView.bounds) - CGRectGetHeight(zoomRect) / 2), CGRectGetHeight(zoomRect) / 2)
    );
    [UIView animateWithDuration:0.18
        animations:^{
            [self zoomToRect:zoomRect animated:NO];
        }
        completion:^(BOOL finished) {
            _isQuickZoomed = YES;
        }];
}

- (void)_quickZoomOut
{
    [UIView animateWithDuration:0.18
        animations:^{
            [self _fullscreen];
        }
        completion:^(BOOL finished) {
            _isQuickZoomed = NO;
        }];
}

- (void)_setupTapHint
{
    CGRect imageFrame = _imageView.frame;
    _tapHint = [HintView
        tapHintView:self
        frame:CGRectMake(
            imageFrame.size.width / 3,
            imageFrame.origin.y + imageFrame.size.height / 16,
            imageFrame.size.width / 3,
            imageFrame.size.height / 2
        )
        showLine:NO
    ];
    [_tapHint start];
}

- (void)hideTapHint
{
    [_tapHint stop];
    _tapHint = nil;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

@end
