#import "ColorPieControl.h"

#import "UserManager.h"

@implementation ColorPieControl
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    UIView *inset = [[UIView alloc]
        initWithFrame:CGRectInset(self.bounds, 10, 10)];
    CGRect insetBounds = inset.bounds;
    _colorPie = [[ColorPie alloc]
        initWithFrame:CGRectInset(insetBounds, -insetBounds.size.width, -insetBounds.size.height)];
    [self addSubview:inset];
    [inset addSubview:_colorPie];
    inset.clipsToBounds = YES;
    inset.layer.cornerRadius = frame.size.width / 8;

    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]];

    return self;
}

- (void)commitPieRotation:(CGFloat)rotation
{
    rotation += user.pieAngle;
    long offset = lround(floor(rotation / M_PI_2));
    union bitColor colors[4] = {
        user.piColors[0],
        user.piColors[1],
        user.piColors[2],
        user.piColors[3]
    };
    uint64_t colorCounts[4] = {
        user.colorCounts[0],
        user.colorCounts[1],
        user.colorCounts[2],
        user.colorCounts[3]
    };
    for (uint16_t i = 0; i < 4; i++) {
        uint64_t rotated = (i-offset) & 3;
        user.piColors[i] = colors[rotated];
        user.colorCounts[i] = colorCounts[rotated];
    }
    user.pieAngle = rotation - offset * M_PI_2;
    assert(user.pieAngle < M_PI_2);
    assert(user.pieAngle >= -0);
    [_colorPie update];
}

- (void)tap:(UITapGestureRecognizer *)sender
{
    [_colorPie.layer removeAllAnimations];
    if (!CGAffineTransformEqualToTransform(_colorPie.transform, CGAffineTransformMakeRotation(user.pieAngle))) {
        return;
    }
    UIView *tapView = sender.view;
    CGPoint tapPoint = [sender locationInView:tapView];
    CGSize size = tapView.bounds.size;
    if (tapPoint.x >= size.width / 2) {
        // advance to next 45 degree
        user.pieAngle = floor((user.pieAngle + 0.001) / M_PI_4) * M_PI_4;
        user.pieAngle += M_PI_4;
        // cycle by tau
        user.pieAngle -= floor(user.pieAngle / (2 * M_PI)) * 2 * M_PI;
    } else {
        // go to previous 45 degree
        user.pieAngle = ceil((user.pieAngle - 0.001) / M_PI_4) * M_PI_4;
        user.pieAngle -= M_PI_4;
        // cycle by tau
        user.pieAngle -= floor(user.pieAngle / (2 * M_PI)) * (2 * M_PI);
    }

    [UIView animateWithDuration:0.2
        delay:0
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            _colorPie.transform = CGAffineTransformMakeRotation(user.pieAngle);
        }
        completion:^(BOOL finished) {
            if (finished) {
                [self commitPieRotation:0];
            }
        }];
}
@end
