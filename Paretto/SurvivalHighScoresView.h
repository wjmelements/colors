@import UIKit;
@interface SurvivalHighScoresView : UIView
- (instancetype)initWithFrame:(CGRect)frame
    textAttributes:(NSDictionary *)textAttributes;
- (__strong UIView **)emojiViews;
- (__strong UIView **)scoreViews;
@end
