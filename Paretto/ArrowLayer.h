@import UIKit;

@interface ArrowLayer : CALayer
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIColor *strokeColor;
@end
