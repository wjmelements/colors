#import <UIKit/UIKit.h>

#import "SettingsViewController.h"

@interface PuzzleDismisser : NSObject <UIViewControllerAnimatedTransitioning>

- (instancetype)initWithPresenter:(SettingsViewController *)presenter;
@property (nonatomic, assign) BOOL dismissing;

- (void)dismiss;

@end
