#import "ColorShopCell.h"

#import "ColorPie.h"
#import "UIColor+C.h"

#import "style.h"
#import "trophies.h"

@implementation ColorShopCell {
    ColorPie *_colorPie;
    UILabel *_label;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    CGFloat size = frame.size.height;
    _colorPie = [[ColorPie alloc]
        initWithFrame:CGRectMake(
            0,
            0,
            size,
            size
        )];
    CGRect maskBounds = _colorPie.bounds;
    // adding this pixel column makes stuff look slightly better
    maskBounds.size.width += 1.0 / [UIScreen mainScreen].scale;;
    UIView *mask = [[UIView alloc]
        initWithFrame:maskBounds];
    mask.clipsToBounds = YES;
    [mask addSubview:_colorPie];

    UIView *contentView = self.contentView;
    [contentView addSubview:mask];
    self.layer.cornerRadius = CORNER_RADIUS;
    self.clipsToBounds = YES;

    _label = [[UILabel alloc]
        initWithFrame:CGRectMake(
            size,
            0,
            frame.size.width - size,
            size
        )
    ];
    _label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_label];
    
    return self;
}

- (void)setRow:(NSInteger)row
{
    NSString *trophy;
    _locked = NO;
    if (row) {
        trophy = trophies[row - 1];
        if (![_earnedTrophies containsString:trophy]) {
            trophy = @"🔒";
            _locked = YES;
        }
    } else {
        trophy = @"";
    }
    [_colorPie setRow:row locked:_locked];
    _label.attributedText = [[NSAttributedString alloc]
        initWithString:trophy
        attributes:_textAttributes
    ];
    if (self.selected) {
        self.contentView.backgroundColor = [UIColor color:WHITE];
    } else {
        self.contentView.backgroundColor = [UIColor color:NULL_COLOR];
    }
}

@end
