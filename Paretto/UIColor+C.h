#include "color_t.h"

#import <UIKit/UIKit.h>

@interface UIColor (C)

+ (UIColor *)color:(color_t)color;
+ (UIColor *)color:(color_t)color alpha:(CGFloat)alpha;

@end
