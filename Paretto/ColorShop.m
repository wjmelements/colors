#import "ColorShop.h"

#import "ColorShopCell.h" 
#import "UIColor+C.h"
#import "style.h"
#import "UserManager.h"

#include "colorsets.h"
#include "sounds.h"

#define ROW_HEIGHT 80.0
static NSString *kRowIdentifier = @"r";

@interface ColorShop () <
    UICollectionViewDataSource,
    UICollectionViewDelegate
    >

@end

@implementation ColorShop {
    NSInteger _selectedRow;
}

- (instancetype)init
{
    self = [super init];
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    UILabel *topLabel = [[UILabel alloc]
        initWithFrame:CGRectZero];
    topLabel.textAlignment = NSTextAlignmentRight;
    topLabel.attributedText = [[NSAttributedString alloc]
        initWithString:_earnedTrophies
        attributes:_textAttributes
    ];
    topLabel.numberOfLines = 1;
    [topLabel sizeToFit];
    UIScrollView *topScroll = [[UIScrollView alloc]
        initWithFrame:CGRectMake(
            50,
            20,
            bounds.size.width - 40,
            40
        )];
    topScroll.clipsToBounds = NO;
    topScroll.contentSize = CGSizeMake(topLabel.bounds.size.width, 40);
    topScroll.showsHorizontalScrollIndicator = NO;
    topScroll.showsVerticalScrollIndicator = NO;
    [topScroll addSubview:topLabel];
    [sView addSubview:topScroll];

    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton
        setTitle:@"👌🏽"
        forState:UIControlStateNormal
    ];
    [backButton
        addTarget:self
        action:@selector(_back)
        forControlEvents:UIControlEventTouchUpInside
    ];
    backButton.frame = CGRectMake(
        0,
        20, // goddamn status bar
        40,
        40
    );
    backButton.backgroundColor = [UIColor whiteColor];
    backButton.clipsToBounds = YES;
    backButton.layer.cornerRadius = CORNER_RADIUS;
    [sView addSubview:backButton];

    UICollectionViewFlowLayout *layout =
        [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(bounds.size.width, ROW_HEIGHT);
    layout.minimumInteritemSpacing = CORNER_RADIUS;

    UICollectionView *collectionView = [[UICollectionView alloc]
        initWithFrame:CGRectMake(
            0,
            70,
            bounds.size.width,
            bounds.size.height - 70
        )
        collectionViewLayout:layout
    ];
    collectionView.backgroundColor = sView.backgroundColor = [UIColor color:interpolate(NULL_COLOR, BLACK, 1)];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView
        registerClass:[ColorShopCell class]
        forCellWithReuseIdentifier:kRowIdentifier
    ];
    _selectedRow = user.selectedColorRow;
    [collectionView
        selectItemAtIndexPath:[NSIndexPath
            indexPathForRow:_selectedRow
            inSection:0]
        animated:NO
        scrollPosition:YES
    ];
    [sView addSubview:collectionView];
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)_back
{
    if (user.selectedColorRow != _selectedRow) {
        user.selectedColorRow = _selectedRow;
        for (int i = 0; i < 4; i++) {
            user.piColors[i].color = piColor(colorSets[_selectedRow][i]);
            user.colorCounts[i] = 0;
        }
        [_delegate didChangeColors];
    }

    [self.presentingViewController
        dismissViewControllerAnimated:NO
        completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ColorShopCell *cell = [collectionView
        dequeueReusableCellWithReuseIdentifier:kRowIdentifier   
        forIndexPath:indexPath
    ];
    cell.earnedTrophies = _earnedTrophies;
    cell.textAttributes = _textAttributes;
    [cell setRow:indexPath.row];

    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return NUM_COLORSETS;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)path
{
    _selectedRow = path.row;
    [collectionView cellForItemAtIndexPath:path].contentView.backgroundColor = [UIColor whiteColor];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)path
{
    [collectionView cellForItemAtIndexPath:path].contentView.backgroundColor = [UIColor color:NULL_COLOR];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)path
{
    ColorShopCell *cell = [collectionView cellForItemAtIndexPath:path];
    BOOL locked = cell.locked;
    if (locked) {
        sounds_play(SWIPE);
    }
    return !locked;
}

@end
