#import "IntegralCounter.h"
#import "ColorGestureRecognizer.h"
#import "PuzzleViewController.h"
#import "SurvivalViewController.h"
#import "UIColor+C.h"
#import "DefeatOverlay.h"
#import "RationalCounter.h"
#import "UserManager.h"

#include "capitalC.h"
#include "survival.h"

#include "sounds.h"
#include "style.h"

#include <mach/clock.h>
#include <mach/mach.h>

static const char *kSurvivalQueueLabel = "s";
static dispatch_queue_t survivalQueue;

#define MIN_INTERVAL 5000000

static inline NSTimeInterval updateAndGet(mach_timespec_t *lastTime, clock_serv_t clock_server) {
    mach_timespec_t ts;
    clock_get_time(clock_server, &ts);
    int64_t nsecs = (ts.tv_nsec - lastTime->tv_nsec);
    NSTimeInterval ret = ts.tv_sec - lastTime->tv_sec + nsecs / 1000000000.0f;
    *lastTime = ts;
    return ret;
}

@implementation SurvivalViewController {
    UIImageView *_survivalViewTop;
    UIImageView *_survivalViewBottom;
    survival_t *_survival;
    ColorGestureRecognizer *_panRecognizer;
    UITapGestureRecognizer *_nextRecognizer;

    // timer
    clock_serv_t _cclock;
    mach_timespec_t _timeOfLastShift;
    mach_timespec_t _timeOfLastResume;
    
    UILabel *_icon;
    // header
    UIView *_header;
    UIButton *_dismissButton;
    UIButton *_lapButton;
    IntegralCounter *_score;
    uint32_t _lapCount;
    uint32_t _skipFrame;
    uint64_t _survivalScore;
    uint64_t _survivalShifts;
    NSTimeInterval _survivalDuration;
    // reset
    DefeatOverlay *_defeatOverlay;
    RationalCounter *_startCounter;
    BOOL _ready;
    BOOL _defeated;

    BOOL _paused;
}

+ (instancetype)shared
{
    static SurvivalViewController *singleton;
    if (!singleton) {
        singleton = [[SurvivalViewController alloc] init];
        survivalQueue = dispatch_queue_create(kSurvivalQueueLabel, DISPATCH_QUEUE_SERIAL);
    }
    return singleton;
}

- (instancetype)init
{
    self = [super init];
    
    host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &_cclock);
    clock_get_time(_cclock, &_timeOfLastShift);

    return self;
}

- (void)setEnabled:(BOOL)enabled
{
    if (_enabled == enabled) {
        return;
    }
    _enabled = enabled;
    [[PuzzleViewController shared] hideTapHint];
    [self _setEnabled:enabled];
}

- (void)_setEnabled:(BOOL)enabled
{
    _panRecognizer.enabled = enabled;
    _nextRecognizer.enabled = enabled;
    _dismissButton.enabled = enabled;
    if (_enabled) {
        _startCounter.paused = NO;
        if (_paused) {
            _paused = NO;
            clock_get_time(_cclock, &_timeOfLastResume);
            if (_ready) {
                [self _start];
            } else {
                [self _beginAdvancing];
            }
        }
    } else {
        _startCounter.paused = YES;
    }
}

- (void)dealloc
{
    mach_port_deallocate(mach_task_self(), _cclock);
}

- (void)viewDidLoad
{
    UIView *gameContainer = [[UIView alloc] initWithFrame:self.view.bounds];
    gameContainer.transform = CGAffineTransformMakeRotation(M_PI);
    [self.view addSubview:gameContainer];

    _survivalViewTop = [[UIImageView alloc] initWithFrame:gameContainer.bounds];
    _survivalViewTop.contentMode = UIViewContentModeScaleToFill;
    _survivalViewTop.backgroundColor = [UIColor color:NULL_COLOR];
    [gameContainer addSubview:_survivalViewTop];

    _survivalViewBottom = [[UIImageView alloc] initWithFrame:gameContainer.bounds];
    _survivalViewBottom.contentMode = UIViewContentModeScaleToFill;
    _survivalViewBottom.backgroundColor = [UIColor color:NULL_COLOR];
    [gameContainer addSubview:_survivalViewBottom];
    
    _survival = Malloc(sizeof(survival_t));
    _ready = YES;
    dispatch_async(survivalQueue, ^{
        [self _init];
    });

    _nextRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_didTap:)];

    _panRecognizer = [[ColorGestureRecognizer alloc] initWithTarget:self action:@selector(_didPan:)];
    _panRecognizer.colors = _survival->colors;
    [self updateColorPie];
    _panRecognizer.groupForPoint = ^group_t(point_t point) {
        survival_t *survival = self->_survival;
        point.y += _survival->heightOffset;
        if (point.y > SURVIVAL_HEIGHT) {
            point.y -= SURVIVAL_HEIGHT;
        }
        return survival->groupForPixel[point.y][point.x];
    };
    _panRecognizer.scale = CGPointMake(SURVIVAL_WIDTH / self.view.bounds.size.width, LAYER_HEIGHT / self.view.bounds.size.height);
    [gameContainer addGestureRecognizer:_panRecognizer];

    _header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 40)];
    _header.backgroundColor = [UIColor color:WHITE alpha:0.5];

    _score = [[IntegralCounter alloc] initWithFrame:CGRectInset(_header.bounds, 45, 0)];
    _score.numberOfLines = 1;
    _score.textAlignment = NSTextAlignmentRight;
    _score.attributedFormatBlock = ^NSAttributedString *(uint64_t value) {
        return [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%llu", value]
            attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:28],
                NSKernAttributeName : @2
            }];;
    };
    _survivalScore = 0;
    _survivalShifts = 0;
    [_header addSubview:_score];
    [self.view addSubview:_header];

    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.frame = CGRectMake(0,0,40,40);
    _dismissButton.alpha = 0.8;
    [_dismissButton
        setImage:[UIImage imageNamed:@"PMin"]
        forState:UIControlStateNormal];
    [_dismissButton
        addTarget:self
        action:@selector(_dismiss)
        forControlEvents:UIControlEventTouchUpInside];
    [_header addSubview:_dismissButton];
    _lapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _lapButton.enabled = NO;
    _lapButton.frame = CGRectMake(_header.bounds.size.width - 40, 0, 40, 40);
    _lapButton.alpha = 0.8;
    _lapButton.backgroundColor = [UIColor color:TURQUOISE];
    _lapButton.layer.masksToBounds = YES;
    _lapButton.layer.cornerRadius = 20;
    [_lapButton
        setTitle:@"🔔"
        forState:UIControlStateNormal];
    [_lapButton
        setTitle:@"🔥"
        forState:UIControlStateDisabled];
    [_lapButton
        addTarget:self
        action:@selector(_lap)
        forControlEvents:UIControlEventTouchUpInside];
    [_header addSubview:_lapButton];
    [self.view addSubview:self.whiteOverlay];
    [self _setEnabled:NO];
}

- (void)updateColorPie
{
    _panRecognizer.angle = user.pieAngle;
    dispatch_async(survivalQueue, ^{
        if (_survival) {
            survival_rotateColors(_survival);
        }
    });
}

- (UIView *)whiteOverlay
{
    if (_whiteOverlay) {
        return _whiteOverlay;
    }
    _whiteOverlay = [[UIView alloc]
        initWithFrame:self.view.bounds
    ];
    _whiteOverlay.backgroundColor = [UIColor color:WHITE alpha:0.5];
    self.icon.center = _whiteOverlay.center;
    [_whiteOverlay addSubview:_icon];
    return _whiteOverlay;
}

- (UIView *)icon
{
    if (_icon) {
        return _icon;
    }
    _icon = [[UILabel alloc] initWithFrame:CGRectZero];
    _icon.attributedText = [[NSAttributedString alloc]
        initWithString:@"🔔"
        attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:iconSize / minScale],
                NSKernAttributeName : @2
        }
    ];
    [_icon sizeToFit];
    return _icon;
}

- (void)_dismiss
{
    self.enabled = NO;
    [_dismisser dismiss];
}

- (void)_lap
{
    sounds_play(RING);
    _lapCount += LAYER_HEIGHT * 3 / 4;
    _lapButton.enabled = NO;
    _dismissButton.enabled = NO;
    _score.textColor = [UIColor color:TURQUOISE];
    _score.shadowColor = [UIColor color:WHEAT];
}

- (void)_unlap
{
    _lapCount = 0;
    sounds_stop(RING);
    _lapButton.enabled = YES;
    _dismissButton.enabled = YES;
    _score.textColor = [UIColor blackColor];
    _score.shadowColor = nil;
}

- (void)_count
{
    [_score countFromCurrentValueTo:_survivalScore withDuration:0];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)_completeImage:(image_ref_t)image toView:(UIImageView *)view offset:(CGFloat)offset
{
    if (!image.height) {
        view.image = nil;
        view.frame = CGRectZero;
        return;
    }
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, image.pixels, SURVIVAL_WIDTH * image.height * 4, NULL);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef imageRef = CGImageCreate(SURVIVAL_WIDTH, image.height, 8, 32, SURVIVAL_WIDTH * 4, colorSpaceRef, bitmapInfo, provider, NULL, YES, renderingIntent);
    CGColorSpaceRelease(colorSpaceRef);
    CGDataProviderRelease(provider);
    UIImage *uiImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    view.image = uiImage;
    CGFloat imageHeight = self.view.bounds.size.height * image.height / LAYER_HEIGHT;
    view.frame = CGRectMake(0, self.view.bounds.size.height - offset - imageHeight, self.view.bounds.size.width, imageHeight);
}

- (void)_draw
{
    image_ref_t imageTop, imageBottom;
    survival_imageTop(_survival, &imageTop);
    survival_imageBottom(_survival, &imageBottom);
    [self _completeImage:imageTop toView:_survivalViewTop offset:0];
    [self _completeImage:imageBottom toView:_survivalViewBottom offset:_survivalViewTop.frame.size.height];
}

- (UIView *)defeatOverlay
{
    return _defeatOverlay;
}

- (void)_showDefeat
{
    if (_lapCount) {
        [self _unlap];
    }
    _lapButton.enabled = NO;
    const uint64_t finalScore = _survivalScore;
    _defeatOverlay = [[DefeatOverlay alloc]
        initWithFrame:self.view.bounds
        score:finalScore
        dismissButton:_dismissButton];
    _defeatOverlay.alpha = 0.1;
    dispatch_async(survivalQueue, ^{
        survival_destroy(_survival);
        [self _init];
    });
    [self.view addSubview:_defeatOverlay];
    [UIView animateWithDuration:0.1
        animations:^{
            _header.alpha = 0;
        }
        completion:^(BOOL finished){
            _defeatOverlay.alpha = 1;
            [self _updateHighestScore:finalScore];
            [_defeatOverlay addGestureRecognizer:_nextRecognizer];
        }];
}

- (void)_didTap:(UITapGestureRecognizer *)sender
{
    if (sender.state != UIGestureRecognizerStateRecognized) {
        return;
    }
    _survivalScore = 0;
    _survivalShifts = 0;
    [_score countFromCurrentValueTo:_survivalScore withDuration:0];
    [sender.view removeGestureRecognizer:sender];
    [_defeatOverlay removeFromSuperview];
    _defeatOverlay = nil;
    _header.alpha = 1.0;
    [_header addSubview:_dismissButton];
    _dismissButton.frame = CGRectMake(0,0,40,40);
    [self _maybeStart];
}

- (void)_maybeStart
{
    // need maybeStart from tap and from game init
    if (_ready) {
        [self _draw];
        [self _start];
    } else {
        _ready = YES;
    }
}

- (void)_start
{
    survival_start(_survival);
    if (!_enabled) {
        _paused = YES;
        return;
    }
    _defeated = NO;
    [self _countdown];
    _ready = NO;
}

- (void)_updateHighestScore:(uint64_t)finalScore
{
    // FIXME 32 bit score
    uint32_t finalScore32 = finalScore;
    if (user_highestScore(&user) < finalScore32) {
        user_setHighestScore(&user, finalScore32);
    }
    int leastIndex = 0;
    leastIndex = ~leastIndex;
    uint64_t least = finalScore;
    for (int i = 0; i < 4; i++) {
        if (user.survivalHighScores[i] < least) {
            leastIndex = i;
            least = user.survivalHighScores[i];
        }
        if (user.nextSurvivalHighScores[i] < least) {
            leastIndex = i + 4;
            least = user.nextSurvivalHighScores[i];
        }
    }
    if (~leastIndex) {
        user.survivalTimes[leastIndex] = _survivalDuration;
        if (leastIndex < 4) {
            user.survivalHighScores[leastIndex] = finalScore;
        } else {
            leastIndex -= 4;
            user.nextSurvivalHighScores[leastIndex] = finalScore;
        }
    }
}

- (void)_beginAdvancing
{
    clock_get_time(_cclock, &_timeOfLastShift);
    [self _advance];
}
- (void)_advance
{
    if (!_enabled) {
        _paused = YES;
        _survivalDuration += updateAndGet(&_timeOfLastResume, _cclock);
        return;
    }
    void (^adv)(void) = ^void(void){
        do {
            _defeated = survival_shift(_survival);
            _survivalShifts++;
            _survivalScore++;
            if (_lapCount) {
                _survivalScore++;
                --_lapCount;
            }
        } while (!_defeated && _skipFrame-- != 0);
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!_lapCount) {
                [self _unlap];
            }
            [self _draw];
            [self _count];
            if (_defeated) {
                _survivalDuration += updateAndGet(&_timeOfLastResume, _cclock);
                [self _showDefeat];
            } else {
                [self _advance];
            }
        });
    };
    mach_timespec_t tp;
    clock_get_time(_cclock, &tp);
    const int64_t nanoseconds = (tp.tv_sec - _timeOfLastShift.tv_sec) * 1E9 + tp.tv_nsec - _timeOfLastShift.tv_nsec;
    int64_t interval;
    if (_lapCount) {
        interval = MIN_INTERVAL;
    } else {
        const CGFloat score = 0.5 * (_survivalShifts / LAYER_HEIGHT) + 4;
        interval = MIN_INTERVAL * ((score + 7) * (score + 6)) / (score * score);
    }
    if (nanoseconds < interval ) {
        _skipFrame = 0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, interval - nanoseconds), survivalQueue, adv);
        tp.tv_nsec += interval - nanoseconds;
        if (tp.tv_nsec >= 1E9) {
            tp.tv_nsec -= 1E9;
            tp.tv_sec += 1;
        }
    } else {
        _skipFrame = (nanoseconds / interval) + 1;
        dispatch_async(survivalQueue, adv);
    }
    _timeOfLastShift = tp;
}

- (void)_countdown
{
    _startCounter = [[RationalCounter alloc]
        initWithFrame:self.view.bounds];
    _startCounter.textAlignment = NSTextAlignmentRight;
    _startCounter.attributedFormatBlock = ^NSAttributedString *(CGFloat value) {
        return [[NSAttributedString alloc]
            initWithString:[NSString stringWithFormat:@"%.0f", ceil(value)]
            attributes:@{
                NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Medium" size:48],
                NSKernAttributeName : @2,
     NSForegroundColorAttributeName : [UIColor color:_panRecognizer.colors[lround(value) % 4]]
            }];
    };

    [self.view addSubview:_startCounter];
    [_startCounter countFrom:5 to:0 withDuration:5];
    _startCounter.completionBlock = ^{
        _lapButton.enabled = YES;
        [_startCounter removeFromSuperview];
        _survivalDuration = 0;
        clock_get_time(_cclock, &_timeOfLastResume);
        [self _beginAdvancing];
        [UIView animateWithDuration:0.1
            animations:^{
                _score.alpha = 1.0;
            }];
    };
    [_startCounter sizeToFit];
    CGPoint center = self.view.center;
    center.y *= 2.0 / 3.0;
    _startCounter.center = center;
}

- (void)_init
{
    survival_init(_survival);
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self _maybeStart];
    });
}

- (void)_didPan:(ColorGestureRecognizer *)sender
{
    colorstate_t *state;
    while ((state = [sender next])) {
        [self _handleStateChange:state];
    }
}

- (void)_handleStateChange:(colorstate_t *)state
{
    if (state->state == UIGestureRecognizerStateCancelled) {
        return;
    }
    dispatch_async(survivalQueue, ^{
        if (_defeated) {
            return;
        }
        color_t color = state->color;
        group_t group = state->group;
        if (state->state == UIGestureRecognizerStateBegan) {
            group_t groupIndex = group - _survival->groupOffset;
            state->startColor = _survival->groupColors.colors[groupIndex];
        }
        int16_t d_group = group - _survival->groupOffset;
        if (d_group < 0) {
            return;
        }
        if (state->wasTap && color == NULL_COLOR && state->startColor == NULL_COLOR) {
            color = survival_forced(_survival, group);
        }
        uint64_t illegal;
        if (survival_color(_survival, group, color, &illegal)) {
            sounds_play(illegal ? SWIPE : SPLASH);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self _draw];
            });
        }
    });
}

@end
