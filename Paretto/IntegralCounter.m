#import "IntegralCounter.h"

#ifndef kLabelCounterRate
#define kLabelCounterRate 3.0
#endif

static CGFloat update(CGFloat t) {
    return 1.0-powf((1.0-t), kLabelCounterRate);
}

@interface IntegralCounter ()

@property uint64_t startingValue;
@property uint64_t destinationValue;
@property NSTimeInterval progress;
@property NSTimeInterval lastUpdate;
@property NSTimeInterval totalTime;
@property CGFloat easingRate;

@property (nonatomic, weak) NSTimer *timer;

@end

@implementation IntegralCounter

-(void)countFrom:(uint64_t)startValue to:(uint64_t)endValue withDuration:(NSTimeInterval)duration
{
    _startingValue = startValue;
    _destinationValue = endValue;
    
    // remove any (possible) old timers
    [_timer invalidate];
    _timer = nil;
    
    if (duration == 0.0) {
        // No animation
        [self _setTextValue:endValue];
        [self runCompletionBlock];
        return;
    }

    _easingRate = 3.0f;
    _progress = 0;
    _totalTime = duration;
    _lastUpdate = [NSDate timeIntervalSinceReferenceDate];

    NSTimer *timer = [NSTimer timerWithTimeInterval:(1.0f/30.0f) target:self selector:@selector(_updateValue) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:UITrackingRunLoopMode];
    _timer = timer;
    [self _updateValue];
}

- (void)countFromCurrentValueTo:(uint64_t)endValue withDuration:(NSTimeInterval)duration {
    [self countFrom:[self currentValue] to:endValue withDuration:duration];
}

- (void)countFromZeroTo:(uint64_t)endValue withDuration:(NSTimeInterval)duration {
    [self countFrom:0.0f to:endValue withDuration:duration];
}

- (void)setPaused:(BOOL)paused
{
    if (paused == _paused) {
        return;
    }
    _paused = paused;
    if (!paused) {
        _lastUpdate = [NSDate timeIntervalSinceReferenceDate];
        [self _updateValue];
    }
}

- (void)_updateValue {
    
    if (_paused) {
        return;
    }
    // update progress
    NSTimeInterval now = [NSDate timeIntervalSinceReferenceDate];
    _progress += now - _lastUpdate;
    _lastUpdate = now;
    
    if (_progress >= _totalTime) {
        [_timer invalidate];
        _timer = nil;
        _progress = _totalTime;
    }

    self.attributedText = _attributedFormatBlock([self currentValue]);
    
    if (_progress == _totalTime) {
        [self runCompletionBlock];
    }
}

- (void)runCompletionBlock {
    
    if (_completionBlock) {
        _completionBlock();
        _completionBlock = nil;
    }
}

- (uint64_t)currentValue {
    
    if (_progress >= _totalTime) {
        return _destinationValue;
    }
    
    CGFloat percent = _progress / _totalTime;
    CGFloat updateVal = update(percent);
    return _startingValue + ceil(updateVal * (_destinationValue - _startingValue));
}

- (void)_setTextValue:(uint64_t)value
{
    self.attributedText = _attributedFormatBlock(value);
}
@end
