#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NSAttributedString* (^RationalCounterAttributedFormatBlock)(CGFloat value);

@interface RationalCounter : UILabel

@property (nonatomic, assign) NSTimeInterval animationDuration;

@property (nonatomic, copy) RationalCounterAttributedFormatBlock attributedFormatBlock;
@property (nonatomic, assign) BOOL paused;
@property (nonatomic, copy) void (^completionBlock)();

-(void)countFrom:(CGFloat)startValue to:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromCurrentValueTo:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromZeroTo:(CGFloat)endValue withDuration:(NSTimeInterval)duration;

- (CGFloat)currentValue;

@end

