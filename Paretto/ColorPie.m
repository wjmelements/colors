#import "ColorPie.h"
#import "UIColor+C.h"
#import "UserManager.h"
#import "PuzzleView.h"
#import "PuzzleViewController.h"
#import "SurvivalViewController.h"

#include "colorsets.h"

@implementation ColorPie {
    CALayer
        *_upLeft,
        *_upRight,
        *_botLeft,
        *_botRight,
        *_center;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    CGSize size = self.bounds.size;
    CGSize quadSize = size;
    quadSize.width /= 2;
    quadSize.height /= 2;
    CALayer *sLayer = self.layer;
    //sLayer.cornerRadius = quadSize.width / 4;
    self.clipsToBounds = NO;
    _upLeft = [[CALayer alloc] init];
    _upLeft.frame = CGRectMake(
            -quadSize.width,
            -quadSize.height,
            size.width,
            size.height
        );
    _upRight = [[CALayer alloc] init];
    _upRight.frame = CGRectMake(
            quadSize.width,
            -quadSize.height,
            size.width,
            size.height
        );
    _botLeft = [[CALayer alloc] init];
    _botLeft.frame = CGRectMake(
            -quadSize.width,
            quadSize.height,
            size.width,
            size.height
        );
    _botRight = [[CALayer alloc] init];
    _botRight.frame = CGRectMake(
            quadSize.width,
            quadSize.height,
            size.width,
            size.height
        );
    _center = [[CALayer alloc] init];
    _center.frame = CGRectMake(
            quadSize.width * 8 / 9,
            quadSize.height * 8 / 9,
            quadSize.width * 2 / 9,
            quadSize.height * 2 / 9
        );
    _center.cornerRadius = _center.bounds.size.width / 2;

    _center.backgroundColor = [UIColor color:NULL_COLOR].CGColor;
    [sLayer addSublayer:_upLeft];
    [sLayer addSublayer:_upRight];
    [sLayer addSublayer:_botLeft];
    [sLayer addSublayer:_botRight];
    [sLayer addSublayer:_center];
    return self;
}

- (void)update
{
    colorset_t colors = {
        piColor(user.piColors[0].color),
        piColor(user.piColors[1].color),
        piColor(user.piColors[2].color),
        piColor(user.piColors[3].color)
    };
    [self _update:colors];
}

- (void)setRow:(NSInteger)row locked:(BOOL)locked
{
    colorset_t colorset;
    for (int i = 0; i < 4; i++) {
        colorset[i] = colorSets[row][i];
    }
    if (locked) {
        for (int i = 0; i < 4; i++) {
            colorset[i] = interpolate(colorset[i], GREY, 9);
        }
    }
    [self _update:colorset];
}

- (void)_update:(colorset_t)colors
{
    // update UI
    [CATransaction begin];
    [CATransaction setAnimationDuration:0]; // else it animates for 0.25s
    _botRight.backgroundColor = [UIColor color:colors[0]].CGColor;
    _botLeft.backgroundColor = [UIColor color:colors[1]].CGColor;
    _upLeft.backgroundColor = [UIColor color:colors[2]].CGColor;
    _upRight.backgroundColor = [UIColor color:colors[3]].CGColor;
    self.transform = CGAffineTransformMakeRotation(user.pieAngle);
    [CATransaction commit];

    // propagate updates
    [[SurvivalViewController shared] updateColorPie];
    [(PuzzleView *)[PuzzleViewController shared].view updateColorPie];
}
@end
