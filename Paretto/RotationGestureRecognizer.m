#import "RotationGestureRecognizer.h"

#import <UIKit/UIGestureRecognizerSubclass.h>

@implementation RotationGestureRecognizer {
    CGPoint _center;
    CGFloat _rotation;
}

- (instancetype)initWithTarget:(id)target action:(SEL)action
{
    self = [super initWithTarget:target action:action];
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    if ([event touchesForGestureRecognizer:self].count > 1) {
        self.state = UIGestureRecognizerStateFailed;
    } else {
        self.state = UIGestureRecognizerStateBegan;
        CGRect bounds = self.view.bounds;
        _center = CGPointMake(
            CGRectGetMidX(bounds),
            CGRectGetMidY(bounds)
        );
        _rotation = 0;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint current, previous;
    UITouch *touch;
    UIView *view = self.view;
    switch(self.state) {
    case UIGestureRecognizerStateBegan:
        self.state = UIGestureRecognizerStateChanged;
    case UIGestureRecognizerStateChanged:
        touch = [touches anyObject];
        current = [touch locationInView:view];
        previous = [touch previousLocationInView:view];
        _rotation += atan2(
            current.y - _center.y,
            current.x - _center.x
        ) - atan2(
            previous.y - _center.y,
            previous.x - _center.x
        );
    default: ;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.state == UIGestureRecognizerStateChanged) {
        self.state = UIGestureRecognizerStateEnded;
    } else {
        self.state = UIGestureRecognizerStateFailed;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.state = UIGestureRecognizerStateFailed;
}

- (CGFloat)rotation
{
    return _rotation;
}


@end
