#import "TutorialViewController.h"

#import "UIColor+C.h"
#import "HintView.h"
#import "UIGestureRecognizer+Helpers.h"

#include "colormap.h"
#include "sounds.h"

// layers
enum {
    TL,
    TR,
    BL,
    BR,
    CENTER,
    NUM_LAYERS,
    SOLUTION = TL,
};

@implementation TutorialViewController {
    // Lifecycle
    id<TutorialDelegate> _delegate;

    // View
    CALayer *_layers[NUM_LAYERS];
    color_t _directionColors[NUM_LAYERS];
    color_t _layerColors[NUM_LAYERS];

    // Gesture Handling
    CGPoint _startLocation;
    HintView *_downHint, *_overHint, *_tapHint;
}

static NSDictionary *textAttributes(CGFloat size)
{
    return @{
        NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:size],
    };
}

- (instancetype)initWithDelegate:(id<TutorialDelegate>)tutorialDelegate
{
    self = [super init];
    _delegate = tutorialDelegate;
    [self _setupPuzzle];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *sView = self.view;
        CGRect centerFrame = _layers[CENTER].frame;
        _downHint = [HintView downHintView:sView frame:centerFrame showLine:YES];
        _overHint = [HintView overHintView:sView];
        _tapHint = [HintView tapHintView:sView frame:centerFrame showLine:YES];
        [_downHint start];

        [self _showTutorialLabel];
    });
    return self;
}

- (void)_showTutorialLabel
{
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    UILabel *tutorialLabel = [[UILabel alloc]
        initWithFrame:CGRectMake(bounds.size.width - 40, 0, 40, 40)
    ];
    tutorialLabel.attributedText = [[NSAttributedString alloc]
        initWithString:@"👶🏼"
        attributes:textAttributes(36)
    ];
    [sView addSubview:tutorialLabel];
}

- (void)_setupPuzzle
{
    for (int i = 0; i < NUM_LAYERS; i++) {
        _layers[i] = [[CALayer alloc] init];
    }
    // colors
    _directionColors[TL] = SALMON;
    _directionColors[TR] = TEAL;
    _directionColors[BL] = PANSY_PINK;
    _directionColors[BR] = GOLDENROD;
    _directionColors[CENTER] = NULL_COLOR;
    _layerColors[TL] = GOLDENROD;
    _layerColors[TR] = TEAL;
    _layerColors[BL] = PANSY_PINK;
    _layerColors[BR] = GOLDENROD;
    _layerColors[CENTER] = NULL_COLOR;
    for (int i = 0; i < NUM_LAYERS; i++) {
        _layers[i].backgroundColor = [UIColor color:_layerColors[i]].CGColor;
    }
    // bounds
    CGSize size = self.view.bounds.size;
    _layers[TL].frame = CGRectMake(
        0, 0, size.width / 2, size.height / 2
    );
    _layers[TR].frame = CGRectMake(
        size.width / 2, 0, size.width / 2, size.height / 2
    );
    _layers[BL].frame = CGRectMake(
        0, size.height / 2, size.width / 2, size.height / 2
    );
    _layers[BR].frame = CGRectMake(
        size.width / 2, size.height / 2, size.width / 2, size.height / 2
    );
    _layers[CENTER].frame = CGRectMake(
        size.width / 3, size.height / 3, size.width / 3, size.height / 3
    );
    UIView *sView = self.view;
    CALayer *sLayer = sView.layer;
    for (int i = 0; i < NUM_LAYERS; i++) {
        [sLayer addSublayer:_layers[i]];
    }

    UILongPressGestureRecognizer *longPanSender = [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
        action:@selector(_longPress:)];
    longPanSender.minimumPressDuration = 0.03;
    longPanSender.allowableMovement = CGFLOAT_MAX;
    [sView addGestureRecognizer:longPanSender];
    sView.userInteractionEnabled = YES;
}


static int getDirection(const CGPoint one, const CGPoint two) {
    CGFloat dx = one.x - two.x;
    CGFloat dy = one.y - two.y;
    if (fabs(dx) + fabs(dy) < 25) {
        return CENTER;
    }
    if (dx < 0) {
        if (dy < 0) {
            return TL;
        } else {
            return BL;
        }
    } else {
        if (dy < 0) {
            return TR;
        } else {
            return BR;
        }
    }
}

- (void)_longPress:(UILongPressGestureRecognizer *)sender
{
    UIView *sView = self.view;
    CGPoint location = [sender locationInView:sView];
    if (sender.state == UIGestureRecognizerStateBegan) {
        _startLocation = location;
    }
    int direction = getDirection(location, _startLocation);
    switch (sender.state) {
    case UIGestureRecognizerStateBegan:
        if (CGRectContainsPoint(_layers[4].frame, location)) {
            [_downHint stop];
            [_overHint start];
            _overHint.baseCenter = location;
            [self _colorWithDirection:CENTER];
        } else {
            sounds_play(SWIPE); // FIXME boop
            [sender cancel];
        }
        break;
    case UIGestureRecognizerStateChanged:
        [self _colorWithDirection:direction];
        break;
    case UIGestureRecognizerStateEnded:
        [_overHint stop];
        if (direction == SOLUTION) {
            [sView removeGestureRecognizer:sender];
            [self _didSolveTutorial];
        } else {
            [_downHint start];
        }
        [self _colorWithDirection:direction];
    case UIGestureRecognizerStateCancelled:
        break;
    }
    _overHint.center = location;
    _overHint.direction = direction;
}

- (void)_colorWithDirection:(int)direction
{
    if (_layerColors[CENTER] != _directionColors[direction]) {
        sounds_play(direction == SOLUTION || direction == CENTER ? SPLASH : SWIPE);
    }
    _layerColors[CENTER] = _directionColors[direction];
    _layers[CENTER].backgroundColor = [UIColor color:_directionColors[direction]].CGColor;
    // update borders
    CGColorRef borderColor = [UIColor color:borderColorFor(_directionColors[direction])].CGColor;
    for (int i = 0; i < CENTER; i++) {
        if (_layerColors[i] == _directionColors[direction]) {
            _layers[i].borderWidth = 1.5;
            _layers[i].borderColor = borderColor;
        } else {
            _layers[i].borderWidth = 0;
        }
    }
    if (direction == SOLUTION || direction == CENTER) {
        _layers[CENTER].borderWidth = 0;
    } else {
        _layers[CENTER].borderWidth = 1.5;
        _layers[CENTER].borderColor = borderColor;
    }
}

- (void)_didSolveTutorial
{
    sounds_play(GLASS_PING);
    UIView *sView = self.view;
    CGRect bounds = sView.bounds;
    UIView *whiteOverlay = [[UIView alloc]
        initWithFrame:bounds];
    whiteOverlay.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    whiteOverlay.userInteractionEnabled = YES;
    [whiteOverlay addGestureRecognizer:[[UITapGestureRecognizer alloc]
        initWithTarget:_delegate
        action:@selector(didFinishTutorial)
    ]];
    UILabel *nice = [[UILabel alloc]
        initWithFrame:CGRectMake(bounds.size.width / 2 - 25, bounds.size.height / 4 - 25, 50, 50)];
    nice.attributedText = [[NSAttributedString alloc]
        initWithString:@"👌🏽"
        attributes:textAttributes(48)
    ];
    [whiteOverlay addSubview:nice];
    [sView addSubview:whiteOverlay];
    [_tapHint start];
}

#pragma mark - UIViewController

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
