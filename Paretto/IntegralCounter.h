#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NSAttributedString* (^IntegralCounterAttributedFormatBlock)(uint64_t value);

@interface IntegralCounter : UILabel

@property (nonatomic, assign) NSTimeInterval animationDuration;

@property (nonatomic, copy) IntegralCounterAttributedFormatBlock attributedFormatBlock;
@property (nonatomic, assign) BOOL paused;
@property (nonatomic, copy) void (^completionBlock)();

-(void)countFrom:(uint64_t)startValue to:(uint64_t)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromCurrentValueTo:(uint64_t)endValue withDuration:(NSTimeInterval)duration;

-(void)countFromZeroTo:(uint64_t)endValue withDuration:(NSTimeInterval)duration;

- (uint64_t)currentValue;

@end

