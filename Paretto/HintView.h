@import UIKit;

@interface HintView : UIView
+ (instancetype)tapHintView:(UIView *)superview frame:(CGRect)frame showLine:(BOOL)showLine;
+ (instancetype)downHintView:(UIView *)superView frame:(CGRect)frame showLine:(BOOL)showLine;
+ (instancetype)overHintView:(UIView *)superView;
- (void)start;
- (void)stop;
- (void)setBaseCenter:(CGPoint)baseCenter;
- (void)setDirection:(int)direction;
- (void)setValidDirections:(uint8_t)mask;
@end
