@class PuzzleDismisser;
@class SurvivalDismisser;

@protocol StatusBarController
- (void)overrideStatusBarHidden:(BOOL)hidden;
- (void)unsetStatusBarOverride;
@end

@interface SettingsViewController : UIViewController <UIScrollViewDelegate, UIViewControllerTransitioningDelegate, StatusBarController>

- (void)puzzleDismisserDidDismiss:(PuzzleDismisser *)puzzleDismisser;
- (void)survivalDismisserDidDismiss:(SurvivalDismisser *)puzzleDismisser;

+ (instancetype)shared;

@end
