#import <UIKit/UIKit.h>

#include "difficulty_t.h"

#import "PuzzleView.h"

extern NSString *timeEmojiForTime(double time);
extern NSString *scoreEmojiForScore(uint32_t attempt, uint64_t streak);
extern NSString *const difficultyTexts[4];

@interface LoadingOverlay : UIView

- (instancetype)initWithFrame:(CGRect)frame showSwitch:(BOOL)showSwitch;
// TODO refactor to use latestIndex
@property (nonatomic, assign) double puzzleTime;
@property (nonatomic, assign) uint32_t puzzleAttempt;
@property (nonatomic, assign) difficulty_t puzzleDifficulty;
@property (nonatomic, assign) uint64_t puzzleStreak;
- (void)update;
@property (nonatomic, assign) BOOL loading;
@property (nonatomic, weak) id<LoadingOverlayDelegate> delegate;

@end
