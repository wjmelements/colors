@import UIKit;

@interface GradientView : UIView
- (CAGradientLayer *)gradientLayer;
@end
