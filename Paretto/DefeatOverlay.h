#import <UIKit/UIKit.h>
#include "score_level_t.h"
NSString *scoreStringForScore(uint64_t score);
extern NSString *const scoreStrings[NUM_SCORE_LEVELS];
@interface DefeatOverlay : UIView
- (instancetype)
    initWithFrame:(CGRect)frame
    score:(uint64_t)score
    dismissButton:(UIButton *)dismissButton;
@end
