#import "PieChart.h"
#import "UIColor+C.h"
#import "UserManager.h"
static const CGFloat twoPi = M_PI * 2;
static inline CAShapeLayer *arcLayer(CGPoint center, CGFloat radius, UIColor *fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat startPct, CGFloat endPct) {
    CAShapeLayer *arc = [CAShapeLayer layer];
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:center];
    [path
        addArcWithCenter:center
        radius:radius
        startAngle:startPct * twoPi
        endAngle:endPct * twoPi
        clockwise:YES];
    [path closePath];
    arc.fillColor = fillColor.CGColor;
    arc.strokeColor = borderColor;
    arc.lineWidth = borderWidth;
    arc.path = path.CGPath;
    return arc;
}
@implementation PieChart {

}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    uint64_t colorSum = 0;
    for (int i = 0; i < 4; i++) {
        colorSum += user.colorCounts[i];
    }
    CGFloat startPct = 0;
    color_t colors[4] = {
        piColor(user.piColors[0].color),
        piColor(user.piColors[1].color),
        piColor(user.piColors[2].color),
        piColor(user.piColors[3].color)
    };
    uint64_t cumSum = 0;
    CGFloat radius = frame.size.width / 2;
    CGPoint center = CGPointMake(radius, radius);
    CGFloat borderWidth = 0;
    CGColorRef borderColor = [UIColor color:NULL_COLOR].CGColor;
    for (int i = 0; i < 4; i++) {
        cumSum += user.colorCounts[i];
        CGFloat endPct = ((CGFloat)cumSum) / colorSum;
        UIColor *fillColor = [UIColor color:colors[i]];
        [self.layer addSublayer:arcLayer(center, radius, fillColor, borderColor, borderWidth, startPct, endPct)];
        startPct = endPct;
    }
    return self;
}

@end
