#include "mocks/colorsets.h"

#include "survival.h"
#include "puzzle.h"
#include "board.h"
#include "layer.h"
#include "color.h"
#include "user.h"

user_t user;


#define PROFILE(title, iterations, block)\
    {\
        printf("\t %s\r", title);\
        clock_t before = clock();\
        for (uint16_t i = 0; i < iterations; i++)\
            block\
        clock_t after = clock();\
        double total = (after - before);\
        total /= iterations;\
        total /= CLOCKS_PER_SEC;\
        printf("%.6f\n", total);\
    }

static survival_t survival;
static void time_survival() {
    puts("survival");
    PROFILE("init/destroy", 10, {
        survival_init(&survival);
        survival_destroy(&survival);
    });
    survival_init(&survival);
    PROFILE("shift", SURVIVAL_HEIGHT, {
        survival_shift(&survival);
    });
    const uint32_t size = survival.groupColors.num_colors;
    PROFILE("color", size, {
        uint64_t illegal;
        survival_color(&survival, i + survival.groupOffset, SALMON, &illegal);
    });
    survival_destroy(&survival);
}
static void time_puzzle() {
    puts("puzzle");
    puzzle_t puzzle;
    PROFILE("generate/free", 10, {
        generatePuzzle(&puzzle);
        free(puzzle);
    });
}
static void time_board() {
    puts("board");
    puzzle_t puzzle;
    generatePuzzle(&puzzle);
    PROFILE("from/destroy", 10, {
        board_t *board = malloc(sizeof(board_t));
        boardFrom(board, puzzle);
        board_destroy(board);
    });
    free(puzzle);
}
static layer_t layer;
static void time_layer() {
    puts("layer");
    puzzle_t puzzle;
    generatePuzzle(&puzzle);
    board_t *board = malloc(sizeof(board_t));
    boardFrom(board, puzzle);
    PROFILE("from/destroy", 10, {
        layerFrom(board, &layer);
        layer_destroy(&layer);
    });
    layerFrom(board, &layer);
    PROFILE("colorGroup", NUM_GROUPS, {
        colorGroup(board, &layer, i, SALMON, 0);
    });
        
    layer_destroy(&layer);
    board_destroy(board);
    free(puzzle);
}
int main() {
    time_puzzle();
    puts("");
    time_board();
    puts("");
    time_layer();
    puts("");
    time_survival();
    return 0;
}
