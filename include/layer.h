#include "board_t.h"
void layerFrom(board_t *board, layer_t *layer);
int colorGroup(board_t *board, layer_t *layer, group_t group, color_t color, uint32_t active);
color_t forced(board_t *board, layer_t *layer, group_t group);
void layer_rotateColors();
