#include "board.h"
#include "barrier.h"
#include "difficulty_t.h"
#include "puzzle.h"

difficulty_t difficultyForEdgeCount(uint32_t edgeCount);

void startPuzzleManager();
void pausePuzzleManager(int await);
void resumePuzzleManager();

// TODO refactor puzzlemanager to return board_t, layer_t
// they would be generated if not ready yet
puzzle_t awaitNextPuzzle();
puzzle_t getNextPuzzle();
puzzle_t getNextBestFitPuzzle();
puzzle_t awaitNextBestFitPuzzle();

void insertPuzzle(puzzle_t puzzle, uint32_t edgeCount, uint32_t smallestTileSize);

typedef union availability {
    struct {
    unsigned hasHatch : 1;
    unsigned hasChicken : 1;
    unsigned hasAlligator : 1;
    unsigned hasDragon : 1;
    };
    unsigned bits;
} availability_t;

void getLatestAvailability(availability_t *availability);

extern void onAvailabilityChanged();
