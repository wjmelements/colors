#include "puzzle_t.h"
extern size_t fileSizeOf(puzzle_t puzle);
void generatePuzzle(puzzle_t *puzzle);
void suggestPuzzle(puzzle_t puzzle);
puzzle_t readPuzzle(int fd);
