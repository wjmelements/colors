typedef enum score_level {
    RAT_POISON,
    SHIT,
    APPLE,
    TOP_HAT,
    DIAMOND,
    DRAGON,
    KING,
    SCIENCE,
    NUM_SCORE_LEVELS
} score_level_t;

static inline score_level_t scoreLevelForScore(uint64_t score) {
    if (score < 16384) {
        if (score < 1867) {
            if (score < 420) {
                return RAT_POISON;
            } else {
                return SHIT;
            }
        } else {
            if (score < 9001) {
                return APPLE;
            } else {
                return TOP_HAT;
            }
        }
    } else {
        if (score < 7700000) {
            if (score < 53657) {
                return DIAMOND;
            } else {
                return DRAGON;
            }
        } else {
            if (score < 47500000) {
                return KING;
            } else {
                return SCIENCE;
            }
        }
    }
}
