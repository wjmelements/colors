#ifndef BITMAP_H
#define BITMAP_H
#include "capitalC.h"
typedef uint8_t* bitmap_t;

static inline size_t bitmap_size(uint64_t count) {
    return (size_t)((count >> 3) + (count & 7 ? 1 : 0));
}
static inline void bitmap_init(bitmap_t *bm, uint64_t count) {
    *bm = Calloc(bitmap_size(count), sizeof(uint8_t));
}
static inline void bitmap_destroy(bitmap_t bm) {
    free(bm);
}
static inline void bitmap_set(bitmap_t bm, uint64_t index) {
    bm[index >> 3] |= 1 << (index & 7);
}
static inline void bitmap_unset(bitmap_t bm, uint64_t index) {
    bm[index >> 3] &= ~(1 << (index & 7));
}
static inline int bitmap_isSet(bitmap_t bm, uint64_t index) {
    return bm[index >> 3] & (1 << (index & 7));
}
#endif
