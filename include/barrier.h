struct barrier;
typedef struct barrier barrier_t;
// return a new initialized barrier
barrier_t* barrier_new(void);
// initialize a barrier
void barrier_init(barrier_t*);
// destroy a barrier
void barrier_delete(barrier_t*);
// cleanup a barrier but don't free
void barrier_destroy(barrier_t*);
// register a new thread to arrive
void barrier_register(barrier_t*);
// deregisters a thread
void barrier_deregister(barrier_t*);
// arrive and wait for all all registered
void barrier_arrive(barrier_t*);
