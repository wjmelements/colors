#ifndef DIFFICULTY_T
#define DIFFICULTY_T
typedef enum difficulty {
    HATCH_DIFFICULTY = 0,
    CHICKEN_DIFFICULTY  = 1,
    ALLIGATOR_DIFFICULTY = 2,
    DRAGON_DIFFICULTY = 3,
    NUM_DIFFICULTIES,
} difficulty_t;
#endif
