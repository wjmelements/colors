#ifndef BOARD_T
#define BOARD_T

#include "bitmap.h"
#include "layer_t.h"

#define NUM_EDGES ((NUM_GROUPS * NUM_GROUPS - NUM_GROUPS) / 2)

typedef struct board {
    group_t groupForPixel[LAYER_WIDTH][LAYER_HEIGHT];
    bitmap_t edges;
    uint32_t edgeCount;
    uint32_t smallestTileSize;
} board_t;

static inline void board_destroy(board_t *board) {
    bitmap_destroy(board->edges);
    free(board);
}

#endif
