#define NUM_COLORSETS 2

#include "color_t.h"
color_t colorSets[NUM_COLORSETS][4] = {
    {
        TEAL,
        SALMON,
        GOLDENROD,
        PANSY_PINK
    },
    {
        YELLOW,
        ORCHID,
        CRIMSON,
        GREEN
    }
};
