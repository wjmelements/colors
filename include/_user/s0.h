struct {
    union header {
        struct {
            unsigned puzzlesPerfectedNextSixBits : 6; // +4 +8 +16 +32 +64 +128
            unsigned highestSurvivalScoreNextThreeBits : 3; // +4 +8 +16
            unsigned puzzlesPerfectedHighestSixBits : 6; // +256 +512 +1024 +2048 +4096 +8192
            unsigned hasCompletedTutorial : 1;
            unsigned highestSurvivalScoreLeastTwoBits : 2; // 0 - 3
            unsigned highestSurvivalScoreNextFourBits : 4; // +32 +64 +128
            unsigned didPerfectDifficulty : 4;
            unsigned reserved : 4;
            unsigned puzzlesPerfectedLeastTwoBits : 2; // +1 +2
            unsigned highestSurvivalScoreMiddleSevenBits : 7; // +256 +512 +1024 +2048 +4096 + 8192 + 16384
            unsigned longestStreakCheckBits : 9; // should match decoded piLongestStreak least-sig 9 bits
            // used bits: 48
        };
        uint64_t bits;
    } header;
    struct {
        uint64_t colorCounts[4];
        uint32_t latestAttempts[9];
        union bitFloat {
            double value;
            uint64_t bits;
        } piBestTime;
        double pieAngle; // [0, M_PI_2)
        union bitColor {
            color_t color;
            uint64_t bits;
        } piColors[4];
        difficulty_t latestDifficulties[9];
        difficulty_t preferredDifficulty;
        uint64_t survivalHighScores[4];
        double latestTimes[9];
        uint64_t nextSurvivalHighScores[4];
        double survivalTimes[8];
        uint64_t reserved;//formerly puzzleStreak
        uint64_t piLongestStreak;
        uint64_t latestStreaks[9];
        uint16_t reserved16;
        uint8_t reserved8;
        uint8_t selectedColorRow;
        union bitFloat  piBestTimes[4];
        uint32_t difficultyBestTimesAttempts[4];
        uint64_t difficultyBestTimesStreaks[4];
    };
};
