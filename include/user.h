#include <math.h>
#include <stdint.h>

#include "color_t.h"
#include "difficulty_t.h"

typedef struct {
    uint32_t version;
    union {
        #include "_user/s0.h"
    };
} user_t;

uint32_t user_puzzlesPerfected(user_t *user);
void user_incrementPuzzlesPerfected(user_t *user);

void user_upgrade(user_t *user);
void user_setHighestScore(user_t *user, uint32_t score);
uint32_t user_highestScore(const user_t *user);
static inline double piToggle(double piBestTime) {
    union bitFloat time, pi;
    time.value = piBestTime;
    pi.value = M_PI;
    time.bits ^= pi.bits;
    return time.value;
}
static inline color_t piColor(color_t color) {
    union bitFloat pi;
    pi.value = M_PI;
    union bitColor col;
    col.color = color;
    col.bits ^= pi.bits;
    return col.color;
}
static inline uint64_t piXor(uint64_t val) {
    union bitFloat pi;
    pi.value = M_PI;
    val ^= pi.bits;
    return val;
}
static inline void user_setLongestStreak(user_t *user) {
    if (user->latestStreaks[0] > piXor(user->piLongestStreak)) {
        user->header.longestStreakCheckBits = user->latestStreaks[0] & 0x1FF;
        user->piLongestStreak = piXor(user->latestStreaks[0]);
    }
}
