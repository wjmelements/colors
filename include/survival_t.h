// Survival works as a window (SURVIVAL_WIDTH, LAYER_HEIGHT) moving over a ring buffer (SURVIVAL_WIDTH, SURVIVAL_HEIGHT).

#include "bitmap.h"
#include "layer_t.h"

#ifndef SURVIVAL_WIDTH
#define SURVIVAL_WIDTH LAYER_WIDTH
#endif

#ifndef SURVIVAL_HEIGHT
#define SURVIVAL_HEIGHT (LAYER_HEIGHT * 2)
#endif
// (SURVIVAL_HEIGHT - LAYER_HEIGHT) must be slightly larger than LAYER_WIDTH.
// Otherwise, there is a risk of contamination of controlDistances,
// as I am assuming in addPoint that we cannot enter the current window.
// TODO calibrate SURVIVAL_HEIGHT

// color invalidity bitmap of following groups
typedef uint64_t invalid_t;

VECTOR(vp, vvp);
VECTOR(color, vc);
VECTOR(bitmap, edges);
VECTOR(invalid, invalids);

typedef struct survival {
    pixel_t pixels[SURVIVAL_HEIGHT][SURVIVAL_WIDTH];
    color_t colors[4];
    group_t groupForPixel[SURVIVAL_HEIGHT][SURVIVAL_WIDTH];
    uint16_t toEdgeDistances[SURVIVAL_HEIGHT][SURVIVAL_WIDTH];
    uint16_t controlDistances[SURVIVAL_HEIGHT][SURVIVAL_WIDTH];
    vc_t groupColors;
    vvp_t pointsForGroup;
    edges_t edgesForGroup; // edges with previous groups
    invalids_t invalids; // invalids for following groups
    uint16_t heightOffset;
    group_t groupOffset; // groupIndex = group - groupOffset
} survival_t;

// one happensbefore two
static inline void survival_unsetEdge(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    bitmap_unset(survival->edgesForGroup.bitmaps[two], bitIndex);
}
static inline void survival_setEdge(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    bitmap_set(survival->edgesForGroup.bitmaps[two], bitIndex);
}
static inline int survival_hasEdge(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    return bitmap_isSet(survival->edgesForGroup.bitmaps[two], bitIndex);
}
static inline void survival_setInvalid(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    survival->invalids.invalids[one] |= 1ull << bitIndex;
}
static inline void survival_unsetInvalid(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    survival->invalids.invalids[one] &= ~(1ull << bitIndex);
}
static inline uint64_t survival_isInvalid(survival_t *survival, group_t one, group_t two) {
    const group_t bitIndex = two - one - 1;
    return survival->invalids.invalids[one] & 1ull << bitIndex;
}
static inline int survival_onlyInvalid(survival_t *survival, group_t groupIndex, group_t onlyIndex) {
    // TODO with two methods, can eliminate branch
    if (groupIndex < onlyIndex) {
        if (survival->invalids.invalids[groupIndex] != 1ull << (onlyIndex - groupIndex - 1)) {
            return 0;
        }
        for (group_t g = 0; g < groupIndex; g++) {
            if (survival->invalids.invalids[g] & 1ull << (groupIndex - g - 1)) {
                return 0;
            }
        }
    } else {
        if (survival->invalids.invalids[groupIndex]) {
            return 0;
        }
        for (group_t g = 0; g < groupIndex; g++) {
            // FIXME this branch is hard to predict
            const uint64_t bit = 1ull << (groupIndex - g - 1);
            if (g == onlyIndex) {
                if (!(survival->invalids.invalids[g] & bit)) {
                    return 0;
                }
            } else {
                if (survival->invalids.invalids[g] & bit) {
                    return 0;
                }
            }
        }
    }
    return 1;
}
static inline int survival_valid(survival_t *survival, group_t groupIndex) {
    if (survival->invalids.invalids[groupIndex]) {
        return 0;
    }
    // TODO fix this shitty bit-for-speed tradeoff
    for (group_t g = 0; g < groupIndex; g++) {
        if (survival_isInvalid(survival, g, groupIndex)) {
            return 0;
        }
    }
    return 1;
}
