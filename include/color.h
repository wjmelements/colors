#include "layer_t.h"

static inline void setGroupColor(layer_t *layer, group_t group, color_t color) {
    layer->groupColors[group] = color;
}
static inline color_t getGroupColor(layer_t *layer, group_t group) {
    return layer->groupColors[group];
}
void resetGroupColors(layer_t *layer);
int layerComplete(layer_t *layer);
