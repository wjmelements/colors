#include <stdint.h>
#ifndef POINT_T
#define POINT_T
typedef struct point {
    uint16_t x;
    uint16_t y;
} point_t;
#endif
