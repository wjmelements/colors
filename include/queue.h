#define QUEUE(queue,type) \
typedef struct queue ## _node {\
    struct queue ## _node *next;\
    type data;\
} queue ## _node_t;\
typedef struct queue {\
    queue ## _node_t *head, **tail;\
} queue ## _t;\
static inline void queue ## _init(queue ## _t *queue) {\
    queue->tail = &(queue->head = Malloc(sizeof(queue ## _node_t)))->next;\
    *queue->tail = NULL;\
}\
static inline void queue ## _destroy(queue ## _t *queue) {\
    queue ## _node_t *next = queue->head;\
    do {\
        queue ## _node_t *prev = next;\
        next = next->next;\
        free(prev);\
    } while (next);\
}\
static inline queue ## _node_t *queue ## _ready(queue ## _t *queue) {\
    return queue->head->next;\
}\
static inline void queue ## _push(queue ## _t *queue, type item) {\
    queue ## _node_t *next = Malloc(sizeof(*next));\
    *queue->tail = next;\
    next->next = NULL;\
    next->data = item;\
    queue->tail = &next->next;\
}\
static inline type queue ## _pop(queue ## _t *queue) {\
    queue ## _node_t *curr = queue->head;\
    queue->head = curr->next;\
    free(curr);\
    return queue->head->data;\
}
