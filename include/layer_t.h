#ifndef LAYER_T
#define LAYER_T

#include "color_t.h"
#include "group_t.h"
#include "point_t.h"
#ifndef LAYER_WIDTH
#define LAYER_WIDTH 525
#endif

#ifndef LAYER_HEIGHT
#define LAYER_HEIGHT 932
#endif

static inline uint32_t indexFor(uint16_t x, uint16_t y) {
    return x * LAYER_HEIGHT + y;
}

static inline uint32_t adjIndexFor(group_t g1, group_t g2) {
    return (g2 * g2 - g2) / 2 + g1;
}

typedef union extreme {
    struct {
        uint16_t lowX, lowY, highX, highY;
    };
    uint64_t val;
} extreme_t;

#include "vector.h"
VECTOR(point, vp);

typedef struct layer {
    pixel_t pixels[LAYER_HEIGHT][LAYER_WIDTH];
    color_t colors[4];
    uint16_t toEdgeDistances[LAYER_WIDTH * LAYER_HEIGHT];
    vp_t pointsForGroup[NUM_GROUPS];
    uint32_t invalidEdges[NUM_GROUPS];
    point_t midPoints[NUM_GROUPS];
    color_t groupColors[NUM_GROUPS];
    extreme_t extremes[NUM_GROUPS];
} layer_t;

static inline void layer_init(layer_t *layer) {
    for (group_t group = 0; group < NUM_GROUPS; group++) {
        vp_init(&layer->pointsForGroup[group], LAYER_WIDTH * LAYER_HEIGHT / 3 / NUM_GROUPS);
    }
}

static inline void layer_destroy(layer_t *layer) {
    for (group_t group = 0; group < NUM_GROUPS; group++) {
        vp_destroy(&layer->pointsForGroup[group]);
    }
}

#endif
