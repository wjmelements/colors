#ifndef PUZZLE_T
#define PUZZLE_T

#include "point_t.h"
typedef struct puzzle {
    uint16_t version;
    union { //versions
        #include "_puzzle/s0.h"
    };
} saved_puzzle_t;
typedef saved_puzzle_t *puzzle_t;

#endif
