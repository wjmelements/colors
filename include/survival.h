#include "survival_t.h"

typedef struct image_ref {
    pixel_t *pixels;
    uint16_t height;
} image_ref_t;


void survival_init(survival_t *survival);
void survival_start(survival_t *survival);
// returns whether the player has lost
int survival_shift(survival_t *survival);
// returns whether a color changed
int survival_color(survival_t *survival, group_t group, color_t color, uint64_t *illegal);
color_t survival_forced(survival_t *survival, group_t group);
void survival_destroy(survival_t *survival);
void survival_imageTop(survival_t *survival_t, image_ref_t *image);
void survival_imageBottom(survival_t *survival, image_ref_t *image);
void survival_rotateColors(survival_t *survival);
