#include "color_t.h"

#ifndef NUM_COLORSETS
#define NUM_COLORSETS 14
#endif
typedef color_t colorset_t[4];
extern const colorset_t colorSets[NUM_COLORSETS];

static inline int colorsets_equal(colorset_t one, colorset_t two) {
    color_t color = 0;
    for (int i = 0; i < 4; i++) {
        color ^= one[i];
        color ^= two[i];
    }
    return color == 0;
}
