#ifndef COLOR_T
#define COLOR_T

#include "pixel_t.h"
typedef enum color {
    WHITE = 0xFFFFFFFF,
    GREY = 0xFF808080,
    YELLOW = 0xFF00FFFF,
    GREEN = 0xFF00FF00,
    BLUE = 0xFFFF0000,
    RED = 0xFF0000FF,
    SALMON = 0xFF7280FA,
    LIGHT_SALMON = 0xFF7AA0FF,
    DARK_SALMON = 0xFF7A96E9,
    SALMON_PINK = 0xFFA491FF,
    CRIMSON = 0xFF3C14DC,
    TEAL = 0xFF808000,
    TURQUOISE = 0xFFD0E040,
    WHEAT = 0xFFB3DEF5,
    BEIGE = 0xFFDCF5F5,
    CREAM = 0xFFD0FDFF,
    IVORY = 0xFFF0FFFF,
    CHAMPAGNE = 0xFFCEE7F7,
    GOLDENROD = 0xFF20A5DA,
    ORCHID = 0xFFD670DA,
    DARK_ORCHID = 0xFFCC3299,
    PANSY_PURPLE = 0xFF4A1878,
    PANSY_PINK = 0xFFE175F3,
    EMERALD = 0xFF78C850,
    PEACH = 0xFFB4E5FF,
    CORAL = 0xFF507FFF,
    FUCHSIA = 0xFFFF00FF,
    FANDANGO = 0xFF8933B5,
    OLIVE = 0xFF008080,
    AMBER = 0xFF00BFFF,
    AVOCADO = 0xFF038256,
    CERULEAN = 0xFFA77B00,
    IRIS = 0xFFCF4F5A,
    MINT = 0xFF89B43E,
    AQUAMARINE = 0xFFD4FF7F,
    PRINTING_GREEN = 0xFF50A500,
    GOLD = 0xFF00D7FF,
    SAFFRON = 0xFF30C4F4,
    CARNATION = 0xFFC9A6FF,
    ROSE = 0xFF7F00FF,
    LAVENDER = 0xFFFAE6E6,
    SPRING = 0xFF57DABD,
    HONEYDEW = 0xFFF0FFF0,
    BLUEGREY = 0xFFCC9966,
    PLATINUM = 0xFFE2E4E5,
    GOLD_METAL = 0xFF37AFD4,
    CELESTE = 0xFFFFFFB2,
    BLACK = 0xFF000000,
} color_t;
#define NULL_COLOR BEIGE
static inline void setColor(pixel_t *pixel, color_t color) {
    pixel->rgba = color;
}
static inline color_t adjust(color_t source, color_t target) {
    pixel_t result, second;
    result.rgba = source;
    second.rgba = target;
    if (result.r < second.r) {
        result.r += 1;
    } else if (result.r > second.r) {
        result.r -= 1;
    }
    if (result.g < second.g) {
        result.g += 1;
    } else if (result.g > second.g) {
        result.g -= 1;
    }
    if (result.b < second.b) {
        result.b += 1;
    } else if (result.b > second.b) {
        result.b -= 1;
    }
    return result.rgba;
}
static inline uint32_t interpolate(uint32_t origin, uint32_t destination, uint16_t distance) {
    pixel_t originPixel, destinationPixel, resultPixel;
    originPixel.rgba = origin;
    destinationPixel.rgba = destination;

    #define inter(r)\
    uint32_t r = destinationPixel.r;\
    r *= distance;\
    r += originPixel.r;\
    resultPixel.r = r / (distance + 1);
    inter(r);
    inter(g);
    inter(b);
    inter(a);
    #undef inter
    return resultPixel.rgba;
}
static inline int color_disjoint(color_t one, color_t two, color_t three, color_t four) {
    return one != two && one != three && one != four && two != three && two != four && three != four;
}

#endif
