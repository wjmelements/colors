// use panic or passert instead of assert
#ifdef DEBUG
#include <assert.h>
#define passert(cond, ...) assert(cond)
#define panic(...) assert(0)
#else
#define passert(...)
#define panic(...)
#endif
