#include "board.h"
#include "puzzle.h"

#include <stdatomic.h>
#include <unistd.h>

#define NUM_BUCKETS 191
#define NUM_PUZZLES 10000

static volatile int jobCounter = 0;
static volatile int buckets[NUM_BUCKETS];
static void *categorize(void *p) {
    board_t *board = Malloc(sizeof(*board));
    puzzle_t puzzle;
    while (atomic_fetch_add(&jobCounter, 1) < NUM_PUZZLES) {
        generatePuzzle(&puzzle);
        boardFrom(board, puzzle);
        free(puzzle);
        atomic_fetch_add(&buckets[board->edgeCount], 1);
    }
    free(board);
    return NULL;
}
int main() {
    const long numThreads = sysconf(_SC_NPROCESSORS_ONLN);
    pthread_t threads[numThreads];
    for (uint32_t i = 0; i < numThreads; i++) {
        pthread_create(&threads[i], NULL, categorize, NULL);
    }
    printf("Using %li threads...\n", numThreads);
    for (uint32_t i = 0; i < numThreads; i++) {
        pthread_join(threads[i], NULL);
    }
    for (uint16_t i = 0; i < NUM_BUCKETS; i++) {
        if (buckets[i]) {
            printf("%u %u\n", i, buckets[i]);
        }
    }
    return 0;
}
