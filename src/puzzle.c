#include <assert.h>
#include "capitalC.h"

#include "puzzle.h"

extern inline size_t fileSizeOf(puzzle_t puzzle)
{
    return sizeof(saved_puzzle_t);
}

#include "color.h"

#ifdef __linux__
#include <bsd/stdlib.h>
#else
#include <stdlib.h>
#endif

static volatile puzzle_t suggestedPuzzles[2] = {0,0};

void suggestPuzzle(puzzle_t puzzle) {
    for (uint8_t i = 0; i < 2; i++) {
        if (!suggestedPuzzles[i]) {
            suggestedPuzzles[i] = puzzle;
            return;
        }
    }
    free(puzzle);
}

void generatePuzzle(puzzle_t *puzzle)
{
    const uint16_t num_points = NUM_GROUPS;
    for (uint8_t i = 2; i --> 0;) {
        if (suggestedPuzzles[i]) {
            *puzzle = suggestedPuzzles[i];
            suggestedPuzzles[i] = 0;
            return;
        }
    }
    puzzle_t ret = *puzzle = Malloc(sizeof(saved_puzzle_t) + num_points * sizeof(point_t));
     
    ret->version = 0;
    ret->num_points = num_points;
    const uint8_t numSubgroups = 2;
    const uint8_t subgroupSize = num_points / numSubgroups;
    for (uint16_t subgroup = 0; subgroup < numSubgroups; subgroup++) {
        uint16_t subgroupOffset = subgroup * subgroupSize;
        for (uint16_t i = 0; i < subgroupSize; i++) {
            point_t *point = &ret->points[i + subgroupOffset];
            // TODO pick a point matching the criteria instead of checking a random one
            point->x = 3 + arc4random_uniform(LAYER_WIDTH - 6);
            point->y = 3 + arc4random_uniform(LAYER_HEIGHT - 6);
            uint16_t closeOnes = 0;
            uint16_t invalid = 0;
            for (uint16_t j = 0; j < subgroupOffset + i; j++) {
                uint16_t dx, dy;
                point_t *pj = &ret->points[j];
                dx = point->x > pj->x ? point->x - pj->x : pj->x - point->x;
                dy = point->y > pj->y ? point->y - pj->y : pj->y - point->y;
                if (!dx && !dy) {
                    invalid = 1;
                    break;
                }
                uint16_t dxy = dx + dy;
                if (j < subgroupOffset) {
                    if (dxy <= (i ? 72 : 360)) {
                        invalid = 1;
                        break;
                    }
                } else {
                    if (dxy <= 30) {
                        invalid = 1;
                        break;
                    }
                    if (dxy <= 72) {
                        closeOnes++;
                    }
                }
            }
            if (invalid || (i && closeOnes != 1)) {
                // in a study of 1000000 generated puzzles, none of these were dragons
                if (i == 0 && subgroup) {
                    subgroup--;
                    break;
                } else {
                    i--;
                }
            }
        }
    }
}

static puzzle_t readPuzzle0(int fd) {
    uint16_t num_points;
    READ(fd, num_points);
    size_t size = (sizeof(saved_puzzle_t));
    puzzle_t puzzle = malloc(size);
    Lseek(fd, 0, SEEK_SET);
    Read(fd, puzzle, size);
    return puzzle;
}

puzzle_t readPuzzle(int fd) {
    uint16_t version;
    READ(fd, version);
    return readPuzzle0(fd);
}
