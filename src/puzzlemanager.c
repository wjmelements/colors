#include "capitalC.h"
#include "panic.h"
#include "puzzlemanager.h"
#include "puzzlestorage.h"
#include "user.h"

#include <pthread.h>
#include <stdatomic.h>

static pthread_t puzzleThread = 0;

static volatile puzzle_t storedPuzzles[NUM_DIFFICULTIES][PUZZLE_STORAGE];
uint32_t storedPuzzlesSmallestTileSize[NUM_DIFFICULTIES][PUZZLE_STORAGE];
static volatile puzzle_t bestPuzzles[PUZZLE_STORAGE] = {0,0};
static volatile uint32_t bestEdgeCounts[PUZZLE_STORAGE] = {0,0};

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

extern user_t user;

static availability_t latestAvailability;

void getLatestAvailability(availability_t *availability) {
    *availability = latestAvailability;
}

static void insertDragonPuzzleAt(puzzle_t puzzle, uint32_t edgeCount, uint8_t index) {
    pthread_mutex_lock(&mutex);
    bestEdgeCounts[index] = edgeCount;
    puzzle_t to_free = bestPuzzles[index];
    bestPuzzles[index] = puzzle;
    pthread_mutex_unlock(&mutex);
    free(to_free);
}

static void insertPuzzleAt(puzzle_t puzzle, difficulty_t difficulty, uint32_t smallestTileSize) {
    volatile puzzle_t *loc = storedPuzzles[difficulty];
    for (uint8_t i = 0; i < PUZZLE_STORAGE; i++) {
        uint32_t *currentTileSize = &storedPuzzlesSmallestTileSize[difficulty][i];
        puzzle_t expected = loc[i];
        if (!expected) {
            if (!atomic_compare_exchange_strong(loc + i, &expected, puzzle)) {
                // concurrent modification
                i = -1;
                continue;
            }
            *currentTileSize = smallestTileSize;

            unsigned alreadyHadOne = latestAvailability.bits & 1 << difficulty;
            latestAvailability.bits |= 1 << difficulty;
            if (!alreadyHadOne) {
                onAvailabilityChanged();
            }
            return;
        }
        if (*currentTileSize < smallestTileSize) {
            if (!atomic_compare_exchange_strong(loc + i, &expected, puzzle)) {
                i = -1;
                continue;
            }

            uint32_t tmp = *currentTileSize;
            *currentTileSize = smallestTileSize;
            smallestTileSize = tmp;

            puzzle = expected;
        }
    }
    free(puzzle);
}

static puzzle_t snatchPuzzleDifficulty(difficulty_t difficulty) {
    volatile puzzle_t *loc = storedPuzzles[difficulty];
    puzzle_t ret = 0;
    for (uint8_t i = 0; i < PUZZLE_STORAGE; i++) {
        if ((ret = loc[i])) {
            if (atomic_compare_exchange_strong(loc + i, &ret, 0)) {
                break;
            } else {
                // concurrent modification; start over
                i = -1;
                continue;
            }
        }
    }
    if (!ret) {
        unsigned noMoreLeft = (latestAvailability.bits & 1 << difficulty);
        latestAvailability.bits &= ~(1 << difficulty);
        if (noMoreLeft) {
            onAvailabilityChanged();
        }
    }
    return ret;
}

static inline puzzle_t snatchPuzzle() {
    return snatchPuzzleDifficulty(user.preferredDifficulty);
}

static void insertDragonPuzzle(puzzle_t puzzle, uint32_t edgeCount, uint32_t smallestTileSize) {
    for (uint8_t i = 0; i < PUZZLE_STORAGE; i++) {
        if (!bestEdgeCounts[i]) {
            insertDragonPuzzleAt(puzzle, edgeCount, i);
            unsigned hasFound = latestAvailability.hasDragon;
            if (!hasFound) {
                latestAvailability.hasDragon = 1;
                onAvailabilityChanged();
            }
            return;
        }
    }
    uint32_t leastEdgeCount = edgeCount;
    uint8_t leastIndex = ~0;
    for (uint8_t i = 0; i < PUZZLE_STORAGE; i++) {
        if (bestEdgeCounts[i] < leastEdgeCount) {
            leastIndex = i;
            leastEdgeCount = bestEdgeCounts[i];
        }
    }
    if (leastEdgeCount != edgeCount) {
        insertDragonPuzzleAt(puzzle, edgeCount, leastIndex);
    } else {
        insertPuzzleAt(puzzle, DRAGON_DIFFICULTY, smallestTileSize);
    }
}

#define MAX_HATCH 44
#define MIN_CHICKEN 45
#define MAX_CHICKEN 46
#define MIN_ALLIGATOR 47
#define MAX_ALLIGATOR 48
#define MIN_DRAGON 49

difficulty_t difficultyForEdgeCount(uint32_t edgeCount) {
    if (edgeCount < MIN_ALLIGATOR) {
        if (edgeCount < MIN_CHICKEN) {
            return HATCH_DIFFICULTY;
        } else {
            return CHICKEN_DIFFICULTY;
        }
    } else {
        if (edgeCount < MIN_DRAGON) {
            return ALLIGATOR_DIFFICULTY;
        } else {
            return DRAGON_DIFFICULTY;
        }
    }
}
 
void insertPuzzle(puzzle_t puzzle, uint32_t edgeCount, uint32_t smallestTileSize) {
    // binary classifier
    if (edgeCount < MIN_ALLIGATOR) {
        if (edgeCount < MIN_CHICKEN) {
            insertPuzzleAt(puzzle, HATCH_DIFFICULTY, smallestTileSize);
        } else {
            insertPuzzleAt(puzzle, CHICKEN_DIFFICULTY, smallestTileSize);
        }
    } else {
        if (edgeCount < MIN_DRAGON) {
            insertPuzzleAt(puzzle, ALLIGATOR_DIFFICULTY, smallestTileSize);
        } else {
            insertDragonPuzzle(puzzle, edgeCount, smallestTileSize);
        }
    }
}

static puzzle_t getNextDragonPuzzle() {
    uint8_t index = 0;
    pthread_mutex_lock(&mutex);
    uint32_t bestEdgeCount = bestEdgeCounts[0];
    for (uint8_t i = 1; i < PUZZLE_STORAGE; i++) {
        if (bestEdgeCount < bestEdgeCounts[i]) {
            index = i;
        }
    }
    puzzle_t ret = bestPuzzles[index];
    bestEdgeCounts[index] = 0;
    bestPuzzles[index] = 0;
    pthread_mutex_unlock(&mutex);
    if (ret) {
        return ret;
    }
    return snatchPuzzleDifficulty(DRAGON_DIFFICULTY);
}

static inline puzzle_t awaitNextDragonPuzzle() {
    do {
        for (uint8_t i = 0; i < PUZZLE_STORAGE; i++) {
            if (bestEdgeCounts[i]) {
                return getNextDragonPuzzle();
            }
        }
        puzzle_t letDownPuzzle = snatchPuzzleDifficulty(ALLIGATOR_DIFFICULTY);
        if (letDownPuzzle) {
            return letDownPuzzle;
        }
        sched_yield();
    } while (user.preferredDifficulty == DRAGON_DIFFICULTY);
    return 0;
}

puzzle_t getNextPuzzle() {
    if (user.preferredDifficulty == DRAGON_DIFFICULTY) {
        return getNextDragonPuzzle();
    }
    return snatchPuzzle();
}

puzzle_t awaitNextPuzzle() {
    puzzle_t puzzle;
    if (user.preferredDifficulty == DRAGON_DIFFICULTY) {
        puzzle_t response = awaitNextDragonPuzzle();
        if (response) {
            return response;
        }
        return awaitNextPuzzle();
    }
    while (!(puzzle = snatchPuzzle())) {
        sched_yield();
    }
    return puzzle;
}

static void *puzzleManager(void *arg) {
    int wasntSet = setpriority(PRIO_DARWIN_THREAD, 0, PRIO_DARWIN_BG);
    if (wasntSet) {
        perror("setpriority");
    }
    do {
        board_t *board = Malloc(sizeof(board_t));
        pthread_cleanup_push((void(*)(void*))board_destroy, board);
            puzzle_t puzzle;
            generatePuzzle(&puzzle);
            pthread_cleanup_push(free, puzzle);
                boardFrom(board, puzzle);
            pthread_cleanup_pop(0);
            insertPuzzle(puzzle, board->edgeCount, board->smallestTileSize);
        pthread_cleanup_pop(1);
        pthread_testcancel();
    } while (1);
    return arg;
}

void startPuzzleManager() {
    pthread_create(&puzzleThread, NULL, puzzleManager, NULL);
}
void pausePuzzleManager(int await) {
    pthread_cancel(puzzleThread);
    if (await) {
        pthread_join(puzzleThread, 0);
    } else {
        pthread_detach(puzzleThread);
    }
}
void resumePuzzleManager() {
    startPuzzleManager();
}

puzzle_t getNextBestFitPuzzle() {
    puzzle_t ret;
    switch (user.preferredDifficulty) {
        case DRAGON_DIFFICULTY:
            ret = getNextDragonPuzzle();
            if (ret) {
                return ret;
            }
        #define snatchCase(difficulty)\
        case difficulty:\
            ret = snatchPuzzleDifficulty(difficulty);\
            if (ret) {\
                return ret;\
            }
        snatchCase(ALLIGATOR_DIFFICULTY);
        snatchCase(CHICKEN_DIFFICULTY);
        snatchCase(HATCH_DIFFICULTY);
        #undef snatchCase
            break;
        default:
            panic("unexpected difficulty");
    }
    return NULL;
}

puzzle_t awaitNextBestFitPuzzle() {
    puzzle_t ret;
    while (1) {
        ret = getNextBestFitPuzzle();
        if (ret) {
            return ret;
        }
        sched_yield();
    }
}
