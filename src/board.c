#include "board.h"

#include "panic.h"
#include "queue.h"
QUEUE(pq, point_t)

#ifndef MAX
#define MAX(a,b) (a > b ? a : b)
#endif

#ifndef MIN
#define MIN(a,b) (a < b ? a : b)
#endif

static void mapPixels(board_t *restrict board, puzzle_t puzzle) {
    // TODO see if can do this with gfx card
    // for now do bf search
    pq_t queue;
    pq_init(&queue);
    bitmap_t visited;
    bitmap_init(&visited, LAYER_WIDTH * LAYER_HEIGHT);
    uint32_t tileSizes[NUM_GROUPS];
    bzero(tileSizes, sizeof(tileSizes));

    for (uint16_t i = 0; i < puzzle->num_points; i++) {
        point_t point = puzzle->points[i];
        bitmap_set(visited, indexFor(point.x, point.y));
        pq_push(&queue, point);
        board->groupForPixel[point.x][point.y] = i;
        tileSizes[i] = 1;
    }
    uint32_t edgeCount = 0;
    bitmap_init(&board->edges, NUM_EDGES);
    do {
        point_t current = pq_pop(&queue);
        group_t currentGroup = board->groupForPixel[current.x][current.y];
        uint16_t x, y;
        #define expand(X,Y)\
        x = X, y = Y;\
        if (x < LAYER_WIDTH && y < LAYER_HEIGHT) {\
            uint32_t index = indexFor(x,y);\
            passert(index < LAYER_WIDTH * LAYER_HEIGHT);\
            point_t point = { x,y };\
            if (bitmap_isSet(visited, index)) {\
                group_t otherGroup = board->groupForPixel[x][y];\
                if (currentGroup != otherGroup) {\
                    group_t g1 = MIN(currentGroup, otherGroup);\
                    group_t g2 = MAX(currentGroup, otherGroup);\
                    uint32_t adjIndex = adjIndexFor(g1, g2);\
                    if (!bitmap_isSet(board->edges, adjIndex)) {\
                        edgeCount++;\
                        bitmap_set(board->edges, adjIndex);\
                    }\
                }\
            } else {\
                bitmap_set(visited, index);\
                board->groupForPixel[x][y] = currentGroup;\
                tileSizes[currentGroup]++;\
                pq_push(&queue, point);\
            }\
        }
        expand(current.x - 1, current.y);
        expand(current.x + 1, current.y);
        expand(current.x, current.y - 1);
        expand(current.x, current.y + 1);
        #undef expand
    } while (pq_ready(&queue));
    board->edgeCount = edgeCount;
    pq_destroy(&queue);
    bitmap_destroy(visited);
    board->smallestTileSize = tileSizes[0];
    for (group_t i = 1; i < NUM_GROUPS; i++) {
        if (board->smallestTileSize > tileSizes[i]) {
            board->smallestTileSize = tileSizes[i];
        }
    }
}

void boardFrom(board_t *restrict board, puzzle_t puzzle) {
    mapPixels(board, puzzle);
}
