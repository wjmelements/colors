#include "panic.h"
#include "user.h"
#include "colorsets.h"

uint32_t user_puzzlesPerfected(user_t *user) {
    uint32_t puzzlesPerfected = 0;
    puzzlesPerfected |= user->header.puzzlesPerfectedLeastTwoBits;
    puzzlesPerfected |= (user->header.puzzlesPerfectedNextSixBits << 2);
    puzzlesPerfected |= (user->header.puzzlesPerfectedHighestSixBits << 8);
    return puzzlesPerfected;
}
void user_incrementPuzzlesPerfected(user_t *user) {
    union header tmp;
    passert(sizeof(tmp) == sizeof(tmp.bits), "required for bit trick");
    tmp.bits = user->header.bits;
    uint32_t puzzlesPerfected = user_puzzlesPerfected(user);
    puzzlesPerfected++;
    tmp.puzzlesPerfectedLeastTwoBits = puzzlesPerfected & 0x3;
    tmp.puzzlesPerfectedNextSixBits = (puzzlesPerfected & 0xFC) >> 2;
    tmp.puzzlesPerfectedHighestSixBits = (puzzlesPerfected & 0x3F00) >> 8;

    user->header.bits = tmp.bits;
}
void user_upgrade(user_t *user) {
    if (piToggle(user->piBestTime.value) <= 0.5) {
        user->piBestTime.value = piToggle(INFINITY);
    }
    for (uint8_t i = 0; i < 4; i++) {
        if (piToggle(user->piBestTimes[i].value) <= 0.5) {
            user->piBestTimes[i].value = piToggle(INFINITY);
        }
    }
    if (fabs(user->pieAngle) >= M_PI_2) {
        user->pieAngle = 0;
    }
    color_t colors[4];
    for (int i = 0; i < 4; i++) {
        colors[i] = piColor(user->piColors[i].color);
    }
    int invalidColorSet = 1;
    for (int i = 0; i < NUM_COLORSETS; i++) {
        int valid = 1;
        for (int j = 0; j < 4; j++) {
            if (colors[j] != colorSets[i][j]) {
                valid = 0;
                break;
            }
        }
        if (valid) {
            invalidColorSet = 0;
            break;
        }
    }
    if (invalidColorSet) {
        for (int i = 0; i < 4; i++) {
            user->piColors[i].color = piColor(colorSets[0][i]);
        }
    }
    
    if (user->header.reserved && user->preferredDifficulty != DRAGON_DIFFICULTY) {
        user->header.reserved = 0;
        user->preferredDifficulty = DRAGON_DIFFICULTY;
    }
    // the following allEven hack occasionally but pseudorandomly reduces past history on startup
    int allEven = 1;
    for (int i = 0; i < 4; i++) {
        if (user->colorCounts[i] % 3 || user->colorCounts[i] < 20) {
            allEven = 0;
            break;
        }
    }
    if (allEven) {
        for (int i = 0; i < 4; i++) {
            user->colorCounts[i] /= 3;
        }
    }
    if (user->reserved) {
        user->latestStreaks[0] = user->reserved;
        user->reserved = 0;
    }
    // validate puzzle streak
    if ((piXor(user->piLongestStreak) & 0x1FF) != user->header.longestStreakCheckBits) {
        user->piLongestStreak = piXor(0);
        user->header.longestStreakCheckBits = 0;
    }
    // upgrade through versions
    switch (user->version) {
        default:
            user->version = 0;
        case 0:
            // need to set individual puzzle times
            for (difficulty_t d = 0; d < 4; d++) {
                user->piBestTimes[d].value = piToggle(INFINITY);
            }
            break;
        case 1:
            break;
    }
    user->version = 1;
}
void user_setHighestScore(user_t *user, uint32_t score) {
    union header tmp;

    tmp.bits = user->header.bits;
    tmp.highestSurvivalScoreLeastTwoBits = score & 0x3;
    tmp.highestSurvivalScoreNextThreeBits = (score & 0x1C) >> 2;
    tmp.highestSurvivalScoreNextFourBits = (score & 0x1E0) >> 5;
    tmp.highestSurvivalScoreMiddleSevenBits = (score & 0xFE00) >> 9;

    user->header.bits = tmp.bits;
}
uint32_t user_highestScore(const user_t *user) {
    uint32_t score = 0;
    score |= user->header.highestSurvivalScoreMiddleSevenBits;
    score <<= 4;
    score |= user->header.highestSurvivalScoreNextFourBits;
    score <<= 3;
    score |= user->header.highestSurvivalScoreNextThreeBits;
    score <<= 2;
    score |= user->header.highestSurvivalScoreLeastTwoBits;
    return score;
}
