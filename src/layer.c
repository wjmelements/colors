#include <assert.h>
#include "capitalC.h"

#include "color.h"
#include "colormap.h"
#include "user.h"

extern user_t user;

static inline void _nullColorGroup(layer_t *layer, group_t group, uint32_t selected) {
    if (selected) {
        double tangents[2];
        tangents[0] = tan(user.pieAngle - M_PI_2);
        tangents[1] = tan(user.pieAngle);
        color_t colors[4];
        for (uint16_t i = 0; i < 4; i++) {
            colors[i] = layer->colors[i];
        }
        const point_t *midPoint = &layer->midPoints[group];
        for (uint32_t i = layer->pointsForGroup[group].num_points; --i < layer->pointsForGroup[group].num_points;) {

            const point_t *point = &layer->pointsForGroup[group].points[i];
            if (layer->toEdgeDistances[indexFor(point->x, point->y)] < 5) {
                int32_t translationX = point->x - midPoint->x;
                int32_t translationY = point->y - midPoint->y;
                color_t borderColor;
                if (translationX * tangents[0] < translationY) {
                    if (translationX * tangents[1] < translationY) {
                        borderColor = colors[0];
                    } else {
                        borderColor = colors[3];
                    }
                } else {
                    if (translationX * tangents[1] < translationY) {
                        borderColor = colors[1];
                    } else {
                        borderColor = colors[2];
                    }
                }
                setColor(&layer->pixels[point->y][point->x], borderColor);
            } else {
                setColor(&layer->pixels[point->y][point->x], NULL_COLOR);
            }
        }
    } else {
        for (uint32_t i = layer->pointsForGroup[group].num_points; --i < layer->pointsForGroup[group].num_points;) {

            const point_t *point = &layer->pointsForGroup[group].points[i];

            if (layer->toEdgeDistances[indexFor(point->x, point->y)] < 1) {
                setColor(&layer->pixels[point->y][point->x], BLACK);
            } else {
                setColor(&layer->pixels[point->y][point->x], NULL_COLOR);
            }
        }
    }

}

static inline void _colorGroup(layer_t *layer, group_t group, color_t color, uint32_t invalid, uint32_t selected) {
    if (color == NULL_COLOR) {
        _nullColorGroup(layer, group, selected);
    } else {
        color_t edgeColor;
        if (invalid) {
            edgeColor = borderColorFor(color);
        } else {
            edgeColor = color;
        }
        for (uint32_t i = layer->pointsForGroup[group].num_points; --i < layer->pointsForGroup[group].num_points;) {
            point_t *point = &layer->pointsForGroup[group].points[i];
            setColor(&layer->pixels[point->y][point->x], interpolate(edgeColor, color, layer->toEdgeDistances[indexFor(point->x, point->y)]));
        }
    }
}

#include "board_t.h"

int colorGroup(board_t *board, layer_t *layer, group_t group, color_t color, uint32_t active) {
    setGroupColor(layer, group, color);

    uint32_t invalidated;
    int wasInvalid = 0;
    if (color == NULL_COLOR) {
        _nullColorGroup(layer, group, active);
        invalidated = 0;
        for (group_t g1 = 0; g1 < group; g1++) {
            if (bitmap_isSet(board->edges, adjIndexFor(g1, group))) {
                if (layer->invalidEdges[g1] == 1 << group) {
                    invalidated |= 1 << g1;
                }
                layer->invalidEdges[group] &= ~(1 << g1);
                layer->invalidEdges[g1] &= ~(1 << group);
            }
        }
        for (group_t g2 = group + 1; g2 < NUM_GROUPS; g2++) {
            if (bitmap_isSet(board->edges, adjIndexFor(group, g2))) {
                if (layer->invalidEdges[g2] == 1 << group) {
                    invalidated |= 1 << g2;
                }
                layer->invalidEdges[g2] &= ~(1 << group);
                layer->invalidEdges[group] &= ~(1 << g2);
            }
        }
    } else {
        invalidated = 1 << group;
        for (group_t g1 = 0; g1 < group; g1++) {
            if (bitmap_isSet(board->edges, adjIndexFor(g1, group))) {
                if (layer->groupColors[g1] == layer->groupColors[group]) {
                    if (!layer->invalidEdges[g1]) {
                        invalidated |= 1 << g1;
                    }
                    layer->invalidEdges[g1] |= 1 << group;
                    layer->invalidEdges[group] |= 1 << g1;
                    wasInvalid = 1;
                } else {
                    if (layer->invalidEdges[g1] == 1 << group) {
                        invalidated |= 1 << g1;
                    }
                    layer->invalidEdges[g1] &= ~(1 << group);
                    layer->invalidEdges[group] &= ~(1 << g1);
                }
            }
        }
        for (group_t g2 = group + 1; g2 < NUM_GROUPS; g2++) {
            if (bitmap_isSet(board->edges, adjIndexFor(group, g2))) {
                if (layer->groupColors[group] == layer->groupColors[g2]) {
                    if (!layer->invalidEdges[g2]) {
                        invalidated |= 1 << g2;
                    }
                    layer->invalidEdges[g2] |= 1 << group;
                    layer->invalidEdges[group] |= 1 << g2;
                    wasInvalid = 1;
                } else {
                    if (layer->invalidEdges[g2] == 1 << group) {
                        invalidated |= 1 << g2;
                    }
                    layer->invalidEdges[group] &= ~(1 << g2);
                    layer->invalidEdges[g2] &= ~(1 << group);
                }
            }
        }
    }
    for (group_t g = 0; invalidated && g < NUM_GROUPS; g++) {
        if (invalidated & 1) {
            _colorGroup(layer, g, layer->groupColors[g], layer->invalidEdges[g], 0);
        }
        invalidated >>= 1;
    }
    return wasInvalid;
}

static void resetLayerColors(layer_t *layer) {
    for (group_t group = 0; group < NUM_GROUPS; group++) {
        _colorGroup(layer, group, layer->groupColors[group], 0, 0);
    }
}

#ifndef MAX
#define MAX(X,Y) (X > Y ? X : Y)
#endif

#ifndef MIN
#define MIN(X,Y) (X < Y ? X : Y)
#endif

#include "queue.h"
QUEUE(pq, point_t);

static inline int inBoard(point_t point) {
    return point.x < LAYER_WIDTH && point.y < LAYER_HEIGHT;
}

static inline int test(board_t *board, point_t point, group_t group) {
    return board->groupForPixel[point.x][point.y] != group;
}

static void _layerStart(layer_t *layer) {
    for (int i = 0; i < 4; i++) {
        layer->colors[i] = piColor(user.piColors[i].color);
    }
}

#include "colorsets.h"

void layer_rotateColors(layer_t *layer) {
    colorset_t uColors;
    for (int i = 0; i < 4; i++) {
        uColors[i] = piColor(user.piColors[i].color);
    }
    if (!colorsets_equal(uColors, layer->colors)) {
        return;
    }
    _layerStart(layer);
}

void layerFrom(board_t *board, layer_t *layer) {
    layer_init(layer);
    _layerStart(layer);
    for (group_t group = 0; group < NUM_GROUPS; group++) {
        layer->extremes[group].val = 0x00000000FFFFFFFF;
    }
    uint16_t x = 0, y = 0;
    bitmap_t visited;
    bitmap_init(&visited, LAYER_WIDTH * LAYER_HEIGHT);
    pq_t queue;
    pq_init(&queue);
    for (uint32_t i = 0; i < LAYER_WIDTH * LAYER_HEIGHT; i++) {
        group_t group = board->groupForPixel[x][y];
        assert(i == indexFor(x,y));
        point_t points[4] = {
            {x-1,y},
            {x+1,y},
            {x,y-1},
            {x,y+1}
        };
        uint16_t edgeDistance = 3;
        for (uint16_t i = 0; i < 4; i++) {
            if (inBoard(points[i])) {
                if (test(board, points[i], group)) {
                    edgeDistance = 0;
                    break;
                }
            } else {
                edgeDistance = 1;
            }
        }
        if (edgeDistance < 2) {
            point_t point = {x,y};
            bitmap_set(visited, i);
            layer->toEdgeDistances[i] = edgeDistance;
            pq_push(&queue, point);
        }
        vp_append(&layer->pointsForGroup[group], (point_t) {x,y});
        extreme_t *extreme = &layer->extremes[group];
        extreme->lowX = MIN(x, extreme->lowX);
        extreme->lowY = MIN(y, extreme->lowY);
        extreme->highX = MAX(x, extreme->highX);
        extreme->highY = MAX(y, extreme->highY);
        if (++y == LAYER_HEIGHT) {
            y = 0;
            x++;
        }
    }
    do {
        point_t current = pq_pop(&queue);
        uint16_t distance = layer->toEdgeDistances[indexFor(current.x, current.y)];
        uint16_t x, y;
        #define expand(X,Y)\
            x = X; y = Y;\
            if (x < LAYER_WIDTH && y < LAYER_HEIGHT) {\
                uint32_t index = indexFor(x,y);\
                point_t point = {x,y};\
                if (!bitmap_isSet(visited, index)) {\
                    bitmap_set(visited, index);\
                    pq_push(&queue, point);\
                    layer->toEdgeDistances[index] = distance + 1;\
                }\
            }
        expand(current.x, current.y - 1);
        expand(current.x, current.y + 1);
        expand(current.x - 1, current.y);
        expand(current.x + 1, current.y);
        #undef expand
    } while (pq_ready(&queue));
    pq_destroy(&queue);
    bitmap_destroy(visited);

    resetGroupColors(layer);
    resetLayerColors(layer);
}

#define checkForced(lessG, moreG)\
        if (bitmap_isSet(board->edges, adjIndexFor(lessG, moreG))) {\
            color_t color = layer->groupColors[g];\
            if (color == NULL_COLOR) {\
                taken |= 1 << 4;\
            } else for (uint8_t i = 0; i < 4; i++) {\
                if (color == layer->colors[i]) {\
                    taken &= ~(1 << i);\
                    break;\
                }\
            }\
        }

color_t forced(board_t *board, layer_t *layer, group_t group) {
    uint8_t taken = 0xF;
    for (group_t g = 0; g < group; g++) {
        checkForced(g, group);
    }
    for (group_t g = group + 1; g < NUM_GROUPS; g++) {
        checkForced(group, g);
    }
    if (!(taken & 0x10)) {
        if (taken & 1) {
            return layer->colors[0];
        }
        if (taken & 2) {
            return layer->colors[1];
        }
        if (taken & 4) {
            return layer->colors[2];
        }
        if (taken & 8) {
            return layer->colors[3];
        }
    }
    switch (taken & 0xF) {
        case 1 : return layer->colors[0];
        case 2 : return layer->colors[1];
        case 4 : return layer->colors[2];
        case 8 : return layer->colors[3];
        default: return NULL_COLOR;
    }
}
