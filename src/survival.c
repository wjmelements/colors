#include "survival.h"


#include "capitalC.h"
#include "colormap.h"
#include "queue.h"
QUEUE(pq, point_t)
#include "user.h"
extern user_t user; 

#include <stdlib.h>
#ifdef __linux__
#include <bsd/stdlib.h>
#endif

void survival_destroy(survival_t *survival) {
    vc_destroy(&survival->groupColors);
    for (uint16_t i = 0; i < survival->pointsForGroup.num_vps; i++) {
        vp_t *vp = &survival->pointsForGroup.vps[i];
        switch (vp->marked) {
            default:
                vp_destroy(vp);
            case 1:
            case 0:
                break;
        }
        bitmap_destroy(survival->edgesForGroup.bitmaps[i]);
    }
    vvp_destroy(&survival->pointsForGroup);
    edges_destroy(&survival->edgesForGroup);
    invalids_destroy(&survival->invalids);
}
void survival_imageTop(survival_t *survival, image_ref_t *image) {
    image->pixels = &survival->pixels[0][0];
    image->height =
        survival->heightOffset < SURVIVAL_HEIGHT - LAYER_HEIGHT
      ? 0
      : survival->heightOffset - LAYER_HEIGHT;
}
void survival_imageBottom(survival_t *survival, image_ref_t *image) {
    image->pixels = &survival->pixels[survival->heightOffset][0];
    image->height =
        survival->heightOffset < SURVIVAL_HEIGHT - LAYER_HEIGHT
      ? LAYER_HEIGHT
      : LAYER_HEIGHT + LAYER_HEIGHT - survival->heightOffset;
}
static inline uint64_t sIndex(uint16_t x, uint16_t y) {
    return y * SURVIVAL_WIDTH + x;
}

static const uint16_t invalidEdgeDistance = ~0;

static void _color(survival_t *survival, group_t groupIndex, uint64_t invalid) {
    vp_t *vp = &survival->pointsForGroup.vps[groupIndex];
    color_t color = survival->groupColors.colors[groupIndex];
    if (invalid || color == NULL_COLOR) {
        color_t edgeColor;
        if (color == NULL_COLOR) {
            // TODO midpoint border color or clear border color with background image
            edgeColor = ORCHID;
            for (uint32_t i = 0; i < vp->num_points; i++) {
                point_t *point = &vp->points[i];
                setColor(&survival->pixels[point->y][point->x], interpolate(edgeColor, color, 40 * survival->toEdgeDistances[point->y][point->x]));
            }
        } else {
            edgeColor = borderColorFor(color);
            for (uint32_t i = 0; i < vp->num_points; i++) {
                point_t *point = &vp->points[i];
                setColor(&survival->pixels[point->y][point->x], interpolate(edgeColor, color, survival->toEdgeDistances[point->y][point->x]));
            }
        }
    } else {
        for (uint32_t i = 0; i < vp->num_points; i++) {
            point_t *point = &vp->points[i];
            setColor(&survival->pixels[point->y][point->x], color);
        }
    }
}

void survival_start(survival_t *survival) {
    for (int i = 0; i < 4; i++) {
        survival->colors[(i + 2) & 3] = piColor(user.piColors[i].color);
    }
}

void survival_init(survival_t *survival) {
    vc_init(&survival->groupColors, NUM_GROUPS + 5);
    vvp_init(&survival->pointsForGroup, NUM_GROUPS + 5);
    edges_init(&survival->edgesForGroup, NUM_GROUPS + 5);
    invalids_init(&survival->invalids, NUM_GROUPS + 5);

    survival->groupOffset = 0;
    survival->heightOffset = 0;
    #ifdef __linux__
    memset(&survival->controlDistances[0][0], 0xFFFFFFFF, sizeof(survival->controlDistances));
    memset(&survival->toEdgeDistances[0][0], 0xFFFFFFFF, sizeof(survival->toEdgeDistances));
    #else
    uint64_t pattern = ~0;
    memset_pattern8(&survival->controlDistances[0][0], &pattern, sizeof(survival->controlDistances));
    memset_pattern8(&survival->toEdgeDistances[0][0], &pattern, sizeof(survival->toEdgeDistances));
    #endif
    bitmap_t visited;
    bitmap_init(&visited, SURVIVAL_WIDTH * SURVIVAL_HEIGHT);
    pq_t toExpand;
    pq_init(&toExpand);
    for (group_t group = 0; group < NUM_GROUPS * SURVIVAL_HEIGHT / LAYER_HEIGHT; group++) {
        point_t point;
        point.x = 3 + arc4random_uniform(SURVIVAL_WIDTH - 6);
        point.y = 6 + arc4random_uniform(SURVIVAL_HEIGHT - 6);
        if (bitmap_isSet(visited, sIndex(point.x,point.y))) {
            // rare, but need to try again
            group--;
            continue;
        }
        pq_push(&toExpand, point);
        survival->groupForPixel[point.y][point.x] = group;
        survival->controlDistances[point.y][point.x] = 0;
        bitmap_set(visited, sIndex(point.x,point.y));
        // TODO consider sorting points by (max)Y
        vp_t points;
        vp_init(&points, SURVIVAL_WIDTH); // TODO profile for better initial size here and other places
        bitmap_t bitmap;
        bitmap_init(&bitmap, group);
        vvp_append(&survival->pointsForGroup, points);
        vc_append(&survival->groupColors, NULL_COLOR);
        edges_append(&survival->edgesForGroup, bitmap);
        invalids_append(&survival->invalids, 0);
    }
    pq_t edgeDistancesToExpand;
    pq_init(&edgeDistancesToExpand);
    do {
        point_t current = pq_pop(&toExpand);
        const group_t currentGroup = survival->groupForPixel[current.y][current.x];
        uint16_t x, y;
        #define expand(X,Y)\
            x = X; y = Y;\
            if (x < SURVIVAL_WIDTH && y < SURVIVAL_HEIGHT) {\
                const point_t point = {x,y};\
                if (bitmap_isSet(visited, sIndex(x,y))) {\
                    if (y < LAYER_HEIGHT) {\
                        const group_t group = survival->groupForPixel[y][x];\
                        if (group != currentGroup) {\
                            if (group < currentGroup) {\
                                survival_setEdge(survival, group, currentGroup);\
                            } else if (group > currentGroup) {\
                                survival_setEdge(survival, currentGroup, group);\
                            }\
                            survival->toEdgeDistances[y][x] = 0;\
                            pq_push(&edgeDistancesToExpand, point);\
                        }\
                    }\
                } else {\
                    bitmap_set(visited, sIndex(x,y));\
                    survival->controlDistances[y][x] = survival->controlDistances[current.y][current.x] + 1;\
                    survival->groupForPixel[y][x] = currentGroup;\
                    survival->toEdgeDistances[y][x] = invalidEdgeDistance;\
                    pq_push(&toExpand, point);\
                }\
            }
        expand(current.x - 1, current.y);
        expand(current.x + 1, current.y);
        expand(current.x, current.y - 1);
        expand(current.x, current.y + 1);
        #undef expand
    } while (pq_ready(&toExpand));
    pq_destroy(&toExpand);
    do {
        point_t current = pq_pop(&edgeDistancesToExpand);
        uint16_t x, y;
        #define expand(X,Y)\
            x = X, y = Y;\
            if (x < SURVIVAL_WIDTH && y < LAYER_HEIGHT) {\
                if (survival->toEdgeDistances[y][x] == invalidEdgeDistance) {\
                    survival->toEdgeDistances[y][x] = survival->toEdgeDistances[current.y][current.x] + 1;\
                    const point_t point = {x,y};\
                    pq_push(&edgeDistancesToExpand, point);\
                }\
            }
        expand(current.x - 1, current.y);
        expand(current.x + 1, current.y);
        expand(current.x, current.y - 1);
        expand(current.x, current.y + 1);
        #undef expand
        // TODO I can improve these searches by not always looking in all four directions
    } while (pq_ready(&edgeDistancesToExpand));
    pq_destroy(&edgeDistancesToExpand);
    point_t point;
    for (uint16_t y = 0; y < LAYER_HEIGHT; y++) {
        point.y = y;
        for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
            point.x = x;
            group_t group = survival->groupForPixel[y][x];
            vp_append(&survival->pointsForGroup.vps[group], point);
        }
    }
    bitmap_destroy(visited);
    for (group_t group = 0; group < survival->groupColors.num_colors; group++) {
        _color(survival, group, 1);
    }
}
static int cleavePointsForGroup(survival_t *survival) {
    uint16_t y = survival->heightOffset;
    // TODO less redundant checks here
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        group_t group = survival->groupForPixel[y][x];
        group_t groupIndex = group - survival->groupOffset;
        uint32_t j = 0;
        vp_t *vp = &survival->pointsForGroup.vps[groupIndex];
        while (j < vp->num_points && vp->points[j].y == y) {
            j++;
        }
        vp_trimTo(vp,j);
        if (j > 0 && !vp->num_points) {
            vp_destroy(vp);
            vp->marked = 1;
        }
    }
    group_t groupTrimOffset = 0;
    int wasDefeated = 0;
    while (groupTrimOffset < survival->pointsForGroup.num_vps && survival->pointsForGroup.vps[groupTrimOffset].marked < 2) {
        if (survival->groupColors.colors[groupTrimOffset] == NULL_COLOR || !survival_valid(survival, groupTrimOffset)) {
            wasDefeated = 1;
        }
        bitmap_destroy(survival->edgesForGroup.bitmaps[groupTrimOffset]);
        groupTrimOffset++;
    }
    if (survival->groupOffset + groupTrimOffset < (NUM_GROUPS * SURVIVAL_HEIGHT / LAYER_HEIGHT)) {
        for (group_t g = groupTrimOffset; g < survival->pointsForGroup.num_vps; g++) {
            vp_t *vp = &survival->pointsForGroup.vps[g];
            if (vp->marked != 1) {
                continue;
            }
            vp->marked = 0;
            if (!survival_valid(survival, g) || survival->groupColors.colors[g] == NULL_COLOR) {
                wasDefeated = 1;
            }
        }
    }
    vvp_trimTo(&survival->pointsForGroup, groupTrimOffset);
    vc_trimTo(&survival->groupColors, groupTrimOffset);
    edges_trimTo(&survival->edgesForGroup, groupTrimOffset);
    invalids_trimTo(&survival->invalids, groupTrimOffset);
    survival->groupOffset += groupTrimOffset;
    return wasDefeated;
}
static void resetRow(survival_t *survival) {
    const uint16_t y = survival->heightOffset;
    const uint16_t prevY = y ? y - 1 : SURVIVAL_HEIGHT - 1;
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        survival->groupForPixel[y][x] = survival->groupForPixel[prevY][x];
        survival->controlDistances[y][x] = survival->controlDistances[prevY][x] + 1;
    }
}
static int cleave(survival_t *survival) {
    int wasDefeated = cleavePointsForGroup(survival);
    resetRow(survival);
    return wasDefeated;
}

static void addPoint(survival_t *survival) {
    const uint16_t y = survival->heightOffset++;
    if (survival->heightOffset >= SURVIVAL_HEIGHT) {
        survival->heightOffset -= SURVIVAL_HEIGHT;
    }
    // same average density assuming NUM_GROUPS < LAYER_HEIGHT
    uint32_t p = arc4random_uniform(LAYER_HEIGHT);
    if (p >= NUM_GROUPS) {
        return;
    }
    // add point
    uint32_t x = 3 + arc4random_uniform(SURVIVAL_WIDTH - 6);
    const group_t group = survival->pointsForGroup.num_vps + survival->groupOffset;
    survival->groupForPixel[y][x] = group;
    survival->controlDistances[y][x] = 0;
    vp_t vp;
    vp_init(&vp, SURVIVAL_WIDTH);
    bitmap_t bitmap;
    bitmap_init(&bitmap, survival->groupColors.num_colors);
    vc_append(&survival->groupColors, NULL_COLOR);
    vvp_append(&survival->pointsForGroup, vp);
    edges_append(&survival->edgesForGroup, bitmap);
    invalids_append(&survival->invalids, 0);
    
    // bfs
    bitmap_t visited;
    bitmap_init(&visited, SURVIVAL_WIDTH * SURVIVAL_HEIGHT);
    pq_t queue;
    pq_init(&queue);
    point_t point = {x,y};
    pq_push(&queue, point);
    do {
        point_t current = pq_pop(&queue);
        uint16_t x, y;
        #define expand(X,Y)\
        x = X, y = Y;\
        if (x < SURVIVAL_WIDTH && y < SURVIVAL_HEIGHT) {\
            uint32_t index = x + y * SURVIVAL_WIDTH;\
            if (!bitmap_isSet(visited, index)) {\
                uint16_t controlDistance = survival->controlDistances[current.y][current.x] + 1;\
                if (controlDistance < survival->controlDistances[y][x]) {\
                    bitmap_set(visited, index);\
                    survival->groupForPixel[y][x] = group;\
                    survival->controlDistances[y][x] = controlDistance;\
                    pq_push(&queue, (point_t){x,y});\
                }\
            }\
        }
        expand(current.x - 1, current.y);
        expand(current.x + 1, current.y);
        expand(current.x, current.y - 1);
        #undef expand
    } while (pq_ready(&queue));
    pq_destroy(&queue);
    bitmap_destroy(visited);
}
static void bindPoints(survival_t *survival) {
    uint16_t rowToBind = survival->heightOffset + LAYER_HEIGHT;
    if (rowToBind >= SURVIVAL_HEIGHT) {
        rowToBind -= SURVIVAL_HEIGHT;
    }
    const uint16_t y = rowToBind;
    const uint16_t prevY = rowToBind ? rowToBind - 1 : SURVIVAL_HEIGHT - 1;
    pq_t edgePoints;
    pq_init(&edgePoints);
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        point_t point = {x,y};
        survival->controlDistances[y][x] = 0;
        group_t group = survival->groupForPixel[y][x];
        group_t groupIndex = group - survival->groupOffset;
        vp_append(&survival->pointsForGroup.vps[groupIndex], point);
        group_t prevGroupIndex = survival->groupForPixel[prevY][x] - survival->groupOffset;
        if (groupIndex == prevGroupIndex) {
            if (x && survival->toEdgeDistances[y][x-1] < survival->toEdgeDistances[prevY][x]) {
                survival->toEdgeDistances[y][x] = survival->toEdgeDistances[y][x-1] + 1;
            } else {
                survival->toEdgeDistances[y][x] = survival->toEdgeDistances[prevY][x] + 1;
            }
        } else {
            point_t prevPoint = {x,prevY};
            pq_push(&edgePoints, point);
            pq_push(&edgePoints, prevPoint);
            survival->toEdgeDistances[y][x] = 0;
            survival->toEdgeDistances[prevY][x] = 0;
            if (groupIndex > prevGroupIndex) {
                survival_setEdge(survival, prevGroupIndex, groupIndex);
            } else {
                survival_setEdge(survival, groupIndex, prevGroupIndex);
            }
        }

        if (x) {
            group_t leftGroupIndex = survival->groupForPixel[y][x-1] - survival->groupOffset;
            if (groupIndex != leftGroupIndex) {
                point_t prevPoint = {x-1,y};
                pq_push(&edgePoints, point);
                pq_push(&edgePoints, prevPoint);
                survival->toEdgeDistances[y][x] = 0;
                survival->toEdgeDistances[y][x-1] = 0;
                if (groupIndex > leftGroupIndex) {
                    survival_setEdge(survival, leftGroupIndex, groupIndex);
                } else {
                    survival_setEdge(survival, groupIndex, leftGroupIndex);
                }
            }
        }
    }
    uint16_t yEdgeBarrier = survival->heightOffset ? survival->heightOffset - 1 : SURVIVAL_HEIGHT - 1;
    while (pq_ready(&edgePoints)) {
        point_t toExpand = pq_pop(&edgePoints);
        uint16_t x, y;
        uint16_t nextDistance = survival->toEdgeDistances[toExpand.y][toExpand.x] + 1;
        #define expand(X,Y)\
            x = X;\
            y = Y;\
            if (x < SURVIVAL_WIDTH && y != yEdgeBarrier) {\
                if (survival->toEdgeDistances[y][x] > nextDistance) {\
                    point_t p = { x,y };\
                    pq_push(&edgePoints, p);\
                    survival->toEdgeDistances[y][x] = nextDistance;\
                }\
            }
        expand(toExpand.x + 1, toExpand.y);
        expand(toExpand.x - 1, toExpand.y);
        expand(toExpand.x, toExpand.y ? toExpand.y - 1 : SURVIVAL_HEIGHT - 1);
        #undef expand
    }    
    pq_destroy(&edgePoints);
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        group_t groupIndex = survival->groupForPixel[y][x] - survival->groupOffset;
        color_t color = survival->groupColors.colors[groupIndex];
        color_t edgeColor;
        if (color == NULL_COLOR) {
            edgeColor = ORCHID;// TODO midpoints
            setColor(&survival->pixels[y][x], interpolate(edgeColor, color, 40 * survival->toEdgeDistances[y][x]));
        } else {
            if (survival_valid(survival, groupIndex)) {
                setColor(&survival->pixels[y][x], color);
            } else {
                edgeColor = borderColorFor(color);
                setColor(&survival->pixels[y][x], interpolate(edgeColor, color, survival->toEdgeDistances[y][x]));
            }
        }
    }
}

int survival_shift(survival_t *survival) {
    bindPoints(survival);
    int wasDefeated = cleave(survival);
    addPoint(survival);
    return wasDefeated;
}

int survival_color(survival_t *survival, group_t group, color_t color, uint64_t *outIllegal) {
    group_t groupIndex = group - survival->groupOffset;
    if (survival->groupColors.colors[groupIndex] == color) {
        return 0;
    }
    survival->groupColors.colors[groupIndex] = color;
    uint64_t invalidated = 0,
                illegal = 0;
    for (group_t g = 0; g < groupIndex; g++) {
        if (survival_hasEdge(survival, g, groupIndex)) {
            if (survival->groupColors.colors[g] == color && color != NULL_COLOR) {
                illegal |= 1ull << g;
                if (survival_valid(survival, g)) {
                    invalidated |= 1ull << g;
                }
                survival_setInvalid(survival, g, groupIndex);
            } else {
                if (survival_onlyInvalid(survival, g, groupIndex)) {
                    invalidated |= 1ull << g;
                }
                survival_unsetInvalid(survival, g, groupIndex);
            }
        }
    }
    for (group_t g = groupIndex + 1; g < survival->invalids.num_invalids; g++) {
        if (survival_hasEdge(survival, groupIndex, g)) {
            if (survival->groupColors.colors[g] == color && color != NULL_COLOR) {
                illegal |= 1ull << g;
                if (survival_valid(survival, g)) {
                    invalidated |= 1ull << g;
                }
                survival_setInvalid(survival, groupIndex, g);
            } else {
                if (survival_onlyInvalid(survival, g, groupIndex)) {
                    invalidated |= 1ull << g;
                }
                survival_unsetInvalid(survival, groupIndex, g);
            }
        }
    }
    *outIllegal = illegal;
    _color(survival, groupIndex, illegal);
    for (group_t g = 0; invalidated && g < survival->invalids.num_invalids; g++) {
        if (invalidated & 1) {
            _color(survival, g, illegal & 1);
        }
        invalidated >>= 1;
        illegal >>= 1;
    }
    return 1;
}

#define checkForced(lessG, moreG)\
    if (survival_hasEdge(survival, lessG, moreG)) {\
        color_t color = survival->groupColors.colors[groupOffset];\
        if (color == NULL_COLOR) {\
            taken |= 0x10;\
        } else for (int i = 0; i < 4; i++) {\
            if (color == survival->colors[i]) {\
                taken &= ~(1ull << i);\
                break;\
            }\
        }\
    }

color_t survival_forced(survival_t *survival, group_t group) {
    uint8_t taken = 0xF;
    group_t offset = group - survival->groupOffset;
    for (group_t groupOffset = 0; groupOffset < offset; groupOffset++) {
        checkForced(groupOffset, offset);
    }
    group_t endOffset = survival->groupColors.num_colors;
    for (group_t groupOffset = offset + 1; groupOffset < endOffset; groupOffset++) {
        checkForced(offset, groupOffset);
    }
    if (!(taken & 0x10)) {
        for (uint8_t i = 0; i < 4; i++) {
            if (taken & 1ull << i) {
                return survival->colors[i];
            }
        }
    }
    switch (taken & 0xF) {
        case 1 : return survival->colors[0];
        case 2 : return survival->colors[1];
        case 4 : return survival->colors[2];
        case 8 : return survival->colors[3];
        default: return NULL_COLOR;
    }
}

#include "colorsets.h"

void survival_rotateColors(survival_t *survival) {
    colorset_t uColors;
    for (int i = 0; i < 4; i++) {
        uColors[i] = piColor(user.piColors[i].color);
    }
    if (!colorsets_equal(uColors, survival->colors)) {
        return;
    }
    survival_start(survival);
}
