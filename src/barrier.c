#include "barrier.h"
#include <pthread.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <sched.h>

struct barrier {
    pthread_mutex_t lock;
    long count;
    long arrived;
    long* exit;
};
void barrier_init(struct barrier* this) {
    pthread_mutex_init(&this->lock, NULL);
    this->exit = malloc(sizeof(long));
    *this->exit = 0;
    this->count = 0;
    this->arrived = 0;
}
void barrier_destroy(struct barrier* this) {
    pthread_mutex_destroy(&this->lock);
    free(this->exit);
}
void barrier_delete(struct barrier* this) {
    barrier_destroy(this);
    free(this);
}
struct barrier* barrier_new() {
    void* p = malloc(sizeof(struct barrier));
    struct barrier* ptr = (struct barrier*) p;
    barrier_init(ptr);
    return ptr;
}
void barrier_register(struct barrier* this) {
    pthread_mutex_lock(&this->lock);
    this->count++;
    pthread_mutex_unlock(&this->lock);
}
void barrier_arrive(struct barrier* this) {
    pthread_mutex_lock(&this->lock);
    long* mine = this->exit;
    this->arrived++;
    if (this->arrived == this->count) {
        this->exit = malloc(sizeof(long));
        *this->exit = 0;
        long arrived = this->arrived;
        this->arrived = 0;
        *mine = arrived;
    }
    pthread_mutex_unlock(&this->lock);
    while (!*mine) {
        // TODO futex if Linux
        sched_yield();
    }
    if (atomic_fetch_sub(mine, 1) == 0) {
        free(mine);
    }
}
void barrier_deregister(struct barrier* this) {
    pthread_mutex_lock(&this->lock);
    this->count--;
    long* mine = this->exit;
    if (this->arrived == this->count) {
        this->exit = malloc(sizeof(long));
        *this->exit = 0;
        long arrived = this->arrived;
        if (arrived == 0) {
            free(mine);
        } else {
            this->arrived = 0;
            *mine = arrived;
            // someone else will free it
        }
    }
    pthread_mutex_unlock(&this->lock);
}
