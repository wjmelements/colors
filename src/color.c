#include "color.h"
#include <string.h>

void resetGroupColors(layer_t *layer) {
    color_t color = NULL_COLOR;
    #ifdef __linux__
    for (group_t g = 0; g < NUM_GROUPS; g++) {
        layer->groupColors[g] = color;
    }
    #else
    memset_pattern4(layer->groupColors, &color, sizeof(layer->groupColors));
    #endif
    memset(layer->invalidEdges, 0, sizeof(layer->invalidEdges));
}

int layerComplete(layer_t *layer) {
    for (group_t i = 0; i < NUM_GROUPS; i++) {
        if (layer->groupColors[i] == NULL_COLOR) {
            return 0;
        }
        if (layer->invalidEdges[i]) {
            return 0;
        }
    }
    return 1;
}
