#include "colorsets.h"
const colorset_t colorSets[NUM_COLORSETS] = {
    {   // default
        GOLDENROD,
        PANSY_PINK,
        SALMON,
        TEAL
    },
    {   // chick
        YELLOW,
        BLUE,
        CRIMSON,
        GREEN
    },
    {   // chicken
        TEAL,
        ORCHID,
        WHITE,
        TURQUOISE
    },
    {   // alligator
        GOLDENROD,
        PANSY_PINK,
        DARK_SALMON,
        TURQUOISE
    },
    {   // dragon
        YELLOW,
        SALMON_PINK,
        CRIMSON,
        EMERALD
    },
    {   // apple
        SALMON_PINK,
        LIGHT_SALMON,
        DARK_SALMON,
        SALMON
    },
    {   // hat
        GOLD,
        FANDANGO,
        MINT,
        BLACK
    },
    {   // ribbon
        FUCHSIA,
        CERULEAN,
        IRIS,
        DARK_ORCHID
    },
    {   // luck
        TEAL,
        AVOCADO,
        TURQUOISE,
        EMERALD
    },
    {   // hibiscus
        ROSE,
        PRINTING_GREEN,
        FANDANGO,
        CORAL
    },
    {   // blossom
        CARNATION,
        SPRING,
        LAVENDER,
        AMBER
    },
    {   // diamond
        GOLD_METAL,
        PLATINUM,
        CELESTE,
        HONEYDEW
    },
    {   // elder dragon
        EMERALD,
        ROSE,
        AMBER,
        DARK_ORCHID
    },
    {   // crown
        GOLD,
        FUCHSIA,
        CRIMSON,
        FANDANGO
    }
};
