#include "board_t.h"
#include "layer_t.h"
#include "puzzle_t.h"
#include "survival_t.h"

#include <stdio.h>

int main () {
    printf("%9lu puzzle_t\n", sizeof(puzzle_t));
    printf("%9lu saved_puzzle_t\n", sizeof(saved_puzzle_t));

    puts("");

    size_t boardIndirection = bitmap_size(NUM_EDGES);
    printf("%9lu board_t\n", sizeof(board_t) + boardIndirection);
    printf("%9lu edges\n", boardIndirection);

    puts("");

    size_t layerIndirection = LAYER_WIDTH * LAYER_HEIGHT * sizeof(point_t);
    printf("%9lu layer_t\n", sizeof(layer_t) + layerIndirection);
    printf("%9lu pixels\n", sizeof(((layer_t *)0)->pixels));
    printf("%9lu pointsForGroup\n", sizeof(((layer_t *)0)->pointsForGroup) + layerIndirection);

    puts("");

    static survival_t survival;
    size_t survivalPointIndirection = sizeof(point_t) * SURVIVAL_WIDTH * SURVIVAL_HEIGHT;
    size_t survivalVectorIndirection = sizeof(vp_t) * NUM_GROUPS;
    printf("%9lu survival_t\n", sizeof(survival_t) + survivalPointIndirection + survivalVectorIndirection);
    printf("%9lu pixels\n", sizeof(survival.pixels));
    printf("%9lu groupForPixel\n", sizeof(survival.groupForPixel));
    printf("%9lu toEdgeDistances\n", sizeof(survival.toEdgeDistances));
    printf("%9lu controlDistances\n", sizeof(survival.controlDistances));
    printf("%9lu pointsForGroup (vector)\n", survivalVectorIndirection);
    printf("%9lu pointsForGroup (points)\n", survivalPointIndirection);

    return 0;
}
