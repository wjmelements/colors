#include "mocks/colorsets.h"

#include "color.h"
#include "layer.h"
#include "mocks/group.h"
#include "user.h"

#include <assert.h>

user_t user;

void test_colors() {
    pixel_t white;
    white.rgba = WHITE;
    assert(white.r == 0xFF);
    assert(white.g == 0xFF);
    assert(white.b == 0xFF);
    assert(white.a == 0xFF);

    pixel_t green;
    green.rgba = GREEN;
    assert(green.r == 0x00);
    assert(green.g == 0xFF);
    assert(green.b == 0x00);
    assert(green.a == 0xFF);

    pixel_t blue;
    blue.rgba = BLUE;
    assert(blue.r == 0x00);
    assert(blue.g == 0x00);
    assert(blue.b == 0xFF);
    assert(blue.a == 0xFF);
    
    pixel_t red;
    red.rgba = RED;
    assert(red.r == 0xFF);
    assert(red.g == 0x00);
    assert(red.b == 0x00);
    assert(red.a == 0xFF);

    pixel_t yellow;
    yellow.rgba = YELLOW;
    assert(yellow.r == 0xFF);
    assert(yellow.g == 0xFF);
    assert(yellow.b == 0x00);
    assert(yellow.a == 0xFF);
}

void test_interpolate() {
    uint32_t result;
    uint16_t i = 0;
    do {
        result = interpolate(RED, RED, i);
        assert(result == RED);
    } while (++i);

    result = interpolate(0x11223344, 0x55443322, 1);
    assert(result == 0x33333333);
    result = interpolate(RED, WHITE, 0);
    assert(result == RED);
}

#include <string.h>

void mock_board(board_t *board) {
    memset(board, 0 , sizeof(*board));
    bitmap_init(&board->edges, NUM_EDGES);
    bitmap_set(board->edges, adjIndexFor(0,1));
}

void mock_layer(layer_t *layer) {
    layer_init(layer);
    resetGroupColors(layer);
}

void test_invalidate() {
    board_t *board = Malloc(sizeof(*board));
    mock_board(board);
    layer_t *layer = Malloc(sizeof(*layer));
    mock_layer(layer);
    colorGroup(board, layer, 0, RED, 0);
    assert(!layer->invalidEdges[0]);
    colorGroup(board, layer, 1, RED, 0);
    assert(layer->invalidEdges[0] == 1 << 1);
    assert(layer->invalidEdges[1] == 1 << 0);
    colorGroup(board, layer, 1, BLUE, 0);
    assert(!layer->invalidEdges[0]);
    assert(!layer->invalidEdges[1]);
    colorGroup(board, layer, 0, BLUE, 0);
    assert(layer->invalidEdges[0] == 1 << 1);
    assert(layer->invalidEdges[1] == 1 << 0);
    colorGroup(board, layer, 0, GREEN, 0);
    assert(!layer->invalidEdges[0]);
    assert(!layer->invalidEdges[1]);
    board_destroy(board);
    layer_destroy(layer);
}

#include "puzzle.h"
#include "board.h"

void test_consistency() {
    puzzle_t puzzle;
    generatePuzzle(&puzzle);
    board_t *board = Malloc(sizeof(*board));
    layer_t *layer = Malloc(sizeof(*layer));
    boardFrom(board, puzzle);
    layerFrom(board, layer);
    for (group_t g = 0; g < NUM_GROUPS; g++) {
        for (uint32_t i = 0; i < layer->pointsForGroup[g].num_points; i++) {
            point_t point = layer->pointsForGroup[g].points[i];
            assert(board->groupForPixel[point.x][point.y] == g);
        }
    }
    layer_destroy(layer);
    board_destroy(board);
}

int main () {
    test_colors();
    test_interpolate();
    test_invalidate();
    test_consistency();
    return 0;
}
