#include "mocks/colorsets.h"

#include "user.h"

#include <assert.h>
#include <strings.h>

void test_score() {
    user_t user;
    assert(sizeof(user.header) == 8);
    bzero(&user, sizeof(user));
    user_upgrade(&user);
    for (uint16_t i = 0; i < 0x3FFF; i++) {
        assert(user_puzzlesPerfected(&user) == i);
        user_incrementPuzzlesPerfected(&user);
        user_upgrade(&user);
    }
    assert(user_puzzlesPerfected(&user) == 0x3FFF);
    for (uint32_t i = 0; i < 0xFFFF; i++) {
        assert(user_highestScore(&user) == i);
        user_setHighestScore(&user, i + 1);
    }
    assert(user_highestScore(&user) == 0xFFFF);
}

void test_toggle() {
    assert(piColor(piColor(SALMON)) == SALMON);
    for (double i = 0.0; i <= 100.0; i+= 1.0) {
        assert(piToggle(piToggle(i)) == i);
        assert(piToggle(i) != i);
    }
    for (uint64_t i = 0; i < 0x3FFF; i++) {
        assert(piXor(piXor(i)) == i);
    }
}

void test_streak() {
    user_t user;
    bzero(&user, sizeof(user));
    user_upgrade(&user);
    for (uint64_t i = 0; i < 0xFFF; i++) {
        user_setLongestStreak(&user);
        user.piLongestStreak = piXor(i);
        user_upgrade(&user);
        assert(piXor(user.piLongestStreak) == i);
        user.latestStreaks[0]++;
    }
}

int main() {
    test_score();
    test_toggle();
    test_streak();
    return 0;
}
