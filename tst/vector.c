#include "point_t.h"
#include "vector.h"
VECTOR(point, vp);

#include <assert.h>

#define COUNT 0xFFF

void test_append() {
    point_t point;
    vp_t vp;
    vp_init(&vp, 525);
    uint32_t i = 0;
    for (point.x = 0; point.x < 0xFFF; point.x++) {
        for (point.y = 0; point.y < COUNT; point.y++) {
            assert(vp.num_points == i);
            vp_append(&vp, point);
            i++;
        }
    }
    assert(vp.num_points == i);
    i = 0;
    for (point.x = 0; point.x < 0xFFF; point.x++) {
        
        for (point.y = 0; point.y < COUNT; point.y++) {
            assert(vp.points[i].x == point.x);
            assert(vp.points[i].y == point.y);
            i++;
        }
    }
    vp_destroy(&vp);
}

int main() {
    for (int i = 0; i < 50; i++) {
        test_append();
    }
    return 0;
}
