#include "mocks/colorsets.h"

#include "survival.h"

#include "user.h"
user_t user;

#include <assert.h>

static void test_dimensions() {
    assert(SURVIVAL_HEIGHT >= LAYER_HEIGHT);
    assert(SURVIVAL_WIDTH >= LAYER_WIDTH);
    assert(SURVIVAL_WIDTH > LAYER_WIDTH || SURVIVAL_HEIGHT > LAYER_HEIGHT);
    assert(SURVIVAL_HEIGHT > LAYER_HEIGHT + SURVIVAL_WIDTH);
}

static survival_t survival;
static void test_destroy() {
    for (uint16_t i = 0; i < 2; i++) {
        survival_init(&survival);
        survival_destroy(&survival);
    }
}

static void test_shift() {
    survival_init(&survival);
    survival_start(&survival);
    survival_shift(&survival);
    survival_destroy(&survival);
}
static void test_frame() {
    survival_init(&survival);
    survival_start(&survival);
    image_ref_t images[2];
    for (uint16_t i = 0; i < SURVIVAL_HEIGHT + LAYER_HEIGHT; i++) {
        survival_imageTop(&survival, &images[0]);
        survival_imageBottom(&survival, &images[1]);
        assert(images[0].height + images[1].height == LAYER_HEIGHT);
        survival_shift(&survival);
    }
    survival_destroy(&survival);
}
static inline uint16_t MIN2(uint16_t a, uint16_t b) {
    return a < b ? a : b;
}
static void _test_toEdge() {
    for (uint16_t y = survival.heightOffset + 1; y < survival.heightOffset + LAYER_HEIGHT; y++) {
        const uint16_t py = y % SURVIVAL_HEIGHT;
        assert(y < SURVIVAL_HEIGHT ? py == y : y / SURVIVAL_HEIGHT * SURVIVAL_HEIGHT + py == y);
        for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
            const int d = survival.toEdgeDistances[py][x];
            assert(abs(d) < SURVIVAL_WIDTH);
            uint16_t min = SURVIVAL_WIDTH;
            point_t point;
            group_t group = survival.groupForPixel[py][x];
            #define test(X,Y)\
                point.x = X, point.y = Y;\
                if (point.x < SURVIVAL_WIDTH ) {\
                    if (point.y < survival.heightOffset + LAYER_HEIGHT) {\
                        point.y %= SURVIVAL_HEIGHT;\
                        int dd = d - survival.toEdgeDistances[point.y][point.x];\
                        assert(abs(dd) <= 1);\
                        min = MIN2(min, survival.toEdgeDistances[point.y][point.x]);\
                        group_t otherGroup = survival.groupForPixel[point.y][point.x];\
                        if (group != otherGroup) {\
                            assert(survival.toEdgeDistances[py][x] == 0);\
                            assert(survival.toEdgeDistances[point.y][point.x] == 0);\
                            if (group < otherGroup) {\
                                assert(survival_hasEdge(&survival, group, otherGroup));\
                            } else {\
                                assert(survival_hasEdge(&survival, otherGroup, group));\
                            }\
                        }\
                    }\
                }
            test(x-1,y);
            test(x+1,y);
            test(x,y-1);
            test(x,y+1);
            #undef test
            assert(!survival.toEdgeDistances[py][x] || min + 1 == survival.toEdgeDistances[py][x]);
        }
    }
}
static void _test_edges() {
    // TODO ensure that these are also the only edges in survival
    for (uint16_t y = survival.heightOffset; y < survival.heightOffset + LAYER_HEIGHT; y++) {
        const uint16_t py = y % SURVIVAL_HEIGHT;
        for (uint16_t x = 1; x < SURVIVAL_WIDTH; x++) {
            group_t g1 = survival.groupForPixel[py][x],
                    g2 = survival.groupForPixel[py][x - 1];
            if (g1 < g2) {
                assert(survival_hasEdge(&survival, g1, g2));
            } else if (g2 < g1) {
                assert(survival_hasEdge(&survival, g2, g1));
            }
        }
    }
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        for (uint16_t y = survival.heightOffset + 1; y < survival.heightOffset + LAYER_HEIGHT; y++) {
            const uint16_t py = y % SURVIVAL_HEIGHT;
            group_t g1 = survival.groupForPixel[py][x],
                    g2 = survival.groupForPixel[py - 1][x];
            if (g1 < g2) {
                assert(survival_hasEdge(&survival, g1, g2));
            } else if (g2 < g1) {
                assert(survival_hasEdge(&survival, g2, g1));
            }
        }
    }
}

static void test_toEdge() {
    survival_init(&survival);
    survival_start(&survival);
    _test_edges();
    _test_toEdge();
    for (uint16_t i = 0; i < 3; i++) {
        survival_shift(&survival);
        _test_edges();
        _test_toEdge();
    }
    survival_destroy(&survival);
}
static void _test_allHaveEdges() {
    for (group_t g1 = 0; g1 < survival.edgesForGroup.num_bitmaps; g1++) {
        if (!survival.pointsForGroup.vps[g1].num_points) {
            continue;
        }
        int found = 0;
        for (group_t g2 = 0; g2 < survival.edgesForGroup.num_bitmaps; g2++) {
            if (g1 < g2) {
                if (survival_hasEdge(&survival, g1, g2)) {
                    found = 1;
                    break;
                }
            } else if (g2 < g1) {
                if (survival_hasEdge(&survival, g2, g1)) {
                    found = 1;
                    break;
                }
            }
        }
        assert(found);
    }
}
static void _test_groupEdges() {
    assert(survival.heightOffset == 0);
    for (uint16_t y = 0; y < LAYER_HEIGHT; y++) {
        for (uint16_t x = 1; x < SURVIVAL_WIDTH; x++) {
            group_t g1 = survival.groupForPixel[y][x];
            group_t g2 = survival.groupForPixel[y][x-1];
            if (g1 < g2) {
                assert(survival_hasEdge(&survival, g1, g2));
            } else if (g2 < g1) {
                assert(survival_hasEdge(&survival, g2, g1));
            }
        }
    }
    for (uint16_t x = 0; x < SURVIVAL_WIDTH; x++) {
        for (uint16_t y = 1; y < LAYER_HEIGHT; y++) {
            group_t g1 = survival.groupForPixel[y][x];
            group_t g2 = survival.groupForPixel[y-1][x];
            if (g1 < g2) {
                assert(survival_hasEdge(&survival, g1, g2));
            } else if (g2 < g1) {
                assert(survival_hasEdge(&survival, g2, g1));
            }
        }
    }

}
static void test_edges() {
    survival_init(&survival);
    survival_start(&survival);
    _test_allHaveEdges();
    _test_groupEdges();
    survival_destroy(&survival);
}

static void test_invalid() {
    survival_init(&survival);
    survival_start(&survival);
    for (group_t g = 0; g < survival.groupColors.num_colors; g++) {
        assert(survival_valid(&survival, g));
    }
    survival_setInvalid(&survival, 0, 1);
    assert(!survival_valid(&survival, 0));
    assert(!survival_valid(&survival, 1));
    assert(survival_onlyInvalid(&survival, 0, 1));
    assert(survival_onlyInvalid(&survival, 1, 0));
    assert(!survival_onlyInvalid(&survival, 0, 2));
    assert(!survival_onlyInvalid(&survival, 1, 2));
    survival_unsetInvalid(&survival, 0, 1);
    for (group_t g = 0; g < survival.groupColors.num_colors; g++) {
        assert(survival_valid(&survival, g));
    }
    assert(!survival_onlyInvalid(&survival, 0, 1));
    assert(!survival_onlyInvalid(&survival, 1, 0));
    survival_destroy(&survival);
}

static void _test_color() {
    survival_init(&survival);
    survival_start(&survival);
    for (group_t group = 0; group < NUM_GROUPS; group++) {
        assert(survival.groupColors.colors[group] == NULL_COLOR);
    }
    survival.groupForPixel[LAYER_HEIGHT][0] = 0;
    survival.groupForPixel[LAYER_HEIGHT][1] = 1;
    survival.groupForPixel[LAYER_HEIGHT][2] = 2;
    survival.groupForPixel[LAYER_HEIGHT - 1][0] = 3;
    survival_shift(&survival);
    assert(survival.groupOffset == 0);
    assert(survival_hasEdge(&survival, 0, 1));
    assert(survival_hasEdge(&survival, 1, 2));
    assert(survival_hasEdge(&survival, 0, 3));

    uint64_t illegals;
    assert(survival_color(&survival, 0, SALMON, &illegals));
    assert(survival.groupColors.colors[0] == SALMON);
    assert(survival_valid(&survival, 0));

    assert(survival_color(&survival, 3, SALMON, &illegals));
    assert(survival.groupColors.colors[3] == SALMON);
    assert(!survival_valid(&survival, 0));
    assert(!survival_valid(&survival, 3));
    assert(survival_onlyInvalid(&survival, 0, 3));
    assert(survival_onlyInvalid(&survival, 3, 0));

    assert(survival_color(&survival, 3, TEAL, &illegals));
    assert(survival.groupColors.colors[0] == SALMON);
    assert(survival.groupColors.colors[3] == TEAL);
    assert(survival_valid(&survival, 0));
    assert(survival_valid(&survival, 3));

    assert(survival_color(&survival, 0, TEAL, &illegals));
    assert(survival.groupColors.colors[0] == TEAL);
    assert(survival.groupColors.colors[3] == TEAL);
    assert(survival_hasEdge(&survival, 0, 3));
    assert(!survival_valid(&survival, 0));// FIXME this can fail
    assert(!survival_valid(&survival, 3));
    assert(survival_onlyInvalid(&survival, 0, 3));
    assert(survival_onlyInvalid(&survival, 3, 0));

    assert(survival_color(&survival, 0, SALMON, &illegals));
    assert(survival.groupColors.colors[0] == SALMON);
    assert(survival_valid(&survival, 0));
    assert(survival_valid(&survival, 3));
    survival_destroy(&survival);
}

static void test_color() {
    for (uint8_t i = 0; i < 2; i++) {
        _test_color();
    }
}

static void test_order() {
    survival_init(&survival);
    survival_start(&survival);
    survival_shift(&survival);
    for (uint16_t vp_i = 0; vp_i < survival.pointsForGroup.num_vps; vp_i++) {
        const vp_t *vp = &survival.pointsForGroup.vps[vp_i];
        for (uint32_t p_i = 1; p_i < vp->num_points; p_i++) {
            assert(vp->points[p_i - 1].y <= vp->points[p_i].y);
        }
    }
    survival_destroy(&survival);
}

static void test_loop() {
    survival_init(&survival);
    survival_start(&survival);
    assert(survival.pointsForGroup.num_vps == survival.edgesForGroup.num_bitmaps);
    for (uint32_t i = 0; i < SURVIVAL_HEIGHT * 3 + 1; i++) {
        survival_shift(&survival);
    }
    survival_destroy(&survival);
}

static void _test_colors() {
    // TODO these lengths are always the same and can be shared
    assert(survival.groupColors.num_colors == survival.pointsForGroup.num_vps);
    for (group_t groupIndex = 0; groupIndex < survival.pointsForGroup.num_vps; groupIndex++) {
        vp_t *vp = &survival.pointsForGroup.vps[groupIndex];
        for (uint32_t i = 0; i < vp->num_points; i++) {
            point_t *point = &vp->points[i];
            assert(point->x < SURVIVAL_WIDTH);
            assert(point->y < SURVIVAL_HEIGHT);
            assert(groupIndex < survival.pointsForGroup.num_vps);
        }
    }
}

static void _test_groupColor(group_t group, color_t color) {
    group_t groupIndex = group - survival.groupOffset;
    assert(survival.groupColors.colors[groupIndex] == color);
    for (uint32_t i = 0; i < survival.pointsForGroup.vps[groupIndex].num_points; i++) {
        point_t point = survival.pointsForGroup.vps[groupIndex].points[i];
        assert(point.x < SURVIVAL_WIDTH && point.y < SURVIVAL_HEIGHT);
    }
}

static void test_colors() {
    survival_init(&survival);
    survival_start(&survival);
    _test_colors();

    uint64_t illegals;
    survival_color(&survival, 0, SALMON, &illegals);
    _test_colors();
    _test_groupColor(0, SALMON);
    for (uint16_t i = 1; i < NUM_GROUPS; i++) {
        _test_groupColor(i, NULL_COLOR);
    }

    survival_color(&survival, 12, CRIMSON, &illegals);
    for (uint16_t i = 0; i < SURVIVAL_HEIGHT + 1; i++) {
        survival_shift(&survival);
        _test_colors();
        if (survival.groupOffset == 0) {
            _test_groupColor(0, SALMON);
        }
        if (survival.groupOffset < 12) {
            _test_groupColor(12, CRIMSON);
        }
    }

    survival_destroy(&survival);
}

static inline void assert_started() {
    assert(piColor(user.piColors[0].color) == survival.colors[2]);
    assert(piColor(user.piColors[1].color) == survival.colors[3]);
    assert(piColor(user.piColors[2].color) == survival.colors[0]);
    assert(piColor(user.piColors[3].color) == survival.colors[1]);
}

static void test_rotateColors() {
    user.piColors[0].color = piColor(RED);
    user.piColors[1].color = piColor(GREEN);
    user.piColors[2].color = piColor(BLUE);
    user.piColors[3].color = piColor(BLACK);
    survival_start(&survival);
    assert_started();
    survival_rotateColors(&survival);
    assert_started();
    color_t c0 = survival.colors[0];
    for (uint8_t i = 0; i < 4; i++) {
        survival.colors[i] = survival.colors[3&(i+1)];
    }
    survival.colors[3] = c0;
    survival_rotateColors(&survival);
    assert_started();
    #define swap(X, Y)\
        X ^= Y;\
        Y ^= X;\
        X ^= Y;
    swap(survival.colors[0], survival.colors[2]);
    swap(survival.colors[1], survival.colors[3]);
    survival_rotateColors(&survival);
    assert_started();
}


int main() {
    user_upgrade(&user);
    test_dimensions();
    test_destroy();
    test_shift();
    test_frame();
    test_toEdge();
    test_edges();
    test_invalid();
    test_color();
    test_order();
    test_colors();
    test_loop();
    test_rotateColors();
    return 0;
}
