#include "bitmap.h"

#include <assert.h>

void basic_test(uint64_t size) {
    bitmap_t bitmap;
    bitmap_init(&bitmap, size);
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < i; j++) {
            assert(bitmap_isSet(bitmap, j));
        }
        for (int j = i; j < size; j++) {
            assert(!bitmap_isSet(bitmap, j));
        }
        bitmap_set(bitmap, i);   
    }
    bitmap_destroy(bitmap);
}

int main() {
    for (uint64_t size = 0; size < 0xFFF; size+=0xFFF/0x10) {
        basic_test(size);
    }
    return 0;
}
