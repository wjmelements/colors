#include <assert.h>

#include "board.h"
#include "puzzle.h"

void test_adjIndexFor() {
    bitmap_t bitmap;
    bitmap_init(&bitmap, NUM_EDGES);
    for (group_t g1 = 0; g1 < NUM_GROUPS; g1++) {
        for (group_t g2 = g1 + 1; g2 < NUM_GROUPS; g2++) {
            uint32_t index = adjIndexFor(g1, g2);
            assert(index < NUM_EDGES);
            assert(!bitmap_isSet(bitmap, index));
            bitmap_set(bitmap, index);
            assert(bitmap_isSet(bitmap, index));
        }
    }
    bitmap_destroy(bitmap);
}

int main() {
    test_adjIndexFor();
    return 0;
}
