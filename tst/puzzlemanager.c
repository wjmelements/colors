#include "mocks/colorsets.h"

#include "puzzlemanager.h"
#include "puzzlestorage.h"

#include <assert.h>

#include "user.h"

user_t user;

void onAvailabilityChanged() {
    // nop
}

int main() {
    puzzle_t puzzle;
    generatePuzzle(&puzzle);
    insertPuzzle(puzzle, 19, 10);
    puzzle_t puzzle2 = getNextBestFitPuzzle();
    assert(puzzle2 == puzzle);
    free(puzzle);
    generatePuzzle(&puzzle);
    insertPuzzle(puzzle, 100, 10);
    user.preferredDifficulty = DRAGON_DIFFICULTY;
    assert(puzzle == awaitNextBestFitPuzzle());
    free(puzzle);
    assert(NULL == getNextBestFitPuzzle());
    // test smallest tile retained
    puzzle_t fakes[PUZZLE_STORAGE + 1];
    for (int i = 0; i <= PUZZLE_STORAGE; i++) {
        fakes[i] = malloc(0);
        insertPuzzle(fakes[i], 20, PUZZLE_STORAGE + 2 - i);
        // the last puzzle will have the smallest smallest tile
    }
    for (int i = 0; i < PUZZLE_STORAGE; i++) {
        puzzle_t fake = getNextBestFitPuzzle();
        assert(fake != fakes[PUZZLE_STORAGE]);
        free(fake);
    }
    assert(NULL == getNextBestFitPuzzle());

    startPuzzleManager();
    for (difficulty_t difficulty = HATCH_DIFFICULTY; difficulty < ALLIGATOR_DIFFICULTY; difficulty++) {
        user.preferredDifficulty = difficulty;
        for (uint16_t i = 0; i < 5; i++) {
            puzzle_t puzzle = awaitNextPuzzle();
            assert(puzzle);
            free(puzzle);
        }
    }
    for (difficulty_t difficulty = ALLIGATOR_DIFFICULTY; difficulty <= DRAGON_DIFFICULTY; difficulty++) {
        for (uint16_t i = 0; i < 3; i++) {
            puzzle_t puzzle = awaitNextPuzzle();
            assert(puzzle);
            free(puzzle);
        }
    }
    for (difficulty_t difficulty = HATCH_DIFFICULTY; difficulty <= DRAGON_DIFFICULTY; difficulty++) {
        for (uint16_t i = 0; i < 4000; i++) {
            free(getNextPuzzle());
        }
    }
    pausePuzzleManager(1);
    return 0;
}
