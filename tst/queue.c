#include <assert.h>

#include "capitalC.h"
#include "queue.h"
QUEUE(queue, int)

void basic_test() {
    queue_t queue;
    queue_init(&queue);
    assert(!queue_ready(&queue));
    queue_push(&queue, 4);
    assert(queue_ready(&queue));
    assert(4 == queue_pop(&queue));
    assert(!queue_ready(&queue));
    queue_destroy(&queue);
}

void iter_test() {
    queue_t queue;
    queue_init(&queue);
    for (int i = 0; i < 100; i++) {
        queue_push(&queue, i);
    }
    for (int i = 0; i < 100; i++) {
        assert(queue_ready(&queue));
        assert(queue_pop(&queue) == i);
    }
    assert(!queue_ready(&queue));
    queue_destroy(&queue);
}

int main () {
    basic_test();
    iter_test();
    return 0;
}
