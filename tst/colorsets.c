#include "colorsets.h"

#include <assert.h>

int main() {
    for (int i = 0; i < NUM_COLORSETS; i++) {
        assert(colorsets_equal(colorSets[i], colorSets[i]));
    }
    for (int i = 1; i < NUM_COLORSETS; i++) {
        for (int j = 0; j < i; i++) {
            assert(!colorsets_equal(colorSets[j], colorSets[i]));
        }
    }
    return 0;
}
